package zzx.service;

import zzx.entity.BusOrderDTO;

public interface BusOrderService extends BaseService<BusOrderDTO>{

	void doChangeStatus(Integer id,Integer status);

}
