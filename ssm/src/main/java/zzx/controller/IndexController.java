package zzx.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller //这个注解正式时使用
@RequestMapping("index")
public class IndexController {

	@RequestMapping("/message")
	public String message(){
		
		return "user/message";
	}
	
}
