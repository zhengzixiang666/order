<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ include file="../common/common.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <title> DOM-XSS TEST </title>
    </head>
    <body>
    <div style="margin-top:20px;margin-left: 20px;">
        <script>
            window.onload= function(){
                var oBox=document.getElementById("box");
                var oSpan=document.getElementById("span1");
                var oText=document.getElementById("text1");
                var oBtn=document.getElementById("Btn");
                oBtn.onclick = function(){
                    oBox.innerHTML = oBox.innerHTML + oSpan.innerHTML + oText.value + "<br/>";
                    // oBox.innerHTML += oSpan.innerHTML + oText.value +  "<br/>";//这是简便的写法，在js中 a=a+b ,那么也等同于 a+=b
                  // oBox.innerText = oBox.innerHTML + oSpan.innerHTML + oText.value + "<br/>";
                    oText.value=""
                };
            }
        </script>

        <div id="box" style="margin-bottom: 20px"></div>
        <span id="span1" >DOM：</span>
        <input type="text" id="text1" class="easyui-textbox" style="width: 300px"/>
        <a href="#" id="Btn" type="button" class="easyui-linkbutton" plain="false" iconCls="icon-add">提 交</a>
    
    	<script>
            var hash = location.hash;
            if(hash){
                var url = hash.substring(1);
                location.href = url;
            }
        </script>
        </div>
    </body>
</html>