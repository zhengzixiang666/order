package zzx.dao;

import org.springframework.stereotype.Repository;


import zzx.entity.BusFoodDTO;
@Repository("busFoodDAO")
public interface BusFoodDAO extends BaseDAO<BusFoodDTO> {

}
