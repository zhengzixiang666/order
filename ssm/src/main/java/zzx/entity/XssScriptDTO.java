package zzx.entity;

import java.io.Serializable;

public class XssScriptDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	
	private String xssScript;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getXssScript() {
		return xssScript;
	}

	public void setXssScript(String xssScript) {
		this.xssScript = xssScript;
	}
	
	

}
