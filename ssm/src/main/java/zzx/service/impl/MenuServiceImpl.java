package zzx.service.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import zzx.dao.MenuDAO;
import zzx.dao.RoleMenuDAO;
import zzx.dao.UserDAO;
import zzx.dao.UserRoleDAO;
import zzx.entity.MenuDTO;
import zzx.entity.RoleDTO;
import zzx.entity.RoleMenuDTO;
import zzx.entity.UserDTO;
import zzx.entity.UserRoleDTO;
import zzx.enumutil.IsleafEnum;
import zzx.enumutil.YesNoEnum;
import zzx.exception.HandException;
import zzx.service.MenuService;
import zzx.util.ListUtil;
import zzx.util.StringUtil;
import zzx.util.ZzxUUIDUtil;
/**
 * 菜单实现类
 * @author 郑仔祥
 *
 */
@Service("menuService")
public  class MenuServiceImpl implements MenuService {
	
	@Resource
	public MenuDAO menuDAO;
	
	@Resource
	public UserRoleDAO userRoleDAO;
	
	@Resource
	public RoleMenuDAO roleMenuDAO;
	
	@Resource
	public UserDAO userDAO;

	public List<MenuDTO> findAllMenuDTO() {
		List<MenuDTO> menuDTOList=menuDAO.getAllMenuDTO();
		return menuDTOList;
	}

	public List<MenuDTO> findParentMenuDTO() {
		List<MenuDTO> menuDTOList=menuDAO.getParentMenuDTO();
		return menuDTOList;
	}

	public List<MenuDTO> findChildrenByParentId(Integer parentId) {
		List<MenuDTO> menuDTOList=menuDAO.getChildrenByParentId(parentId);
		return menuDTOList;
	}
	/**
	 * 获取除去一级菜单的所有子菜单
	 */
	public List<MenuDTO> findAllMenuDTOByUserId(Integer userId) throws HandException {
		if(userId==null){
			throw new HandException("用户未登陆，请重新登陆！");
		}
		//根据用户id，查询该用户是否存在
		UserDTO us=userDAO.getUserDTOByUserId(userId);
		if(us==null){
			throw new HandException("根据用户id，未获取到用户，请重新登陆！");
		}
		List<MenuDTO> menu=new ArrayList<MenuDTO>();
		//先通过用户id查询所有角色。
		List<UserRoleDTO> userRoleDTO=userRoleDAO.getUserRoleDTOByUserId(userId);
		
		//记录父菜单id
		Set<Integer> set = new HashSet<Integer>();
		if(!ListUtil.isEmpty(userRoleDTO)){
			for (UserRoleDTO userRole : userRoleDTO) {
				RoleDTO roleDTO=userRole.getRoleDTO();
				if(roleDTO!=null && roleDTO.getRoleid()!=null){
					//通过角色id获取所有菜单
					List<RoleMenuDTO> RoleMenuDTOList=roleMenuDAO.getMenuDTOByRoleId(roleDTO.getRoleid());
					for (RoleMenuDTO roleMenuDTO : RoleMenuDTOList) {
						MenuDTO menuDTO=roleMenuDTO.getMenuDTO();
						if(menuDTO!=null && menuDTO.getId()!=null){
							this.dg(menuDTO.getId(),set);
						}
					}
				}
			}
		}
		if(set!=null && set.size()>0){
			for (Integer integer : set) {
				//根据菜单id查询菜单
				MenuDTO  m=menuDAO.getMenuDTOByMenuId(integer);
				if(m!=null && !YesNoEnum.YES.getValue().equals(m.getParentid())){
					menu.add(m);
				}
			}
		}
		return menu;
	}
	/**
	 * 根据用户id，查询所有一级父菜单
	 * @param userId
	 * @return
	 * @throws HandException 
	 */
	public List<MenuDTO> findParentMenuDTOByUserId(Integer userId) throws HandException {
		if(userId==null){
			throw new HandException("用户未登陆，请重新登陆！");
		}
		//根据用户id，查询该用户是否存在
		UserDTO us=userDAO.getUserDTOByUserId(userId);
		if(us==null){
			throw new HandException("根据用户id，未获取到用户，请重新登陆！");
		}
		//如果用户未启用抛出异常
		if(YesNoEnum.NO.getValue().compareTo(us.getIsUsed())==0){
			throw new HandException("该用户未启用，请重新登陆！");
		}
		List<MenuDTO> menu=new ArrayList<MenuDTO>();
		//先通过用户id查询所有角色。
		List<UserRoleDTO> userRoleDTO=userRoleDAO.getUserRoleDTOByUserId(userId);
		
		//记录父菜单id
		Set<Integer> set = new HashSet<Integer>();
		if(!ListUtil.isEmpty(userRoleDTO)){
			for (UserRoleDTO userRole : userRoleDTO) {
				RoleDTO roleDTO=userRole.getRoleDTO();
				if(roleDTO!=null && roleDTO.getRoleid()!=null){
					//通过角色id获取所有菜单
					List<RoleMenuDTO> RoleMenuDTOList=roleMenuDAO.getMenuDTOByRoleId(roleDTO.getRoleid());
					for (RoleMenuDTO roleMenuDTO : RoleMenuDTOList) {
						MenuDTO menuDTO=roleMenuDTO.getMenuDTO();
						if(menuDTO!=null && menuDTO.getId()!=null){
							this.dg(menuDTO.getId(),set);
						}
					}
				}
			}
		}
		if(set!=null && set.size()>0){
			for (Integer integer : set) {
				//根据菜单id查询菜单
				MenuDTO  m=menuDAO.getMenuDTOByMenuId(integer);
				if(m!=null && YesNoEnum.YES.getValue().equals(m.getParentid())){
					menu.add(m);
				}
			}
		}
		return menu;
	}

	/**
	 * 根据子节点获取父节点
	 * @param parentid
	 * @param set
	 * @return
	 */
	private Integer dg(Integer parentid,Set<Integer> set){
		set.add(parentid);
		if(parentid==1 || parentid==null){
			return null;
		}else{
			MenuDTO menuDTO= menuDAO.getParentMenuDTOById(parentid);
			if(menuDTO.getId()==1){
				
				return null;
			}
			return dg(menuDTO.getId(),set);
		}
	}

	@Override
	public MenuDTO findMenuDTOByMenuId(Integer id) throws HandException {
		if(id==null){
			throw new HandException("未选择需要需改的菜单");
		}
		return menuDAO.getMenuDTOByMenuId(id);
	}

	/**
	 * 新增菜单，菜单默认启用isused=1，菜单类型menutype=1，checked不差值，菜单编码，菜单id,都由uuid生成
	 */
	@Override
	public void createMenuDTOByMenuDTO(MenuDTO menuDTO) throws HandException {
		// 基础校验不能为空
		this.checkMenuDTO(menuDTO);
		//新菜单名，不能和数据库中已存在的菜单名相同,查询数据库中所有菜单
		List<MenuDTO> menuDTOList=menuDAO.getAllListMenuDTO();
		if(menuDTOList!=null && menuDTOList.size()>0){
			for (MenuDTO menuDTO2 : menuDTOList) {
				if(!StringUtils.isBlank(menuDTO2.getText()) && 
						menuDTO2.getText().equals(menuDTO.getText())){
					throw new HandException("该菜单名已经存在，不能继续！");
				}
			}
		}
		/**
		 * 如果是子节点则他的Parentid就为你传过来的id，则兄弟节点就是同级的parentid
		 */
		if(IsleafEnum.ISLEAF_1.getValue().equals(menuDTO.getBrotherOrSon())){
			menuDTO.setParentid(menuDTO.getParentid());
		}else{
			menuDTO.setParentid(menuDTO.getId());
		}
		menuDTO.setId(null);
		menuDTO.setIsused(YesNoEnum.YES.getValue());
		menuDTO.setMenutype(YesNoEnum.YES.getValue());
		menuDTO.setCode(ZzxUUIDUtil.getUUID());
		menuDAO.createMenuDTO(menuDTO);
	}

	/**
	 * 更新菜单
	 */
	@Override
	public void updateMenuDTOByMenuId(MenuDTO menuDTO) throws HandException {
		//校验不能为空
		this.checkMenuDTO(menuDTO);
		if(menuDTO.getId()==null){
			throw new HandException("需要修改的菜单id为空，不能继续！");
		}
		List<MenuDTO> menuDTOList=menuDAO.getAllListMenuDTO();
		if(ListUtil.isNotEmpty(menuDTOList)){
			for (MenuDTO menu : menuDTOList) {
				if(menu.getId().compareTo(menuDTO.getId())!=0){
					if(menu.getText().equals(menuDTO.getText())){
						throw new HandException("已经存在一个相同的菜单名，不能继续！");
					}
				}
			}
		}
		menuDAO.updateMenuDTO(menuDTO);
	}
	/**
	 * 基础校验
	 * @param menuDTO
	 */
	private void checkMenuDTO(MenuDTO menuDTO){
		//先校验空
		if(menuDTO==null){
			throw new HandException("新菜单对象为空，不能继续！");
		}
		if(StringUtil.isBlank(menuDTO.getText())){
			throw new HandException("新菜单名称为空，不能继续！");
		}
		if(menuDTO.getParentid()==null){
			throw new HandException("新菜单父类id为空，不能继续！");
		}
		if(menuDTO.getIsleaf()==null){
			throw new HandException("新菜单类型为空，不能继续！");
		}
		//菜单类型为功能时，ur不能为空
		if(IsleafEnum.ISLEAF_1.getValue().equals(menuDTO.getIsleaf())){
			if(StringUtil.isBlank(menuDTO.getUrl())){
				throw new HandException("新菜单为功能时Url为空，不能继续！");
			}
		}
//		if(IsleafEnum.ISLEAF_2.getValue().equals(menuDTO.getIsleaf())){
//			if(!StringUtil.isBlank(menuDTO.getUrl())){
//				throw new HandException("新菜单为菜单时Url不为空，不能继续！");
//			}
//		}
		if(StringUtil.isBlank(menuDTO.getIconCls())){
			throw new HandException("新菜单图标为空，不能继续！");
		}
		if(menuDTO.getMenuorder()==null){
			throw new HandException("新菜单排序为空，不能继续！");
		}
	}

	/**
	 * 删除菜单逻辑,先根据菜单id，查询，菜单角色关联表，全部删除，再删除菜单表
	 */
	@Override
	public void deleteMenuDTOByMenuId(Integer id) throws HandException {
		if(id==null){
			throw new HandException("未选择需要删除的菜单，不能继续！");
		}
		roleMenuDAO.deleteRoleMenuDTOByMenuId(id);
		//如果菜单的子节点还存在则先删除子节点
		List<MenuDTO> menuDTOList=menuDAO.getChildrenByParentId(id);
		if(ListUtil.isNotEmpty(menuDTOList)){
			throw new HandException("存在子节点，不能继续！");
		}
		menuDAO.deleteMenuDTOByMenuId(id);
	}

	@Override
	public MenuDTO findFirstChildrenByParentId(Integer parentId) throws HandException {
		List<MenuDTO> menuDTOList=menuDAO.getChildrenByParentId(parentId);
		MenuDTO dto=new MenuDTO();
		if(CollectionUtils.isEmpty(menuDTOList)) {
			return dto;
		}
		for (MenuDTO menuDTO : menuDTOList) {
			if(IsleafEnum.ISLEAF_1.getValue().equals(menuDTO.getIsleaf())) {
				dto=menuDTO;
				break;
			}
		}
		if(dto.getId()==null) {
			for (MenuDTO menuDTO : menuDTOList) {
				dto=dg2(menuDTO.getId());
				break;
			}
		}
		return dto;
	}

	private MenuDTO dg2(Integer parentId){
		MenuDTO dto=new MenuDTO();
		List<MenuDTO> menuDTOList=menuDAO.getChildrenByParentId(parentId);
		if(CollectionUtils.isEmpty(menuDTOList)) {
			return dto;
		}
		for (MenuDTO menuDTO : menuDTOList) {
			if(IsleafEnum.ISLEAF_1.getValue().equals(menuDTO.getIsleaf())) {
				dto=menuDTO;
				break;
			}
		}
		if(dto.getId()==null) {
			for (MenuDTO menuDTO : menuDTOList) {
				dto=dg2(menuDTO.getId());
				break;
			}
		}
		return dto;
	}
}
