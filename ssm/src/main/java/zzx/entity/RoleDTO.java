package zzx.entity;

/**
 * 角色实体类tur_role
 * @author 郑仔祥
 *
 */
public class RoleDTO {
	
	private Integer roleid;//角色id
	
	private String code;//角色编码
	
	private String name;//角色名
	
	private Integer attrbute;//角色属性/1，超级管理员。2，管理员。3，操作员
	
	private Integer isused;//是否启用/1，启用，2，不启用
	
	private String remark;//备注
	
	public RoleDTO(){}

	public RoleDTO(Integer roleid, String code, String name, Integer attrbute, Integer isused, String remark) {
		this.roleid = roleid;
		this.code = code;
		this.name = name;
		this.attrbute = attrbute;
		this.isused = isused;
		this.remark = remark;
	}

	public Integer getRoleid() {
		return roleid;
	}

	public void setRoleid(Integer roleid) {
		this.roleid = roleid;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getAttrbute() {
		return attrbute;
	}

	public void setAttrbute(Integer attrbute) {
		this.attrbute = attrbute;
	}

	public Integer getIsused() {
		return isused;
	}

	public void setIsused(Integer isused) {
		this.isused = isused;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Override
	public String toString() {
		return "RoleDTO [roleid=" + roleid + ", code=" + code + ", name=" + name + ", attrbute=" + attrbute
				+ ", isused=" + isused + ", remark=" + remark + "]";
	}
	
	
	
	
	
}
