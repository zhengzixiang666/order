package zzx.service;

import zzx.entity.BusFoodDTO;
import zzx.vo.BusOrderDetailVo;

public interface BusFoodService extends BaseService<BusFoodDTO> {

	void saveOrderAndDetail(BusOrderDetailVo list);

}
