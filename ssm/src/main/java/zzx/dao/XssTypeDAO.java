package zzx.dao;

import org.springframework.stereotype.Repository;

import zzx.entity.XssTypeDTO;

@Repository("xssTypeDAO")
public interface XssTypeDAO extends BaseDAO<XssTypeDTO>{

	
}
