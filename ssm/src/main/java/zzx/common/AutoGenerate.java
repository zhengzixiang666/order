package zzx.common;
import java.util.HashMap;
import java.util.Map;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.InjectionConfig;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.baomidou.mybatisplus.generator.config.TemplateConfig;
import com.baomidou.mybatisplus.generator.config.rules.DateType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;

public class AutoGenerate {
	
	public static void autoGenerate() {
		// Step1：代码生成器
        AutoGenerator mpg = new AutoGenerator();
        // Step2：全局配置
        GlobalConfig gc = new GlobalConfig();
        String projectPath = "E:\\Generator"; // 填写代码生成的目录(需要修改)
        gc.setOutputDir(projectPath);// 拼接出代码最终输出的目录
        gc.setAuthor("zhengzx");// 配置开发者信息（可选）（需要修改）
        gc.setOpen(true);// 配置是否打开目录，false 为不打开（可选）
        //gc.setActiveRecord(true);// 不需要ActiveRecord特性的请改为false
        gc.setEnableCache(false);// XML 二级缓存
        gc.setBaseResultMap(true);// XML ResultMap
        gc.setBaseColumnList(true);// XML columList
        //gc.setKotlin(true);//是否生成 kotlin 代码
        //gc.setSwagger2(true);// 实体属性 Swagger2 注解，添加 Swagger 依赖，开启 Swagger2 模式（可选）
        gc.setFileOverride(true); // 重新生成文件时是否覆盖，false 表示不覆盖（可选）
        gc.setIdType(IdType.ASSIGN_ID);// 配置主键生成策略，此处为 ASSIGN_ID（可选）
        gc.setDateType(DateType.ONLY_DATE);// 配置日期类型，此处为 ONLY_DATE（可选）
        gc.setEntityName("%sDTO");
        gc.setMapperName("%sDAO");
        gc.setServiceName("%sService");// 默认生成的 service 会有 I 前缀
        gc.setServiceImplName("%sServiceImpl");
        mpg.setGlobalConfig(gc);
        // Step3：数据源配置（需要修改）
        DataSourceConfig dsc = new DataSourceConfig();
        dsc.setDbType(DbType.MYSQL);
        dsc.setUrl("jdbc:mysql://127.0.0.1:3306/student?useSSL=false&characterEncoding=utf8&serverTimezone=Asia/Shanghaijdbc:mysql://127.0.0.1:3306/guarantee?useSSL=false&characterEncoding=utf8&serverTimezone=Asia/Shanghai"); // 配置数据库 url 地址
        // dsc.setSchemaName("testMyBatisPlus"); // 可以直接在 url 中指定数据库名
        dsc.setDriverName("com.mysql.cj.jdbc.Driver");// 配置数据库驱动
        dsc.setUsername("root");// 配置数据库连接用户名
        dsc.setPassword("123456");// 配置数据库连接密码
        mpg.setDataSource(dsc);
        // Step:4：包配置
        PackageConfig pc = new PackageConfig();
        pc.setParent("com.zzx"); // 配置父包名（需要修改）
        // pc.setModuleName("test_mybatis_plus");// 配置模块名（需要修改）
        pc.setEntity("entity");// 配置 entity 包名
        pc.setMapper("dao");// 配置 mapper 包
        pc.setService("service");// 配置 service 
        pc.setServiceImpl("service.impl");
        pc.setController("controller");// 配置 controller 包名
        mpg.setPackageInfo(pc);
        // Step5：策略配置（数据库表配置）
        StrategyConfig strategy = new StrategyConfig();
        strategy.setTablePrefix(new String[] { "tur_"});// 此处可以修改为您的表前缀
        strategy.setInclude(new String[] { "tur_user"});// 指定表名（可以同时操作多个表，使用 , 隔开）（需要修改）
        strategy.setNaming(NamingStrategy.underline_to_camel); // 配置数据表与实体类名之间映射的策略
        strategy.setColumnNaming(NamingStrategy.underline_to_camel);// 配置数据表的字段与实体类的属性名之间映射的策略
        strategy.setEntityLombokModel(false); // 配置 lombok 模式
        strategy.setRestControllerStyle(false);// 配置 rest 风格的控制器（@RestController）
        strategy.setControllerMappingHyphenStyle(true);// 配置驼峰转连字符
	    //strategy.setSuperEntityClass("com.baomidou.demo.TestEntity");// 自定义实体父类
	    //strategy.setSuperEntityColumns(new String[] { "test_id", "age" }); // 自定义实体，公共字段
	    strategy.setSuperMapperClass("zzx.dao.BaseDAO");// 自定义 mapper 父类
	    strategy.setSuperServiceClass("zzx.service.BaseService");// 自定义 service 父类
	    strategy.setSuperServiceImplClass("zzx.service.impl.BaseServiceImpl");// 自定义 service 实现类父类
	    //strategy.setSuperControllerClass("com.zzx.controller.BaseController");// 自定义 controller 父类
	    //public static final String ID = "test_id";// 【实体】是否生成字段常量（默认 false）
	    //strategy.setEntityColumnConstant(true);// 【实体】是否为构建者模型（默认 false）
	    //strategy.setEntityBuilderModel(true);// public User setName(String name) {this.name = name; return this;}
        mpg.setStrategy(strategy);
        //Step6:自定义配置
        // 注入自定义配置，可以在 VM 中使用 】  ${cfg.abc}
        InjectionConfig cfg = new InjectionConfig() {
    	@Override
    	public void initMap() {
    		Map<String, Object> map = new HashMap<String, Object>();
    			map.put("path", "selectList");
    			this.setMap(map);
    		}
    	};
    	mpg.setCfg(cfg);
    	TemplateConfig tc = new TemplateConfig();
    	tc.setController("templates/controller2.java.vm");
    	tc.setService("templates/service2.java.vm");
    	tc.setServiceImpl("templates/serviceImpl2.java.vm");
    	tc.setEntity("templates/entity2.java.vm");
    	tc.setMapper("templates/mapper2.java.vm");
    	tc.setXml("templates/mapper2.xml.vm");
    	mpg.setTemplate(tc);
        // Step6：执行代码生成操作
        mpg.execute();
    }
	
	public static void main(String[] args) {
		autoGenerate();
	}
	
}
