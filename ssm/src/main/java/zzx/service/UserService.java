package zzx.service;


import java.util.List;
import java.util.Map;

import zzx.entity.UserDTO;
import zzx.exception.HandException;

/**
 * 用户服务层接口
 * @author 郑仔祥
 *
 */
public interface UserService {
	
	/**
	 * 登陆用户，
	 * @param username	用户名
	 * @param password	密码
	 * @return
	 */
	public UserDTO findUserDTOLogin(String username,String password)throws HandException;
	
	/**
	 * 通过用户id，发现用户和角色列表
	 * @param userId  用户id
	 * @return	map
	 */
	public Map<String,Object> findUserDTOByUserId(Integer userId)throws HandException;
	
	/**
	 * 得到所有用户
	 * @return
	 */
	public List<UserDTO> findAllUserDTO();
	
	public UserDTO findUserDTOById(Integer userId)throws HandException;
	
	
	
	/**
	 * 
	 * 创建用户，添加用户
	 * @param userDTO	新用户
	 * @return
	 */
	public UserDTO createUserDTOByUserDTO(UserDTO userDTO)throws HandException;
	
	/**
	 * 删除一个用户
	 */
	public void deleteUserDTOByUserId(Integer userId)throws HandException;
	/**
	 * 修改一个用户
	 * @param userDTO  用户信息
	 * @return
	 */
	public void updateUserDTOByUserDTO(UserDTO userDTO,String roleid)throws HandException;
	
	public void updatePasswordByUserId(UserDTO userDTO)throws HandException;
	
	/**
	 * 条件查询
	 */
	public List<UserDTO> findUserDTOByUserDTO(UserDTO userDTO);
	
	public List<UserDTO> findBindList();

	public UserDTO registerUserDTOByUserDTO(UserDTO userDTO)throws HandException;
	
	
	
	
}
