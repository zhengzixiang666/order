package zzx.dao;

import org.springframework.stereotype.Repository;

import zzx.entity.BusOrderDTO;

@Repository("busOrderDAO")
public interface BusOrderDAO extends BaseDAO<BusOrderDTO> {

}
