<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ include file="../common/common.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>文件生成</title>
<script type="text/javascript">

if ($.fn.pagination){
    $.fn.pagination.defaults.beforePageText = '第';
    $.fn.pagination.defaults.afterPageText = '共{pages}页';
    $.fn.pagination.defaults.displayMsg = '显示{from}到{to},共{total}记录';
}
//分页功能    
function pagerFilter(data) {
    if (typeof data.length == 'number' && typeof data.splice == 'function') {
        data = {
            total: data.length,
            rows: data
        }
    }
    var dg = $(this);
    var opts = dg.datagrid('options');
    var pager = dg.datagrid('getPager');
    pager.pagination({
        onSelectPage: function (pageNum, pageSize) {
            opts.pageNumber = pageNum;
            opts.pageSize = pageSize;
            pager.pagination('refresh', {
                pageNumber: pageNum,
                pageSize: pageSize
            });
            dg.datagrid('loadData', data);
        }
    });
    if (!data.originalRows) {
        if(data.rows)
            data.originalRows = (data.rows);
        else if(data.data && data.data.rows)
            data.originalRows = (data.data.rows);
        else
            data.originalRows = [];
    }
    var start = (opts.pageNumber - 1) * parseInt(opts.pageSize);
    var end = start + parseInt(opts.pageSize);
    data.rows = (data.originalRows.slice(start, end));
    return data;
}

	$(function() {
		$('#dg').datagrid({
			iconCls : 'icon-a_detail',
			fit : true,
			fitColumns : true,
			rownumbers : true,
			pagination : true,
			loadFilter: pagerFilter,
			checkOnSelect:true,
			border : false,
			striped : true,
			toolbar : '#tb',
			columns : [ [ {
				field : 'id',
				title : '文件id',
				checkbox:  true
			},{
				field : 'fileName',
				title : '文件名',
				width : '10%',
				align : 'center'
			},{
				field : 'loginCode',
				title : '用户名',
				width : '10%',
				align : 'center'
			}, {
				field : 'suffixName',
				title : '文件后缀名',
				width : '10%',
				align : 'center'
			},{
				field : 'createTime',
				title : '上传时间',
				width : '20%',
				align : 'center',
				formatter : function(value){
	                var date = new Date(value);
	               return formatDate(date,"yyyy-MM-dd");
	            }
			}, {
				field : 'fileType',
				title : '文件类型',
				width : '10%',
				align : 'center'
			}, {
				field : 'message',
				title : '备注',
				width : '15%',
				align : 'center'
			},{
				field : 'field',
				title : '操作',
				width : '25%',
				align : 'center',
				formatter:function(value,row,index){
					var row=JSON.stringify(row);
					var data="<a href='#' name='edit' onclick='edit(" + row + ")' title='修改'></a> "
					+ "\| <a href='#' name='man' class='easyui-linkbutton' onclick='man(" + row + ")' title='下载'></a> "
					+"\| <a href='#' name='deleteRow' class='easyui-linkbutton' onclick='doDelete(" + row + ")' title='删除'></a>";
					return data;
				}
				
			}]],
			onLoadSuccess:function(data){    
				  $("a[name='edit']").linkbutton({text:'修改',plain:false,iconCls:'icon-edit'}); 
			        $("a[name='man']").linkbutton({text:'下载',plain:false,iconCls:'icon-man'});
			        $("a[name='deleteRow']").linkbutton({text:'删除',plain:false,iconCls:'icon-cancel'});    
			}
		});
		reloadData();
	});
	//加载数据表格
	function reloadData(){
		$.ajax({
			url:'findAllSysFileDTO.do',
			type:'post',
			dataType:'json',
			success:function(data){
				var json=data.data;
				success(json);
			},
			error:function(data){
				var json=data.data;
				alert(json.message);
			}
		});
	}
	//处理枚举类的显示，并加载表格
	function success(json){
		$('#dg').datagrid('loadData',json);
	}
	
	//查询
	function doQuery(){
		var loginCode=jQuery('#loginCode').val();
		var fileName=jQuery('#fileName').val();
		$.ajax({
			url:'findAllSysFileDTO.do',
			type:'post',
			data:{"loginCode":loginCode,"fileName":fileName},
			dataType:'json',
			success:function(data){
				var json=data.data;
				success(json);
			},
			error:function(data){
				var json=data.data;
				alert(json.message);
			}
		});
	}

	//新增
	function doAdd(){
		layer.open({
			type:2,
			title:'当前位置 : 上传文件',
			content:'doAddUpload.do',
			skin:'layui-layer-lan',
			area:['400px','300px'],
			maxmin:false,
			btn: ['关闭'],
			yes: function(index, layero){
				layer.close(index);
			  },
			cancel: function(index, layero){ 
				 layer.close(index);
				}   
			});   
		}
		
		//删除文件的方法
		function doDelete(row){
			var id=row.id;
			layer.confirm('确定删除吗?', {
				  btn: ['确定','取消']
				},function(){
					$.ajax({
						type:'post',
						data:{"id":id},
						url:'deleteFileById.do',
						dataType:'json',
						success:function(json){
							layer.alert(json.message,{
								skin: 'layui-layer-molv',
								yes:function(index, layero){
									layer.close(index);
									location.reload();
								}
							});
							
						},
						error:function(json){
							layer.alert(json.message);
						}
					});
				},function(){
					
				});
		}
		//下载
		function man(row){
			var id=row.id;
			var path=window.parent.$('#path').val();
 			window.location.href=path+"/file/downloadFile.do?id="+id;
		}
		//修改
		function edit(row){
			var id=row.id;
			var loginCode=window.parent.$('#loginCode').val();
			layer.open({
				type:2,
				title:'当前位置 : 上传文件',
				content:'findFileById.do?id='+id+'&loginCode='+loginCode,
				skin:'layui-layer-lan',
				area:['400px','300px'],
				maxmin:false,
				btn: ['关闭'],
				yes: function(index, layero){
					layer.close(index);
				  },
				cancel: function(index, layero){ 
					 layer.close(index);
					}   
				});  
		}
</script>
</head>
<body>
	<div id="tb" style="padding:15px;background-color: #ADD8E6;">
				<div>
					当前位置：文件管理
				</div>
				<div style="padding:15px">
					<span style="margin-left: 10%">文件名称:</span>
					<input id="fileName" class="easyui-textbox" type="text">
					<span style="margin-left: 10%">用户名:</span>
					<input id="loginCode" class="easyui-textbox" type="text">
					<a href="#" class="easyui-linkbutton" style="margin-left:20%;" iconCls="icon-search" onclick="doQuery()">查 询</a>
					<a href="#" class="easyui-linkbutton" plain="false" iconCls="icon-add" onclick="doAdd()">新 增</a>
				</div>
		</div>
		<table id="dg">
		</table>
</body>
</html>