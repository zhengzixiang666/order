package zzx.dao;

import org.springframework.stereotype.Repository;

import zzx.entity.ApplyMemberDTO;

@Repository("applyMemberDAO")
public interface ApplyMemberDAO extends BaseDAO<ApplyMemberDTO> {

}
