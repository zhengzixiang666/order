package zzx.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class BusOrderDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	
	private String orderCode;
	
	private Integer orderSumNumber;
	
	private Date orderDate;
	
	private String orderDesc;
	
	private Integer orderStatus;
	
	private Integer userId;
	
	private BigDecimal orderSumMoney;

	private BigDecimal payMoney;
	
	private Integer escStatus;//排除状态
	

	public Integer getEscStatus() {
		return escStatus;
	}

	public void setEscStatus(Integer escStatus) {
		this.escStatus = escStatus;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getOrderCode() {
		return orderCode;
	}

	public void setOrderCode(String orderCode) {
		this.orderCode = orderCode;
	}

	public Integer getOrderSumNumber() {
		return orderSumNumber;
	}

	public void setOrderSumNumber(Integer orderSumNumber) {
		this.orderSumNumber = orderSumNumber;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public String getOrderDesc() {
		return orderDesc;
	}

	public void setOrderDesc(String orderDesc) {
		this.orderDesc = orderDesc;
	}

	

	public Integer getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(Integer orderStatus) {
		this.orderStatus = orderStatus;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public BigDecimal getOrderSumMoney() {
		return orderSumMoney;
	}

	public void setOrderSumMoney(BigDecimal orderSumMoney) {
		this.orderSumMoney = orderSumMoney;
	}

	public BigDecimal getPayMoney() {
		return payMoney;
	}

	public void setPayMoney(BigDecimal payMoney) {
		this.payMoney = payMoney;
	}
	
	
	

}
