package zzx.enumutil;

public interface BaseIntegerEnum {
	
	public abstract String getDisplayName();
	
	public abstract Integer getValue();

}
