package zzx.service;

import java.util.List;
import java.util.Map;

import zzx.exception.HandException;

public interface BaseService<T> {
	
	/**
	 * 查询，带条件
	 */
	public List<T> selectListByMap(Map<String,Object> map);
	
	/**
	 * 通过id查询
	 */
	public T selectById(Integer id)throws HandException;
	
	/**
	 * 增加
	 */
	public void saveOne(T t)throws HandException;
	
	/**
	 * 修改
	 * 
	 */
	public void updateById(T t)throws HandException;
	
	/**
	 * 删除
	 * 
	 */
	public void deleteById(Integer id)throws HandException;
	
	/**
	 * 查询，带条件
	 */
	public List<T> selectListByDTO(T t);

}
