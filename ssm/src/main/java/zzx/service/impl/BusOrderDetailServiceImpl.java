package zzx.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import zzx.dao.BusOrderDetailDAO;
import zzx.entity.BusOrderDetailDTO;
import zzx.service.BusOrderDetailService;
@Service("busOrderDetailService")
public class BusOrderDetailServiceImpl extends BaseServiceImpl<BusOrderDetailDTO, BusOrderDetailDAO> implements BusOrderDetailService {

	@Resource
	private BusOrderDetailDAO busOrderDetailDAO;
	
	@Override
	public void updateByOrderCode(BusOrderDetailDTO t) {
		busOrderDetailDAO.updateByOrderCode(t);
	}

	

}
