package zzx.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import zzx.dao.RoleDAO;
import zzx.dao.RoleMenuDAO;
import zzx.entity.MenuDTO;
import zzx.entity.RoleDTO;
import zzx.entity.RoleMenuDTO;
import zzx.exception.HandException;
import zzx.service.RoleMenuService;
import zzx.util.ListUtil;
import zzx.util.StringUtil;

@Service("roleMenuService")
public class RoleMenuServiceImpl implements RoleMenuService {
	
	
	@Resource
	public RoleMenuDAO roleMenuDAO;
	
	@Resource
	public RoleDAO roleDAO;

	/**
	 * 
	 */
	public List<MenuDTO> findAllMenuDTOByRoleId(Integer roleid) throws HandException {
		if(roleid==null){
			throw new HandException("角色id为空，不能继续！");
		}
		List<RoleMenuDTO> roleMenuDTO=roleMenuDAO.getMenuDTOByRoleId(roleid);
		List<MenuDTO> menu=new ArrayList<MenuDTO>();
		if(ListUtil.isNotEmpty(roleMenuDTO)){
			for (RoleMenuDTO roleMenu : roleMenuDTO) {
				MenuDTO menuDTO=roleMenu.getMenuDTO();
				menu.add(menuDTO);
			}
		}
		return menu;
	}
	/**
	 * 保存或者删除角色菜单表
	 * @param roleid
	 * @param menuids
	 */
	public void saveOrDeleteRoleMenuDTO(Integer roleid, String menuids) throws HandException {
		//基础检验，不能为空
		if(roleid==null){
			throw new HandException("未获取角色id，不能继续！");
		}
		//根据角色查询角色表，如果角色表里面没有则说明没有这个角色，报错
		RoleDTO roleDTO=roleDAO.getRoleDTOByRoleId(roleid);
		if(roleDTO==null){
			throw new HandException("该角色不存在,不能继续");
		}
		List<RoleMenuDTO> roleMenuDTO=roleMenuDAO.getMenuDTOByRoleId(roleid);
		//如果菜单id，为空，根据角色id查询库里所有的菜单id，如果菜单不为空，则删除
		if(StringUtil.isBlank(menuids)){
			if(ListUtil.isNotEmpty(roleMenuDTO)){
				roleMenuDAO.deleteAllRoleMenuDTOByRoleid(roleid);
			}
		}
		//记录传进来有的菜单id，数据库里面没有的id，取传进来的id
		String jly="";
		//记录传进来没有的菜单id，数据库里面有的菜单id,取数据库的id
		String jlm="";
		if(StringUtil.isNotBlank(menuids)){
			String[] strs=menuids.split(",");
			if(ListUtil.isNotEmpty(roleMenuDTO)){
				for (RoleMenuDTO role : roleMenuDTO) {
					Integer menu=role.getMenuid();
					Integer men=null;
					boolean isex=false;
					if(strs!=null){
						for (String string : strs) {
							 men=Integer.valueOf(string);
							if(men.compareTo(menu)==0){
								isex=true;
								break;
							}
						}
						if(!isex){
							jlm+=menu+",";
						}
					}
				}
			}
			
			for (String str : strs) {
				if(StringUtil.isNotBlank(str)){
					Integer menuid=Integer.valueOf(str);
					if(ListUtil.isEmpty(roleMenuDTO)){
						roleMenuDAO.createRoleMenuDTO(roleid, menuid);
					}
					if(ListUtil.isNotEmpty(roleMenuDTO)){
						boolean isexit=false;
						Integer menus=null;
						for (RoleMenuDTO roleMenu : roleMenuDTO) {
							menus=roleMenu.getMenuid();
							if(menuid.compareTo(menus)==0){
								isexit=true;
								break;
							}
						}
						if(!isexit){
							jly+=menuid+",";
						}
					}
					
				}
			}
		}
		//记录传进来有的菜单id，数据库里面没有的id，取传进来的id:jly
		//记录传进来没有的菜单id，数据库里面有的菜单id,取数据库的id:jlm
		if(StringUtil.isNotBlank(jly)){
			//增加进来的
			String[] jlys= jly.split(",");
			if(jlys!=null && jlys.length>0){
				for (String jlyid : jlys) {
					Integer menuJlyid=Integer.valueOf(jlyid);
					roleMenuDAO.createRoleMenuDTO(roleid, menuJlyid);
				}
			}
		}
		if(StringUtil.isNotBlank(jlm)){
			//删除没有进来库里有的
			String[] jlms= jlm.split(",");
			if(jlms!=null && jlms.length>0){
				for (String jlmid : jlms) {
					Integer menuJlmid=Integer.valueOf(jlmid);
					roleMenuDAO.deleteRoleMenuDTO(roleid, menuJlmid);
				}
			}
		}
		
	}

	

}
