<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ include file="../common/common.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title></title>
<script type="text/javascript">

if ($.fn.pagination){
    $.fn.pagination.defaults.beforePageText = '第';
    $.fn.pagination.defaults.afterPageText = '共{pages}页';
    $.fn.pagination.defaults.displayMsg = '显示{from}到{to},共{total}记录';
}

//分页功能    
function pagerFilter(data) {
    if (typeof data.length == 'number' && typeof data.splice == 'function') {
        data = {
            total: data.length,
            rows: data
        }
    }
    var dg = $(this);
    var opts = dg.datagrid('options');
    var pager = dg.datagrid('getPager');
    pager.pagination({
        onSelectPage: function (pageNum, pageSize) {
            opts.pageNumber = pageNum;
            opts.pageSize = pageSize;
            pager.pagination('refresh', {
                pageNumber: pageNum,
                pageSize: pageSize
            });
            dg.datagrid('loadData', data);
        }
    });
    if (!data.originalRows) {
        if(data.rows)
            data.originalRows = (data.rows);
        else if(data.data && data.data.rows)
            data.originalRows = (data.data.rows);
        else
            data.originalRows = [];
    }
    var start = (opts.pageNumber - 1) * parseInt(opts.pageSize);
    var end = start + parseInt(opts.pageSize);
    data.rows = (data.originalRows.slice(start, end));
    return data;
}

	$(function() {
		$('#dg').datagrid({
			iconCls : 'icon-a_detail',
			fit : true,
			fitColumns : true,
			rownumbers : true,
			pagination : true,
			loadFilter: pagerFilter,
			checkOnSelect:true,
			border : false,
			striped : true,
			toolbar : '#tb',
			columns : [ [ {
				field : 'id',
				title : '类别id',
				checkbox:  true
			},{
				field : 'userName',
				title : '申请人',
				width : '20%',
				align : 'center'
			},{
				field : 'remark',
				title : '申请理由',
				width : '15%',
				align : 'center'
			},{
				field : 'applyDate',
				title : '申请日期',
				width : '10%',
				align : 'center',
				formatter : function(value){
                    var date = new Date(value);
                   return formatDate(date,"yyyy-MM-dd");
                }
			},{
				field : 'auditRemark',
				title : '审核备注',
				width : '15%',
				align : 'center'
			},{
				field : 'auditDate',
				title : '审核日期',
				width : '10%',
				align : 'center',
				formatter : function(value){
					if(value == null || value == ''){
						 return '';
						}else{
                    var date = new Date(value);
                   return formatDate(date,"yyyy-MM-dd");
						}
                }
			},{
				field : 'status',
				title : '状态',
				width : '15%',
				align : 'center',
				formatter : function(value){
                   if(value==0){
                	   return "已申请"
                   }
                   if(value==1){
                	   return "申请通过"
                   }
                   if(value==2){
                	   return "申请驳回"
                   }
                }
			},{
				field : 'field',
				title : '操作',
				width : '15%',
				align : 'center',
				formatter:function(value,row,index){
					if(row.status == '0'){
					var data="<a href='#' name='edit' onclick='edit(" + row.id + ")' title='审核'></a> ";
					return data;
						}else{
							return "";}
				}
				
			}]],
		onLoadSuccess:function(data){    
	        $("a[name='edit']").linkbutton({text:'审核',plain:false,iconCls:'icon-edit'}); 
	            
		}
		});
		reloadData();
	});
	//加载数据表格
	function reloadData(){
		var userId=$('#userId').val();
		var status=$('#status').val();
		if(userId==16 || userId==38){
			userId="";
		}
		$.ajax({
			url:'findDTO.do',
			type:'post',
			dataType:'json',
			data:{"userId":userId,"status":status},
			success:function(data){
				var json=data.data;
				success(json);
			},
			error:function(){
				alert('error');
			}
		});
	}
	//处理枚举类的显示，并加载表格
	function success(json){
		$('#dg').datagrid('loadData',json);
	}
	
	//查询
	function doQuery(){
		var userId=$('#userId').val();
		var status=$('#status').val();
		if(userId==16 || userId==38){
			userId="";
		}
		$.ajax({
			url:'findDTO.do',
			type:'post',
			data:{"userId":userId,"status":status},
			dataType:'json',
			success:function(data){
				var json=data.data;
				success(json);
			},
			error:function(){
				alert('请求失败');
			}
		});
	}
	//审核
	function edit(applyId){
		layer.open({
			type:2,
			title:'当前位置 : 会员审核',
			content:'findApply.do?applyId='+applyId,
			skin:'layui-layer-lan',
			area:['600px','300px'],
			maxmin:false,
			btn: ['关闭'],
			yes: function(index, layero){
				reloadData();
				layer.close(index);
			  },
			cancel: function(index, layero){ 
					reloadData();
					layer.close(index)
				}   
			});   
	}
	
</script>
</head>
<body>
<input type="hidden" id="userId" name="userId" value="${sessionScope.userId}">
	<div id="tb" style="padding:15px;background-color: #ADD8E6;">
			<div>
				当前位置：会员审核
			</div>
			<div style="padding:15px">
			<span >状态:
				<input id="status" name="status" class="easyui-combobox"   panelHeight="auto"
    						data-options="valueField:'otherid',textField:'name',url:'<%= this.getServletContext().getContextPath()%>/enum/findEnumDTOByEnumWord.do?enumWord=memberStatus'">
    						</input></span>
				<a href="#" class="easyui-linkbutton" style="margin-left:20px;" iconCls="icon-search" onclick="doQuery()">查 询</a>
			</div>
	</div>
	<table id="dg">
	</table>
<!-- 	<div id="dd"></div> -->
</body>
</html>