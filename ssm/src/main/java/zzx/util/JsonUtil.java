package zzx.util;

public class JsonUtil {
	
	public boolean success;//是否发送成功
	
	public String message;//错误信息
	
	public Object data;//对象信息
	
	public JsonUtil(){}

	public JsonUtil(boolean success, String message, Object data) {
		this.success = success;
		this.message = message;
		this.data = data;
	}
	
	public JsonUtil(String message){
		this.message = message;
	};

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}
	
	
	
	
	

}
