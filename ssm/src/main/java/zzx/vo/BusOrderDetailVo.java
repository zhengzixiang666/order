package zzx.vo;

import java.util.List;

import zzx.entity.BusOrderDetailDTO;

public class BusOrderDetailVo {
	
	private List<BusOrderDetailDTO> orderList;

	public List<BusOrderDetailDTO> getOrderList() {
		return orderList;
	}

	public void setOrderList(List<BusOrderDetailDTO> orderList) {
		this.orderList = orderList;
	}
	
	

}
