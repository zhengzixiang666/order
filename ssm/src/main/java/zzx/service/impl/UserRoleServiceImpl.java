package zzx.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;


import zzx.dao.UserRoleDAO;
import zzx.entity.RoleDTO;
import zzx.entity.UserRoleDTO;
import zzx.exception.HandException;
import zzx.service.UserRoleService;
import zzx.util.ListUtil;

@Service("userRoleService")
public class UserRoleServiceImpl implements UserRoleService {
	
	@Resource
	public UserRoleDAO userRoleDAO;
	
	/**
	 * 根据用户id,查询对应的所有角色
	 * @throws HandException 
	 */
	public List<RoleDTO> findAllRoleDTOByUserId(Integer userId) throws HandException{
		if(userId==null){
			throw new HandException("用户id为空，不能继续！");
		}
		List<RoleDTO> roleDTOList=new ArrayList<RoleDTO>();
		List<UserRoleDTO> userRoleDTOList=userRoleDAO.getUserRoleDTOByUserId(userId);
		if(ListUtil.isNotEmpty(userRoleDTOList)){
			for (UserRoleDTO userRoleDTO : userRoleDTOList) {
				RoleDTO roleDTO= userRoleDTO.getRoleDTO();
				roleDTOList.add(roleDTO);
			}
		}
		return roleDTOList;
	}

}
