<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ include file="common/common.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>西财商场会员管理系统</title>
<style type="text/css">
.panel-header{
	background: linear-gradient(to bottom,#85bacb 0,#85bacb 100%);
}
</style>
<script type="text/javascript">
	var nodeid="";
	$(function() {
		var userId=$('#userId').val();
		 $.getJSON("menu/findParentMenuDTOByUserId.do?userId="+userId, function (result) {
			 	if(result.data){
			 		aa(result.data[0].id);
			        $.each(result.data, function (i, field) { 
			            $("#cs").append("<li style=\" display:inline;margin-top:5px;margin-left:50px;\"><a href=\"#\" class=\"\" style=\"color:white;\" onclick=\"aa(" + field.id + ");\">" + field.text + "</a></li>");  
			        });  
			 	}else{
			 		layer.alert(result.message,{
						skin: 'layui-layer-molv',
						yes:function(){
							window.location.href="login.jsp";
						},
						cancel:function(){
							window.location.href="login.jsp";
						}
					});
			 	}
		    });  
		 //获取有用的菜单
		 $.ajax({
				url:'menu/findAllMenuDTOByUserId.do',
				type:'post',
				data:{"userId":userId},
				dataType:'json',
				success:function(json){
					var dd=json.data;
					for ( var i in dd) {
						nodeid+=dd[i].id+",";
					}
				},
				error:function(){
					var path=$('#path').val();
					window.location.href=path+"/login.jsp";
				}
			});
	});
	function aa(parentId){
		
		//根据父类id查询第一个出现的子菜单
		 $.ajax({
				url:'menu/findFirstChildrenByParentId.do',
				type:'post',
				data:{"parentId":parentId},
				dataType:'json',
				success:function(json){
					var dd=json.data;
					$('#iframe1').attr('src',dd.url);
				
				},
				error:function(){
					var path=$('#path').val();
					window.location.href=path+"/login.jsp";
				}
			});
		$('#MyTree').tree({
			checkbox : false,
			url : 'menu/findChildrenByParentId.do?parentId='+parentId,
			loadFilter : function(data) {
				return data.data;
			},
			onClick : function(node) {
				if(node.isleaf==2){
					return;
				}
				var url=node.url;
				$('#iframe1').attr('src',url);
			},
			onLoadSuccess:function(node,data){
				//发送ajax，获取角色id,得到的有权限的
				var root=$(this).tree('getRoots');
				var c=$(this).tree('getChildren',root);
				//删除没有的菜单
				if(nodeid=='' || nodeid==null){
					for ( var i in c) {
						var not = $('#MyTree').tree('find', c[i].id);
						if(not){
							$('#MyTree').tree('remove', not.target);
						}
					}
				}else{
					var nos=nodeid.split(",");
					var bxtid="";
					for ( var i in c) {
						var xtid=c[i].id ;
						var isfalse=false;
						for(var j in nos){
							if(xtid == nos[j]){
								isfalse=true;
								break;
							}
						}
						if(!isfalse){
							var not = $('#MyTree').tree('find', xtid);
							if(not){
								$('#MyTree').tree('remove', not.target);
							}
						}
					}
				}
			}
		});
		
	}
	
	//退出登陆
	function doLogout(){
		var path=$('#path').val();
		window.location.href=path+"/login.jsp";
	}
</script>
</head>
<body class="easyui-layout">
	<input id="path" type="hidden" value="<%= this.getServletContext().getContextPath()%>"/>
	<input id="userId" value="${param.userId}" name="userId"/>
	<input id="loginCode" value="${param.loginCode}" name="loginCode"/>
	<div region="north" style="background-color: #104E8B;height: 12%">
				<ul class="" style="float: left;margin-top: 30px" id="cs">
				</ul>
				
				<ul class="" style="float: right;margin-top: 28px">
					<li style="float:right;color:white; margin-top:5px;margin-left:10px;" title="你好! ${param.loginCode}">
					你好! ${param.loginCode}
					<a href="#" class="" style="color:white;margin-left: 20px;margin-right: 20px;" onclick="doLogout()">注销</a>
					</li>
				</ul>
	</div>
	<div region="west"  title="功能菜单" split="true" style="width: 15%;background-color: #ADD8E6; height: 100%; ">
			<ul id="MyTree" class="easyui-tree" style="color: black;">
			</ul>
	</div>
	<div region="center">
		<div id="cc" class="easyui-layout" style="width:100%;height:100%;background-color: white;">
			<iframe id="iframe1" style="height: 100%;width: 100%" frameborder="0"   src="" ></iframe>
		</div>
	</div>
	
	
</body>
</html>