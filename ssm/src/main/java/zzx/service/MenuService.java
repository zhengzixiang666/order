package zzx.service;

import java.util.List;

import zzx.entity.MenuDTO;
import zzx.exception.HandException;
/**
 * 菜单服务层
 * @author 郑仔祥
 *
 */
public interface MenuService {
	
	public abstract List<MenuDTO> findAllMenuDTO();
	
	public abstract List<MenuDTO> findParentMenuDTO();

	public abstract List<MenuDTO> findChildrenByParentId(Integer parentId);
	
	/**
	 * 根据用户id，查询所有一级父菜单
	 * @param userId
	 * @return
	 */
	public abstract List<MenuDTO> findParentMenuDTOByUserId(Integer userId)throws HandException;
	
	public abstract List<MenuDTO> findAllMenuDTOByUserId(Integer userId)throws HandException;
	
	public MenuDTO findMenuDTOByMenuId(Integer id)throws HandException;

	/**
	 * 新增菜单
	 * @param menuDTO
	 * @throws HandException
	 */
	public abstract void createMenuDTOByMenuDTO(MenuDTO menuDTO)throws HandException;

	/**
	 * 修改菜单
	 * @param menuDTO
	 * @throws HandException
	 */
	public abstract void updateMenuDTOByMenuId(MenuDTO menuDTO)throws HandException;

	public abstract void deleteMenuDTOByMenuId(Integer id)throws HandException;

	/**
	 * 根据父id查询第一个子菜单
	 * @param parentId
	 * @return
	 * @throws HandException
	 */
	public abstract MenuDTO findFirstChildrenByParentId(Integer parentId)throws HandException;
	
}
