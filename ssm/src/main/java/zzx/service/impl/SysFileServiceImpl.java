package zzx.service.impl;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import zzx.dao.SysFileDAO;
import zzx.entity.SysFileDTO;
import zzx.exception.HandException;
import zzx.service.SysFileService;
import zzx.util.ListUtil;
import zzx.util.ZzxUUIDUtil;

@Service(value="sysFileService")
public class SysFileServiceImpl implements SysFileService {
	
	@Resource
	private SysFileDAO sysFileDAO;

	/**
	 * 上传文件
	 */
	@Override
	public void createFile(SysFileDTO sysFileDTO) {
		
		//文件名不能为空
		if(StringUtils.isBlank(sysFileDTO.getFileName())){
			throw new HandException("文件名不能为空，不能继续！");
		}
		//文件后缀名不能为空
		if(StringUtils.isBlank(sysFileDTO.getSuffixName())){
			throw new HandException("文件后缀名不能为空，不能继续！");
		}
		//文件类型不能为空
		if(StringUtils.isBlank(sysFileDTO.getFileType())){
			throw new HandException("文件类型不能为空，不能继续！");
		}
		//文件字节不能为空
		if(sysFileDTO.getDocument()==null){
			throw new HandException("文件字节不能为空，不能继续！");
		}
		//文件名称不能重复
		List<SysFileDTO> list=sysFileDAO.getFileByFileName(sysFileDTO.getFileName());
		if(ListUtil.isNotEmpty(list)){
			throw new HandException("文件名称已经存在，不能继续！");
		}
		sysFileDTO.setId(ZzxUUIDUtil.getUUID());
		sysFileDTO.setCreateTime(new Date());
		sysFileDAO.uploadFile(sysFileDTO);
		
	}

	@Override
	public List<SysFileDTO> findAllSysFileDTO(SysFileDTO sysFileDTO) {
		return sysFileDAO.getFileListNoDocument(sysFileDTO);
	}

	@Override
	public void deleteFileById(String id) throws HandException {
		if(StringUtils.isBlank(id)){
			throw new HandException("文件id为空，不能继续！");
		}
		sysFileDAO.deleteFileById(id);
	}

	@Override
	public void updateFileById(SysFileDTO sysFileDTO) throws HandException {
		//id不能为空，文件名不能为空，不能修改库里存在的文件名
		if(StringUtils.isBlank(sysFileDTO.getId())){
			throw new HandException("文件id为空，不能继续！");
		}
		//文件名不能为空
		if(StringUtils.isBlank(sysFileDTO.getFileName())){
			throw new HandException("文件名不能为空，不能继续！");
		}
		List<SysFileDTO> list=sysFileDAO.getFileByFileName(sysFileDTO.getFileName());
		if(ListUtil.isNotEmpty(list)){
			//名字相同id不同，不是同一个文件
			for (SysFileDTO sysFileDTO2 : list) {
				if(!sysFileDTO2.getId().equals(sysFileDTO.getId())){
					throw new HandException("文件名称已经存在，不能继续！");
				}
			}
		}
		//更新
		sysFileDTO.setCreateTime(new Date());
		sysFileDAO.updateFileById(sysFileDTO);
		
	}

	@Override
	public SysFileDTO downloadFileById(String id) throws HandException {
		if(StringUtils.isBlank(id)){
			throw new HandException("文件id为空，不能继续！");
		}
		
		return sysFileDAO.getFileById(id);
	}

	@Override
	public SysFileDTO findFileById(String id) throws HandException {
		if(StringUtils.isBlank(id)){
			throw new HandException("文件id为空，不能继续！");
		}
		return sysFileDAO.getFileById(id);
	}

	
}
