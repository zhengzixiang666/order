<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ include file="../common/common.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>新增用户</title>
<script type="text/javascript">
	function doSave(){
		$('#form').form('submit',{
			url:'add.do',
			success:function(data){
				var data = eval('(' + data + ')');//将json对象转为js对象
				layer.alert(data.message,{
					skin: 'layui-layer-molv',
					yes:function(){
						location.reload();
					}
				});
			}
		});
		
	}
// 	function format(date){
// 		 return formatDate(date,"yyyy-MM-dd");
// 	}
</script>
</head>
<body>
	<div style="margin-top: 20px">
	   	<form id="form" method="post" >
		    	<table align="center">
		    	
		    			<#if cfg.addList?exists>
					       <#list table.fields as field> 
					          <#list cfg.addList as searchs>
					        	  <#if field.propertyName==searchs>
					        	       <#if cfg.maps[searchs] =="text">
						                 <tr>
							    			<td align="right"><label>${field.comment}：<input id="${field.propertyName}" name="${field.propertyName}"  class="easyui-textbox" type="text" ></input></label></td>
							    		</tr>
								        </#if>
								        <#if cfg.maps[searchs] =="date">
						                 <tr>
							    			<td align="right"><label>${field.comment}： <input class="easyui-datetimebox" id="${field.propertyName}" name="${field.propertyName}"
                                           data-options="required:true,showSeconds:false"  style="width:150px"></label></td>
							    		</tr>
								        </#if>
								        <#if cfg.maps[searchs] =="select">
								         <tr>
							    			<td align="right"><label>${field.comment}：<input id="${field.propertyName}" name="${field.propertyName}" class="easyui-combobox"  panelHeight="auto"
    											data-options="valueField:'otherid',textField:'name',url:'<%= this.getServletContext().getContextPath()%>/enum/findEnumDTOByEnumWord.do?enumWord=${field.propertyName}'"></input></label></td>
							    		</tr>
								        </#if>
					        	  </#if>
					        </#list>
				     	 </#list>
				      </#if>
		    	</table>
		   </form>
	   </div>
</body>
</html>