package zzx.dao;

import org.springframework.stereotype.Repository;

import zzx.entity.XssScriptDTO;

@Repository("xssScriptDAO")
public interface XssScriptDAO extends BaseDAO<XssScriptDTO>{

	
}
