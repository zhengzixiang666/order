<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ include file="../common/common.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>新增用户</title>
<script type="text/javascript">
	function doSave(){
		$('#form').form('submit',{
			url:'add.do',
			success:function(data){
				layer.alert("新增成功",{
					skin: 'layui-layer-molv',
					yes:function(){
						location.reload();
					}
				});
				
			}
		});
		
	}
// 	function format(date){
// 		 return formatDate(date,"yyyy-MM-dd");
// 	}
</script>
</head>
<body>
	<div style="margin-top: 20px">
	   	<form id="form" method="post" >
		    	<table align="center">
		    		<tr>
		    			<td align="left"><label>XSS脚本：<input id="xssScript" name="xssScript" class="easyui-textbox" style="width:500px;height:150px" type="text"></input></label></td>
		    		</tr>
		    		<tr>
		    			<td colspan="2"><div align="right"><a href="#" class="easyui-linkbutton" plain="false" iconCls="icon-add" onclick="doSave()">保 存</a> </div></td>
		    		</tr>
		    	</table>
		   </form>
	   </div>
</body>
</html>