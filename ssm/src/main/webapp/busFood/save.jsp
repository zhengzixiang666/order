<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ include file="../common/common.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title></title>
<script type="text/javascript">

if ($.fn.pagination){
    $.fn.pagination.defaults.beforePageText = '第';
    $.fn.pagination.defaults.afterPageText = '共{pages}页';
    $.fn.pagination.defaults.displayMsg = '显示{from}到{to},共{total}记录';
}

//分页功能    
function pagerFilter(data) {
    if (typeof data.length == 'number' && typeof data.splice == 'function') {
        data = {
            total: data.length,
            rows: data
        }
    }
    var dg = $(this);
    var opts = dg.datagrid('options');
    var pager = dg.datagrid('getPager');
    pager.pagination({
        onSelectPage: function (pageNum, pageSize) {
            opts.pageNumber = pageNum;
            opts.pageSize = pageSize;
            pager.pagination('refresh', {
                pageNumber: pageNum,
                pageSize: pageSize
            });
            dg.datagrid('loadData', data);
        }
    });
    if (!data.originalRows) {
        if(data.rows)
            data.originalRows = (data.rows);
        else if(data.data && data.data.rows)
            data.originalRows = (data.data.rows);
        else
            data.originalRows = [];
    }
    var start = (opts.pageNumber - 1) * parseInt(opts.pageSize);
    var end = start + parseInt(opts.pageSize);
    data.rows = (data.originalRows.slice(start, end));
    return data;
}

	$(function() {
		$('#dg').datagrid({
			iconCls : 'icon-a_detail',
			fit : true,
			fitColumns : true,
			rownumbers : true,
			pagination : true,
			loadFilter: pagerFilter,
			checkOnSelect:true,
			border : false,
			striped : true,
			toolbar : '#tb',
			columns : [ [ {
				field : 'id',
				title : '类别id',
				checkbox:  true
			},{
				field : 'foodCode',
				title : '商品编号',
				width : '15%',
				align : 'center'
			},{
				field : 'name',
				title : '商品名称',
				width : '15%',
				align : 'center'
			},{
				field : 'money',
				title : '价格',
				width : '15%',
				align : 'center'
			},{
				field : 'factory',
				title : '厂家',
				width : '15%',
				align : 'center'
			},{
				field : 'factoryDate',
				title : '生产日期',
				width : '15%',
				align : 'center',
				formatter : function(value){
                    var date = new Date(value);
                   return formatDate(date,"yyyy-MM-dd");
                }
			},{
				field : 'field',
				title : '操作',
				width : '25%',
				align : 'center',
				formatter:function(value,row,index){
					var data=" <a href='#' name='edit' onclick='edit(" + row.id + ")' title='修改'></a> "
					+"\| <a href='#' name='deleteRow' class='easyui-linkbutton' onclick='deleteRow(" + row.id + ")' title='删除'></a>";
					return data;
				}
				
			}]],
			onLoadSuccess:function(data){    
		        $("a[name='edit']").linkbutton({text:'修改',plain:false,iconCls:'icon-edit'}); 
		        $("a[name='deleteRow']").linkbutton({text:'删除',plain:false,iconCls:'icon-cancel'});    
			}
		});
		reloadData();
	});
	//加载数据表格
	function reloadData(){
		$.ajax({
			url:'findAllDTO.do',
			type:'post',
			dataType:'json',
			success:function(data){
				var json=data.data;
				success(json);
			},
			error:function(){
				alert('error');
			}
		});
	}
	//处理枚举类的显示，并加载表格
	function success(json){
		
		$('#dg').datagrid('loadData',json);
	}
	
	//查询
	function doQuery(){
		var foodCode=jQuery('#foodCode').val();
		var name=jQuery('#name').val();
		$.ajax({
			url:'findDTO.do',
			type:'post',
			data:{"foodCode":foodCode,"name":name},
			dataType:'json',
			success:function(data){
				var json=data.data;
				success(json);
			},
			error:function(){
				alert('请求失败');
			}
		});
	}
	//新增
	function doAdd(){
		layer.open({
			type:2,
			title:'当前位置 : 新增',
			content:'addMain.do',
			skin:'layui-layer-lan',
			area:['400px','350px'],
			maxmin:false,
			btn: ['关闭'],
			yes: function(index, layero){
				reloadData();
				layer.close(index);
			  },
			cancel: function(index, layero){ 
					reloadData();
					layer.close(index)
				}   
			});   
	}
	//修改方法
	function edit(id){
		layer.open({
			type:2,
			title:'当前位置 : 修改',
			content:'editMain.do?id='+id,
			skin:'layui-layer-lan',
			area:['600px','350px'],
			maxmin:false,
			btn: ['关闭'],
			yes: function(index, layero){
				reloadData();
				layer.close(index);
			  },
			cancel: function(index, layero){ 
				 reloadData();
				  layer.close(index)
				}   
			}); 
	}

	//删除方法
	function deleteRow(id){
		$.ajax({
			type:'post',
			data:{"id":id},
			url:'delete.do',
			dataType:'json',
			success:function(json){
				reloadData();
				layer.alert(json.message);
			},
			error:function(){
				layer.alert("删除失败");
			}
		});
	}
	
	
</script>
</head>
<body>
	<div id="tb" style="padding:15px;background-color: #ADD8E6;">
			<div>
				当前位置：商品列表
			</div>
			<div style="padding:15px">
				<span style="margin-left: 10%">商品编号:</span>
				<input id="foodCode" name="foodCode" class="easyui-textbox" type="text">
				<span style="margin-left: 10%">商品名称:</span>
				<input id="name" name="name" class="easyui-textbox" type="text">
				<a href="#" class="easyui-linkbutton" style="margin-left:20%;" iconCls="icon-search" onclick="doQuery()">查 询</a>
				<a href="#" class="easyui-linkbutton" plain="false" iconCls="icon-add" onclick="doAdd()">新 增</a>
			</div>
	</div>
	<table id="dg">
	</table>
<!-- 	<div id="dd"></div> -->
</body>
</html>