package zzx.controller;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import zzx.entity.BusFoodDTO;
import zzx.exception.HandException;
import zzx.service.BusFoodService;
import zzx.util.CodeUtil;
import zzx.util.JsonUtil;
import zzx.vo.BusOrderDetailVo;

@Controller //这个注解正式时使用
@RequestMapping("busFood")
public class BusFoodController {
	
	@Resource
	public BusFoodService busFoodService;
	

	@RequestMapping("/save")
	public String type(){
		return "busFood/save";
	}
	
	@RequestMapping("/list")
	public String list(){
		return "busFood/list";
	}
	
	@RequestMapping("/findAllDTO")
	@ResponseBody
	public JsonUtil findAllDTO(){
		List<BusFoodDTO> list=busFoodService.selectListByDTO(null);
		JsonUtil json=new JsonUtil(true,"发送成功",list);
		return json;
	};
	
	@RequestMapping("/findDTO")
	@ResponseBody
	public JsonUtil findDTO(BusFoodDTO busFoodDTO){
		List<BusFoodDTO> list=busFoodService.selectListByDTO(busFoodDTO);
		JsonUtil json=new JsonUtil(true,"查询成功",list);
		return json;
	}
	
	
	@RequestMapping("/addMain")
	public String addMain(){
		
		return "busFood/addMain";
	}
	
	
	@RequestMapping("/add")
	@ResponseBody
	public JsonUtil add(BusFoodDTO busFoodDTO){
		JsonUtil json=new JsonUtil();
		try {
			busFoodDTO.setFoodCode(CodeUtil.getCode());
			busFoodDTO.setCreateDate(new Date());
			busFoodService.saveOne(busFoodDTO);
			json.setMessage("新增成功！");
		} catch (HandException e) {
			e.printStackTrace();
			json.setMessage(e.getMessage());
			return json;
		}
		return json;
	}
	
	@RequestMapping("/doSubmit")
	@ResponseBody
	public JsonUtil doSubmit(@RequestBody BusOrderDetailVo list){
		JsonUtil json=new JsonUtil();
		try {
			busFoodService.saveOrderAndDetail(list);
			json.setMessage("新增成功！");
		} catch (HandException e) {
			e.printStackTrace();
			json.setMessage(e.getMessage());
			return json;
		}
		return json;
	}
	
	
	/**
	 * 通过用户id，查询用户信息
	 * @param session
	 * @param userId
	 * @return
	 */
	@RequestMapping("/editMain")
	public ModelAndView editMain(@RequestParam Integer id,ModelAndView mv){
		mv.setViewName("busFood/editMain");
		try {
			BusFoodDTO busFoodDTO=busFoodService.selectById(id);
			mv.addObject("editDTO", busFoodDTO);
		} catch (HandException e) {
			e.printStackTrace();
			return mv;
		}
		return mv;
	}
	
	/**
	 * 更新用户并且分配角色
	 */
	@RequestMapping("/update")
	@ResponseBody
	public JsonUtil update(BusFoodDTO busFoodDTO){
		JsonUtil json=new JsonUtil();
		try {
			busFoodService.updateById(busFoodDTO);
			json.setMessage("更新成功！");
		} catch (HandException e) {
			e.printStackTrace();
			json.setMessage(e.getMessage());
			return json;
		}
		return json;
	}
	
	@RequestMapping("/delete")
	@ResponseBody
	public JsonUtil delete(@RequestParam Integer id){
		
		try {
			busFoodService.deleteById(id);
			return new JsonUtil(true,"删除成功",null);
		} catch (HandException e) {
			e.printStackTrace();
			return new JsonUtil(false,e.getMessage(),null);
		}
		
	}

}
