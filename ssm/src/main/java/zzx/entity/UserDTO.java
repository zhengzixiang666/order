package zzx.entity;

import java.util.Date;


/**
 * 用户实体类tur_user
 * @author 郑仔祥
 *
 */
public class UserDTO {

	private Integer userId;// 用户id

	private String loginCode;//登陆账号

	private String loginPassWord;//登陆密码

	private Integer isUsed;//是否启用，1启用，2不启用

	private String attrBute;// 用户属性LIST 1|超级管理员/2|管理员/3|操作员

	private String roleStr;//角色名称列表

	private String ext;// 备用字段
	
	private String bind;//是否绑定1，已绑定，2，未绑定
	
	
	private Date createdate;//创建日期
	
	private String userType;
	
	private String points;
	
	private String discount;
	

	public String getBind() {
		return bind;
	}



	public void setBind(String bind) {
		this.bind = bind;
	}



	public UserDTO() {
	}



	public UserDTO(Integer userId, String loginCode, String loginPassWord, Integer isUsed, String attrBute,
			String roleStr, String ext, Date createdate) {
		super();
		this.userId = userId;
		this.loginCode = loginCode;
		this.loginPassWord = loginPassWord;
		this.isUsed = isUsed;
		this.attrBute = attrBute;
		this.roleStr = roleStr;
		this.ext = ext;
		this.createdate = createdate;
	}


	public Date getCreatedate() {
		return createdate;
	}



	public void setCreatedate(Date createdate) {
		this.createdate =createdate;
	}



	public Integer getUserId() {
		return userId;
	}




	public void setUserId(Integer userId) {
		this.userId = userId;
	}




	public String getLoginCode() {
		return loginCode;
	}




	public void setLoginCode(String loginCode) {
		this.loginCode = loginCode;
	}




	public String getLoginPassWord() {
		return loginPassWord;
	}




	public void setLoginPassWord(String loginPassWord) {
		this.loginPassWord = loginPassWord;
	}




	public Integer getIsUsed() {
		return isUsed;
	}




	public void setIsUsed(Integer isUsed) {
		this.isUsed = isUsed;
	}




	public String getAttrBute() {
		return attrBute;
	}




	public void setAttrBute(String attrBute) {
		this.attrBute = attrBute;
	}




	public String getRoleStr() {
		return roleStr;
	}




	public void setRoleStr(String roleStr) {
		this.roleStr = roleStr;
	}




	public String getExt() {
		return ext;
	}




	public void setExt(String ext) {
		this.ext = ext;
	}



	public String getUserType() {
		return userType;
	}



	public void setUserType(String userType) {
		this.userType = userType;
	}



	public String getPoints() {
		return points;
	}



	public void setPoints(String points) {
		this.points = points;
	}



	public String getDiscount() {
		return discount;
	}



	public void setDiscount(String discount) {
		this.discount = discount;
	}



	@Override
	public String toString() {
		return "UserDTO [userId=" + userId + ", loginCode=" + loginCode + ", loginPassWord=" + loginPassWord
				+ ", isUsed=" + isUsed + ", attrBute=" + attrBute + ", roleStr=" + roleStr + ", ext=" + ext
				+ ", createdate=" + createdate + "]";
	}




	
	

}
