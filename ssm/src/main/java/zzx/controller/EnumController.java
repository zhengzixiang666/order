package zzx.controller;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import zzx.entity.EnumDTO;
import zzx.service.EnumService;

/**
 * 枚举控制层
 * @author 郑仔祥
 *
 */
@Controller
@RequestMapping("enum")
public class EnumController {
	
	@Resource
	public EnumService enumService;
	
	@RequestMapping("/findEnumDTOByEnumWord")
	@ResponseBody
	public List<EnumDTO> findEnumDTOByEnumWord(@RequestParam String enumWord){
		return enumService.findEnumDTOByEnumWord(enumWord);
	} 

}
