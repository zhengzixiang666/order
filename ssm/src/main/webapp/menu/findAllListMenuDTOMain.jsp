<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ include file="../common/common.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<style type="text/css">
</style>
<script type="text/javascript">
	$(function() {
		$('#MyTree').tree({
			checkbox : false,
			url : 'findAllMenuDTO.do',
			loadFilter : function(data) {
				return data.data;
			},
			onLoadSuccess:function(node,data){
			}
		});
	});
	//新增菜单
	function doAdd(){
		var select= $('#MyTree').tree('getSelected');
		if(!select){
			layer.alert("未选中节点,不能新增");
		}
		var parentid=select.parentid;
		var id=select.id;
		layer.open({
			type:2,
			title:'当前位置 : 新增菜单',
			content:'addMenuDTOMain.do?parentid='+parentid+'&id='+id,
			skin:'layui-layer-lan',
			area:['400px','520px'],
			maxmin:false,
			btn: ['关闭'],
			yes: function(index, layero){
				layer.close(index);
			  },
			cancel: function(index, layero){ 
				 layer.close(index);
				}   
			});   
	}
	//修改菜单
	function doEdit(){
		var select= $('#MyTree').tree('getSelected');
		if(!select){
			layer.alert("未选中节点,不能修改");
		}
		var id=select.id;
		layer.open({
			type:2,
			title:'当前位置 : 修改菜单',
			content:'findMenuDTOByMenuId.do?id='+id,
			skin:'layui-layer-lan',
			area:['400px','500px'],
			maxmin:false,
			btn: ['关闭'],
			yes: function(index, layero){
				layer.close(index);
			  },
			cancel: function(index, layero){ 
				 layer.close(index);
				}   
			}); 
	}
	function doDelete(){
		var select= $('#MyTree').tree('getSelected');
		if(!select){
			layer.alert("未选中节点,不能修改");
		}
		var id=select.id;
		layer.confirm('确定删除吗?', {
			  btn: ['确定','取消']
			},function(){
				$.ajax({
					type:'post',
					data:{"id":id},
					url:'deleteMenuDTOByMenuId.do',
					dataType:'json',
					success:function(json){
						layer.alert(json.message,{
							skin: 'layui-layer-molv',
							yes:function(index, layero){
								window.parent.layer.close(index);
								location.reload();
							}
						});
						
					},
					error:function(json){
						layer.alert(json.message);
					}
				});
			},function(){
				
			});
	}
</script>
</head>
<body>
	<div style="overflow-x:hidden;overflow-y:auto;width:100%;height:550px;">
		<a href="#" class="easyui-linkbutton" style="float: right;" iconCls="icon-cancel" onclick="doDelete()">删除</a>
		<a href="#" class="easyui-linkbutton" style="float: right;" iconCls="icon-edit" onclick="doEdit()">修改</a>
		<a href="#" class="easyui-linkbutton" style="float: right;" iconCls="icon-add" onclick="doAdd()">新增</a>
	
		<div region="west" title="导航菜单" split="true"
			style="width: 15%; height: 100%;">
			<ul id="MyTree" class="easyui-tree" style="color: black;">
			</ul>
		</div>
	</div>
</body>
</html>