package zzx.enumutil;

public enum YesNoEnum implements BaseIntegerEnum {

	YES(1,"是"),
	NO(2,"否");
	
	public Integer value;
	
	public String dispalyName;
	
	//根据值获取枚举
	public static YesNoEnum getYesNoEnumByValue(String value){
		for (YesNoEnum yesNoEnum : YesNoEnum.values()) {
			if(yesNoEnum.value.equals(value)){
				return yesNoEnum;
			}
		}
		return null;
	}
	//根据名称获取枚举
	public static YesNoEnum getYesNoEnumByName(String displayName){
		for (YesNoEnum yesNoEnum : YesNoEnum.values()) {
			if(yesNoEnum.dispalyName.equals(displayName)){
				return yesNoEnum;
			}
		}
		return null;
	}
	
	
	private YesNoEnum(Integer value, String dispalyName) {
		this.value = value;
		this.dispalyName = dispalyName;
	}
	public String getDisplayName() {
		
		return this.dispalyName;
	}
	public Integer getValue() {
		return this.value;
	}



}
