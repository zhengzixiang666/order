<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ include file="../common/common.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>修改用户</title>
<script type="text/javascript">
	//执行更新方法
	function doUpdate(){
			$('#form').form('submit',{
				url:'auditApply.do?',
				success:function(data){
					var data = eval('(' + data + ')');//将json对象转为js对象
					layer.alert(data.message,{
						skin: 'layui-layer-molv'
					});
				}
			});
	}
	//驳回
	function doBohui(){
		$('#form').form('submit',{
			url:'rejectApply.do',
			success:function(data){
				var data = eval('(' + data + ')');//将json对象转为js对象
				layer.alert(data.message,{
					skin: 'layui-layer-molv'
				});
			}
		});
}
	
</script>
</head>
<body>
	<div style="margin-top: 20px">
	   	<form id="form" method="post" >
		    	<table align="center">
		    		<tr>
		    			<td>
		    			<input type="hidden" id="id" name="id" value="${applyMemberDTO.id}"/>
		    			<input type="hidden" id="level" name="level" value="${applyMemberDTO.level}"/>
		    			</td>
		    		</tr>
		    		<tr>
		    			<td align="right"><label>申请原因：<input id="remark" name="remark" value="${applyMemberDTO.remark}" class="easyui-textbox" type="text" disabled></input></label></td>
		    		</tr>
		    		<tr>
		    			<td align="right"><label>会员等级：<input id="level" disabled name="level1" value="${applyMemberDTO.level}" class="easyui-combobox"  panelHeight="auto"
		    			 data-options="valueField:'otherid',textField:'name',url:'<%= this.getServletContext().getContextPath()%>/enum/findEnumDTOByEnumWord.do?enumWord=memberLevel'"></input></label></td>
		    		</tr>
		    		<tr>
		    			<td align="right"><label>审核意见：<input id="auditRemark" name="auditRemark" value="" class="easyui-textbox" type="text" ></input></label></td>
		    		</tr>
		    		
		    		<tr>
		    			<td colspan="2">
		    			<div align="right"><a href="#" class="easyui-linkbutton" plain="false" iconCls="icon-add" onclick="doUpdate()">审核通过</a>
		    			<a href="#" class="easyui-linkbutton" plain="false" iconCls="icon-add" onclick="doBohui()">驳回申请</a> </div>
		    			</td>
		    		</tr>
		    	</table>
		   </form>
	   </div>
</body>
</html>