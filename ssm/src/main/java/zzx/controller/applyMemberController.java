package zzx.controller;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import zzx.entity.ApplyMemberDTO;
import zzx.entity.EnumDTO;
import zzx.entity.RoleDTO;
import zzx.entity.UserDTO;
import zzx.exception.HandException;
import zzx.service.ApplyMemberService;
import zzx.service.EnumService;
import zzx.service.UserService;
import zzx.util.JsonUtil;

@Controller //这个注解正式时使用
@RequestMapping("apply")
public class applyMemberController {
	
	@Resource
	public ApplyMemberService applyMemberService;
	
	@Resource
	public UserService userService;
	
	@Resource
	public EnumService enumService;
	
	@RequestMapping("/list")
	public String mylist(){
		return "apply/list";
		
	}
	@RequestMapping("/auditlist")
	public String myauditlist(){
		return "apply/auditlist";
		
	}
	@RequestMapping("/auditMain")
	public String auditMain(){
		return "apply/auditApply";
		
	}
	@RequestMapping("/findDTO")
	@ResponseBody
	public JsonUtil findDTO(ApplyMemberDTO ApplyMemberDto){
		List<ApplyMemberDTO> list=applyMemberService.selectListByDTO(ApplyMemberDto);
		JsonUtil json=new JsonUtil(true,"查询成功",list);
		return json;
	}
	
	/**
	 * 新增申请的主页面
	 * @return
	 */
	@RequestMapping("/addApplyMain")
	public String addUserDTOMain(){
		
		return "apply/addApplyDTOMain";
	}
	
	/**
	 * 新增申请
	 * @param userDTO
	 * @return
	 */
	@RequestMapping("/addApplyMemberDTO")
	@ResponseBody
	public JsonUtil addUserDTO(ApplyMemberDTO applyMemberDto){
		JsonUtil json=new JsonUtil();
		try {
			ApplyMemberDTO dto = new ApplyMemberDTO();
			dto.setUserId(applyMemberDto.getUserId());
			 UserDTO d = userService.findUserDTOById(applyMemberDto.getUserId());
			 applyMemberDto.setUserName(d.getLoginCode());
			List<ApplyMemberDTO> list=applyMemberService.selectListByDTO(dto);
			if(list.size() !=0 && list.get(0).getStatus() == 0) {
				return new JsonUtil(false,"已提交申请，请勿重复申请！",null);
			}
			applyMemberDto.setApplyDate(new Date());
			applyMemberService.saveOne(applyMemberDto);
		} catch (HandException e) {
			e.printStackTrace();
			json.setMessage(e.getMessage());
			json.setSuccess(false);
			return json;
		}
		return new JsonUtil(true,"会员申请已提交！",null);
	}
	/**
	 * 审核
	 * @param userDTO
	 * @return
	 */
	@RequestMapping("/auditApply")
	@ResponseBody
	public JsonUtil auditApply(ApplyMemberDTO applyMemberDto){
		JsonUtil json=new JsonUtil();
		try {
			int userId =0;
			//获取数据
			ApplyMemberDTO dto =applyMemberService.selectById(applyMemberDto.getId());
			userId = dto.getUserId();
			//开始更新数据
			dto.setAuditDate(new Date());
			dto.setAuditRemark(applyMemberDto.getAuditRemark());
			dto.setStatus(1);
			dto.setLevel(applyMemberDto.getLevel());
			applyMemberService.updateById(dto);
				//更新完成后，还需要更新用户表上对应的字段
				//根据枚举来获取对应积分折扣
				List<EnumDTO> list = enumService.findEnumDTOByEnumWord("memberLevel");
				String discount ="1";
				for (EnumDTO enumDTO : list) {
					if(applyMemberDto.getLevel().equals(enumDTO.getOtherid().toString())) {
						discount = enumDTO.getExt();
					}
				}
				UserDTO user = userService.findUserDTOById(userId);
				//开始更新用户表数据
				user.setUserType(applyMemberDto.getLevel());
				user.setDiscount(discount);
				userService.updatePasswordByUserId(user);
		} catch (HandException e) {
			e.printStackTrace();
			json.setMessage(e.getMessage());
			json.setSuccess(false);
			return json;
		}
		return new JsonUtil(true,"操作成功！",null);
	}
	/**
	 * 驳回
	 * @param userDTO
	 * @return
	 */
	@RequestMapping("/rejectApply")
	@ResponseBody
	public JsonUtil rejectApply(ApplyMemberDTO applyMemberDto){
		JsonUtil json=new JsonUtil();
		try {
			//获取数据
			ApplyMemberDTO dto =applyMemberService.selectById(applyMemberDto.getId());
			//开始更新数据
			dto.setAuditDate(new Date());
			dto.setAuditRemark(applyMemberDto.getAuditRemark());
			dto.setStatus(2);
			dto.setLevel(applyMemberDto.getLevel());
			applyMemberService.updateById(dto);
		} catch (HandException e) {
			e.printStackTrace();
			json.setMessage(e.getMessage());
			json.setSuccess(false);
			return json;
		}
		return new JsonUtil(true,"操作成功！",null);
	}
	/**
	 * 根据id查询对应申请
	 * @param 
	 * @return
	 */
	@RequestMapping("/findApply")
	public ModelAndView findRoleDTOByRoleId(@RequestParam Integer applyId,HttpServletResponse response,ModelAndView mv){
		mv.setViewName("apply/auditApply");
		try {
			ApplyMemberDTO  dto= applyMemberService.selectById(applyId);
			mv.addObject("applyMemberDTO", dto);
		} catch (Exception e) {
			e.printStackTrace();
			return mv;
		}
		return mv;
	}
}
