package zzx.dao;

import org.springframework.stereotype.Repository;

import zzx.entity.BusOrderDetailDTO;

@Repository("busOrderDetailDAO")
public interface BusOrderDetailDAO extends BaseDAO<BusOrderDetailDTO> {

	void updateByOrderCode(BusOrderDetailDTO t);

}
