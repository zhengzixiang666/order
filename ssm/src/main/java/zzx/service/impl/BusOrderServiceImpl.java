package zzx.service.impl;

import java.math.BigDecimal;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import zzx.dao.BusOrderDAO;
import zzx.entity.BusOrderDTO;
import zzx.entity.BusOrderDetailDTO;
import zzx.entity.UserDTO;
import zzx.service.BusOrderDetailService;
import zzx.service.BusOrderService;
import zzx.service.UserService;
@Service("busOrderService")
public class BusOrderServiceImpl extends BaseServiceImpl<BusOrderDTO,BusOrderDAO> implements BusOrderService {

	@Resource
	private BusOrderService busOrderService;
	
	@Resource
	private BusOrderDetailService busOrderDetailService;
	
	@Resource
	private UserService userService;
	
	@Override
	public void doChangeStatus(Integer id,Integer status) {
		if(id!=null) {
			//获取支付金额
			BigDecimal payMoney = new BigDecimal(1);
			BusOrderDTO bo=busOrderService.selectById(id);
			payMoney = bo.getPayMoney();
			BusOrderDTO busOrderDTO=new BusOrderDTO();
			busOrderDTO.setId(id);
			busOrderDTO.setOrderStatus(status);
			busOrderService.updateById(busOrderDTO);
			
			BusOrderDetailDTO bod=new BusOrderDetailDTO();
			bod.setOrderCode(bo.getOrderCode());
			bod.setOrderStatus(status);
			//更新订单详情的状态
			busOrderDetailService.updateByOrderCode(bod);
			if(status ==2) {
				//更新之后，按照实际金额除以10计算积分
				UserDTO user = userService.findUserDTOById(bo.getUserId());
				BigDecimal points = new BigDecimal(0);
				if(null == user.getPoints() || "".equals(user.getPoints())) {
					points= payMoney.divide(new BigDecimal(10));
				}else {
					points = new BigDecimal(user.getPoints()).add(payMoney.divide(new BigDecimal(10)));
				}
				user.setPoints(points.toPlainString());
				userService.updatePasswordByUserId(user);
				
			}
		}
	}

	

}
