package zzx.controller;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import zzx.entity.XssTypeDTO;
import zzx.exception.HandException;
import zzx.service.XssTypeService;
import zzx.util.JsonUtil;

@Controller //这个注解正式时使用
@RequestMapping("xssType")
public class XssTypeController {
	
	@Resource
	public XssTypeService xssTypeService;
	
	@RequestMapping("/type")
	public String type(){
		return "xssType/type";
	}

	@RequestMapping("/findAllDTO")
	@ResponseBody
	public JsonUtil findAllDTO(){
		List<XssTypeDTO> list=xssTypeService.selectListByDTO(null);
		JsonUtil json=new JsonUtil(true,"发送成功",list);
		return json;
	};
	
	@RequestMapping("/findDTO")
	@ResponseBody
	public JsonUtil findDTO(XssTypeDTO xssTypeDTO){
		List<XssTypeDTO> list=xssTypeService.selectListByDTO(xssTypeDTO);
		JsonUtil json=new JsonUtil(true,"查询成功",list);
		return json;
	}
	
	
	@RequestMapping("/addMain")
	public String addMain(){
		
		return "xssType/addMain";
	}
	
	
	@RequestMapping("/add")
	@ResponseBody
	public JsonUtil add(XssTypeDTO xssTypeDTO){
		JsonUtil json=new JsonUtil();
		try {
			xssTypeService.saveOne(xssTypeDTO);
			json.setMessage("新增成功！");
		} catch (HandException e) {
			e.printStackTrace();
			json.setMessage(e.getMessage());
			return json;
		}
		return json;
	}
	
	/**
	 * 通过用户id，查询用户信息
	 * @param session
	 * @param userId
	 * @return
	 */
	@RequestMapping("/editMain")
	public ModelAndView editMain(@RequestParam Integer id,ModelAndView mv){
		mv.setViewName("xssType/editMain");
		try {
			XssTypeDTO xssTypeDTO=xssTypeService.selectById(id);
			mv.addObject("editDTO", xssTypeDTO);
		} catch (HandException e) {
			e.printStackTrace();
			return mv;
		}
		return mv;
	}
	
	/**
	 * 更新用户并且分配角色
	 */
	@RequestMapping("/update")
	@ResponseBody
	public JsonUtil update(XssTypeDTO xssTypeDTO){
		JsonUtil json=new JsonUtil();
		try {
			xssTypeService.updateById(xssTypeDTO);
			json.setMessage("更新成功！");
		} catch (HandException e) {
			e.printStackTrace();
			json.setMessage(e.getMessage());
			return json;
		}
		return json;
	}
	
	@RequestMapping("/delete")
	@ResponseBody
	public JsonUtil delete(@RequestParam Integer id){
		
		try {
			xssTypeService.deleteById(id);
			return new JsonUtil(true,"删除成功",null);
		} catch (HandException e) {
			e.printStackTrace();
			return new JsonUtil(false,e.getMessage(),null);
		}
	}
	
}
