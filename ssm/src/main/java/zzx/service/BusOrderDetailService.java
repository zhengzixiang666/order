package zzx.service;

import zzx.entity.BusOrderDetailDTO;

public interface BusOrderDetailService extends BaseService<BusOrderDetailDTO> {

	void updateByOrderCode(BusOrderDetailDTO t);

}
