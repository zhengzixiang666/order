package zzx.controller;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import zzx.entity.BusOrderDTO;
import zzx.entity.BusOrderDetailDTO;
import zzx.exception.HandException;
import zzx.service.BusOrderDetailService;
import zzx.service.BusOrderService;
import zzx.util.JsonUtil;

@Controller //这个注解正式时使用
@RequestMapping("busOrder")
public class BusOrderController {
	
	@Resource
	public BusOrderService busOrderService;
	
	@Resource
	public BusOrderDetailService busOrderDetailService;
	

	@RequestMapping("/save")
	public String save(){
		return "busOrder/save";
		
	}
	
	@RequestMapping("/wait")
	public String waits(){
		
		return "busOrder/wait";
	}
	
	@RequestMapping("/compet")
	public String compet(){
		return "busOrder/compet";
		
	}
	
	@RequestMapping("/mylist")
	public String mylist(){
		return "busOrder/mylist";
		
	}
	
	@RequestMapping("/list")
	public ModelAndView list(@RequestParam Integer id,ModelAndView mv){
		mv.setViewName("busOrder/list");
		try {
			BusOrderDTO busOrderDTO=busOrderService.selectById(id);
			mv.addObject("busOrderDTO", busOrderDTO);
		} catch (HandException e) {
			e.printStackTrace();
			return mv;
		}
		return mv;
	}
	
	@RequestMapping("/doPay")
	@ResponseBody
	public JsonUtil doPay(@RequestParam Integer id){
		
		try {
			busOrderService.doChangeStatus(id,2);
			return new JsonUtil(true,"支付成功",null);
		} catch (HandException e) {
			e.printStackTrace();
			return new JsonUtil(false,e.getMessage(),null);
		}
	}
	
	@RequestMapping("/doCheck")
	@ResponseBody
	public JsonUtil doCheck(@RequestParam Integer id){
		
		try {
			busOrderService.doChangeStatus(id,3);
			return new JsonUtil(true,"支付成功",null);
		} catch (HandException e) {
			e.printStackTrace();
			return new JsonUtil(false,e.getMessage(),null);
		}
	}
	
	
	
	@RequestMapping("/findAllDTO")
	@ResponseBody
	public JsonUtil findAllDTO(){
		List<BusOrderDTO> list=busOrderService.selectListByDTO(null);
		JsonUtil json=new JsonUtil(true,"发送成功",list);
		return json;
	};
	
	@RequestMapping("/findDetailAllDTO")
	@ResponseBody
	public JsonUtil findDetailAllDTO(BusOrderDetailDTO busOrderDetailDTO){
		List<BusOrderDetailDTO> list=busOrderDetailService.selectListByDTO(busOrderDetailDTO);
		JsonUtil json=new JsonUtil(true,"发送成功",list);
		return json;
	};
	
	
	
	@RequestMapping("/findDTO")
	@ResponseBody
	public JsonUtil findDTO(BusOrderDTO busOrderDTO){
		List<BusOrderDTO> list=busOrderService.selectListByDTO(busOrderDTO);
		JsonUtil json=new JsonUtil(true,"查询成功",list);
		return json;
	}
	
	
	@RequestMapping("/addMain")
	public String addMain(){
		
		return "busOrder/addMain";
	}
	
	
	@RequestMapping("/add")
	@ResponseBody
	public JsonUtil add(BusOrderDTO busOrderDTO){
		JsonUtil json=new JsonUtil();
		try {
			busOrderService.saveOne(busOrderDTO);
			json.setMessage("新增成功！");
		} catch (HandException e) {
			e.printStackTrace();
			json.setMessage(e.getMessage());
			return json;
		}
		return json;
	}
	
	/**
	 * 通过用户id，查询用户信息
	 * @param session
	 * @param userId
	 * @return
	 */
	@RequestMapping("/editMain")
	public ModelAndView editMain(@RequestParam Integer id,ModelAndView mv){
		mv.setViewName("busOrder/editMain");
		try {
			BusOrderDTO busOrderDTO=busOrderService.selectById(id);
			mv.addObject("editDTO", busOrderDTO);
		} catch (HandException e) {
			e.printStackTrace();
			return mv;
		}
		return mv;
	}
	
	/**
	 * 更新用户并且分配角色
	 */
	@RequestMapping("/update")
	@ResponseBody
	public JsonUtil update(BusOrderDTO busOrderDTO){
		JsonUtil json=new JsonUtil();
		try {
			busOrderService.updateById(busOrderDTO);
			json.setMessage("更新成功！");
		} catch (HandException e) {
			e.printStackTrace();
			json.setMessage(e.getMessage());
			return json;
		}
		return json;
	}
	
	@RequestMapping("/delete")
	@ResponseBody
	public JsonUtil delete(@RequestParam Integer id){
		
		try {
			busOrderService.deleteById(id);
			return new JsonUtil(true,"删除成功",null);
		} catch (HandException e) {
			e.printStackTrace();
			return new JsonUtil(false,e.getMessage(),null);
		}
		
	}


}
