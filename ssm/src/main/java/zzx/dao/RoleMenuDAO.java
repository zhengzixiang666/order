package zzx.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import zzx.entity.RoleMenuDTO;

@Repository("roleMenuDAO")
public interface RoleMenuDAO {
	//根据角色id查询所有菜单
	public abstract List<RoleMenuDTO> getMenuDTOByRoleId(Integer roleid);
	//根据角色id查询所有角色菜单关联表
	public abstract List<RoleMenuDTO> getAllRoleMenuDTOByRoleId(Integer roleid);
	
	//保存角色id,菜单id
	public abstract void createRoleMenuDTO(@Param("roleid")Integer roleid,@Param("menuid")Integer menuid);

	//删除记录，根据角色id，和菜单id
	public abstract void deleteRoleMenuDTO(@Param("roleid")Integer roleid,@Param("menuid")Integer menuid);
	
	//根据角色id，删除所有角色菜单
	public abstract void deleteAllRoleMenuDTOByRoleid(Integer roleid);
	
	public int deleteRoleMenuDTOByMenuId(Integer menuId);
	
}
