<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ include file="../common/common.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title></title>
<script type="text/javascript">

if ($.fn.pagination){
    $.fn.pagination.defaults.beforePageText = '第';
    $.fn.pagination.defaults.afterPageText = '共{pages}页';
    $.fn.pagination.defaults.displayMsg = '显示{from}到{to},共{total}记录';
}

//分页功能    
function pagerFilter(data) {
    if (typeof data.length == 'number' && typeof data.splice == 'function') {
        data = {
            total: data.length,
            rows: data
        }
    }
    var dg = $(this);
    var opts = dg.datagrid('options');
    var pager = dg.datagrid('getPager');
    pager.pagination({
        onSelectPage: function (pageNum, pageSize) {
            opts.pageNumber = pageNum;
            opts.pageSize = pageSize;
            pager.pagination('refresh', {
                pageNumber: pageNum,
                pageSize: pageSize
            });
            dg.datagrid('loadData', data);
        }
    });
    if (!data.originalRows) {
        if(data.rows)
            data.originalRows = (data.rows);
        else if(data.data && data.data.rows)
            data.originalRows = (data.data.rows);
        else
            data.originalRows = [];
    }
    var start = (opts.pageNumber - 1) * parseInt(opts.pageSize);
    var end = start + parseInt(opts.pageSize);
    data.rows = (data.originalRows.slice(start, end));
    return data;
}

	$(function() {
		$('#dg').datagrid({
			iconCls : 'icon-a_detail',
			fit : true,
			fitColumns : true,
			rownumbers : true,
			pagination : true,
			loadFilter: pagerFilter,
			checkOnSelect:true,
			border : false,
			striped : true,
			toolbar : '#tb',
			columns : [ [ {
				field : 'id',
				title : '类别id',
				checkbox:  true
			},{
				field : 'foodCode',
				title : '商品编号',
				width : '15%',
				align : 'center'
			},{
				field : 'name',
				title : '商品名称',
				width : '15%',
				align : 'center'
			},{
				field : 'money',
				title : '价格',
				width : '15%',
				align : 'center'
			},{
				field : 'factory',
				title : '厂家',
				width : '15%',
				align : 'center'
			},{
				field : 'factoryDate',
				title : '生产日期',
				width : '15%',
				align : 'center',
				formatter : function(value){
                    var date = new Date(value);
                   return formatDate(date,"yyyy-MM-dd");
                }
			},{
				field : 'field',
				title : '输入数量',
				width : '25%',
				align : 'center',
				formatter:function(value,row,index){
					var data="<input name='inputrow' id='row" + row.id + "' type='text'>"
// 					var data=" <a href='#' name='edit' onclick='edit(" + row.id + ")'></a> ";
// 					+"\| <a href='#' name='deleteRow' class='easyui-linkbutton' onclick='deleteRow(" + row.id + ")' title='删除'></a>";
					return data;
				}
				
			}]],
// 			onLoadSuccess:function(data){    
// 		        $("input[name='inputrow']").on('input',changeRow(data)); 
// 			}
		});
		reloadData();
	});
	//加载数据表格
	function reloadData(){
		$.ajax({
			url:'findAllDTO.do',
			type:'post',
			dataType:'json',
			success:function(data){
				var json=data.data;
				success(json);
			},
			error:function(){
				alert('error');
			}
		});
	}
	//处理枚举类的显示，并加载表格
	function success(json){
		
		$('#dg').datagrid('loadData',json);
	}
	
	//查询
	function doQuery(){
		var foodCode=jQuery('#foodCode').val();
		var name=jQuery('#name').val();
		$.ajax({
			url:'findDTO.do',
			type:'post',
			data:{"foodCode":foodCode,"name":name},
			dataType:'json',
			success:function(data){
				var json=data.data;
				success(json);
			},
			error:function(){
				alert('请求失败');
			}
		});
	}
	
	function doAdd(){
		
		var list=$('#dg').datagrid('getSelections');
		var sumMoney=0;//总价
		var sumCount=0;//总数量
		if(list){
			for(var i=0;i<list.length;i++){
				var count=$('#row'+list[i].id).val();
				var money=list[i].money;
				sumMoney+=money*count;
				sumCount+=parseFloat(count);
			}
		}
		$('#sumMoney').attr('value',sumMoney);
		$('#sumCount').attr('value',sumCount);
	}
	
	function doSubmit(){
		var list=$('#dg').datagrid('getSelections');
		var userId=$('#userId').val();
		var dataList=[];
		if(list){
			for(var i=0;i<list.length;i++){
				var poList={};
				var count=$('#row'+list[i].id).val();
				var money=list[i].money;
				poList.foodId=list[i].id;
				poList.orderNumber=count;
				poList.userId=userId;
				poList.orderMoney=money;
				dataList.push(poList);
			}
		}
		$.ajax({
			url:'doSubmit.do',
			type:'post',
			traditional: true,
			data:JSON.stringify({orderList:dataList}),
			dataType:'json',
			contentType : 'application/json',
			success:function(data){
				layer.alert("添加成功！",{
					skin: 'layui-layer-molv',
					yes:function(index, layero){
						layer.close(index);
						reloadData();
					}
				});
			},
			error:function(){
				alert('请求失败');
			}
		});
	}
</script>
</head>
<body>
   <input type="hidden" id="userId" name="userId" value="${sessionScope.userId}">
	<div id="tb" style="padding:15px;background-color: #ADD8E6;">
			<div>
				当前位置：商品列表
			</div>
			<div style="padding:15px">
				<span style="margin-left: 100px">商品编号:</span>
				<input id="foodCode" name="foodCode" class="easyui-textbox" type="text">
				<span style="margin-left: 100px">商品名称:</span>
				<input id="name" name="name" class="easyui-textbox" type="text">
				<span style="margin-left: 50px;">总数量:<input value=''  style="width: 50px;border:none;background:#ADD8E6;color: red" id="sumCount"></input></span>
				<span style="margin-left: 5px;">总价格:<input value=''  style="width: 50px;border:none;background:#ADD8E6;color: red" id="sumMoney"></input></span>
				<a href="#" class="easyui-linkbutton" style="margin-left:50px;" iconCls="icon-search" onclick="doQuery()">查 询</a>
				<a href="#" class="easyui-linkbutton" plain="false" iconCls="icon-add" onclick="doAdd()">确 认</a>
				<a href="#" class="easyui-linkbutton" plain="false" onclick="doSubmit()">加入购物车</a>
			</div>
	</div>
	<table id="dg">
	</table>
<!-- 	<div id="dd"></div> -->
</body>
</html>