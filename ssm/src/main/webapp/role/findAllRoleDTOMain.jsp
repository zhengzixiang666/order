<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ include file="../common/common.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title></title>
<script type="text/javascript">
if ($.fn.pagination){
    $.fn.pagination.defaults.beforePageText = '第';
    $.fn.pagination.defaults.afterPageText = '共{pages}页';
    $.fn.pagination.defaults.displayMsg = '显示{from}到{to},共{total}记录';
}//分页功能    
function pagerFilter(data) {
    if (typeof data.length == 'number' && typeof data.splice == 'function') {
        data = {
            total: data.length,
            rows: data
        }
    }
    var dg = $(this);
    var opts = dg.datagrid('options');
    var pager = dg.datagrid('getPager');
    pager.pagination({
        onSelectPage: function (pageNum, pageSize) {
            opts.pageNumber = pageNum;
            opts.pageSize = pageSize;
            pager.pagination('refresh', {
                pageNumber: pageNum,
                pageSize: pageSize
            });
            dg.datagrid('loadData', data);
        }
    });
    if (!data.originalRows) {
        if(data.rows)
            data.originalRows = (data.rows);
        else if(data.data && data.data.rows)
            data.originalRows = (data.data.rows);
        else
            data.originalRows = [];
    }
    var start = (opts.pageNumber - 1) * parseInt(opts.pageSize);
    var end = start + parseInt(opts.pageSize);
    data.rows = (data.originalRows.slice(start, end));
    return data;
}

	$(function() {
		$('#dg').datagrid({
			iconCls : 'icon-a_detail',
			fit : true,
			fitColumns : true,
			rownumbers : true,
			pagination : true,
			loadFilter: pagerFilter,
			checkOnSelect:true,
			border : false,
			striped : true,
			toolbar : '#tb',
			columns : [ [ {
				field : 'roleid',
				title : '角色id',
				checkbox:  true
			},{
				field : 'name',
				title : '角色名',
				width : '10%',
				align : 'center'
			},{
				field : 'code',
				title : '角色编码',
				width : '10%',
				align : 'center'
			},  {
				field : 'attrbute',
				title : '角色属性',
				width : '20%',
				align : 'center'
			},{
				field : 'isused',
				title : '是否启用',
				width : '20%',
				align : 'center'
			},{
				field : 'remark',
				title : '备注',
				width : '20%',
				align : 'center'
			},{
				field : 'field',
				title : '操作',
				width : '20%',
				align : 'center',
				formatter:function(value,row,index){
					if(row.roleid!=27 && row.roleid!=28){
					var data="<a href='#' name='edit' onclick='edit(" + row.roleid + ")' title='修改'></a> "
					+ "\| <a href='#' name='man' class='easyui-linkbutton' onclick='man(" + row.roleid + ")' title='权限'></a> "
					+"\| <a href='#' name='deleteRow' class='easyui-linkbutton' onclick='deleteRow(" + row.roleid + ")' title='删除'></a>";
					return data;
					}else{
						return "<a href='#' name='man' class='easyui-linkbutton' onclick='man(" + row.roleid + ")' title='权限'></a>";
					}
				}
				
			}]],
			onLoadSuccess:function(data){    
		        $("a[name='edit']").linkbutton({text:'修改',plain:false,iconCls:'icon-edit'}); 
		        $("a[name='man']").linkbutton({text:'权限',plain:false,iconCls:'icon-man'});
		        $("a[name='deleteRow']").linkbutton({text:'删除',plain:false,iconCls:'icon-cancel'});    
		            
			}
		});

		reloadData();
		
	});
	//加载表格数据
	function reloadData(){
		$.ajax({
			url:'findAllRoleDTO.do',
			type:'post',
			dataType:'json',
			success:function(data){
				var json=data.data;
				success(json);
			},
			error:function(){
				alert('error');
			}
		});
	}
	
	//处理枚举类的显示，并加载表格
	function success(json){
		for ( var i in json) {
			if(json[i].isused==1 || json[i].isused=='1'){
				json[i].isused='启用';
			}
			if(json[i].isused==2 || json[i].isused=='2'){
				json[i].isused='不启用';
			}
			if(json[i].attrbute==1){
				json[i].attrbute='超级管理员';
			}
			if(json[i].attrbute==2){
				json[i].attrbute='管理员';
			}
			if(json[i].attrbute==3){
				json[i].attrbute='操作员';
			}
		}
		$('#dg').datagrid('loadData',json);
	}
	
	
	//查询
	function doQuery(){
		var code=$('#code').val();
		var isused=$('#isused').val();
		var name=$('#name').val();
		$.ajax({
			url:'findRoleDTOByRoleDTO.do',
			type:'post',
			data:{"code":code,"isused":isused,"name":name},
			dataType:'json',
			success:function(data){
				var json=data.data;
				success(json);
			},
			error:function(){
				alert('error');
			}
		});
	}
	//新增
	function doAdd(){
		layer.open({
			type:2,
			title:'当前位置 : 新增角色',
			content:'addRoleDTOMain.do',
			skin:'layui-layer-lan',
			area:['600px','300px'],
			maxmin:false,
			btn: ['关闭'],
			yes: function(index, layero){
				reloadData();
				layer.close(index);
			  },
			cancel: function(index, layero){ 
				 reloadData();
				 layer.close(index);
				}   
			});   
	}
	//修改方法
	function edit(roleid){
		layer.open({
			type:2,
			title:'当前位置 : 修改角色',
			content:'findRoleDTOByRoleId.do?roleid='+roleid,
			skin:'layui-layer-lan',
			area:['600px','300px'],
			maxmin:false,
			btn: ['关闭'],
			yes: function(index, layero){
				reloadData();
				layer.close(index);
			  },
			cancel: function(index, layero){ 
				 reloadData();
				 layer.close(index);
				}   
			});
	}
	
	//删除方法
	function deleteRow(roleid){
		$.ajax({
			type:'post',
			data:{"roleid":roleid},
			url:'deleteRoleDTOByRoleid.do',
			dataType:'json',
			success:function(json){
				reloadData();
				layer.alert(json.message);
			},
			error:function(){
				layer.alert("删除失败");
			}
		});
	}
	
	//权限
	function man(roleid){
		var index = layer.open({
			type:2,
			title:'当前位置 : 分配菜单权限',
			content:'findAllMenuDTOMain.do?roleid='+roleid,
			skin:'layui-layer-lan',
			area:['50%','50%'],
			maxmin:true,
			btn: ['关闭'],
			yes: function(index, layero){
				layer.close(index);
			  },
			cancel: function(index, layero){ 
				 layer.close(index);
				}   
			});
		layer.full(index);  
	}
	
	
</script>
</head>
<body>
	<div id="tb" style="padding:15px;background-color: #ADD8E6;">
			<div>
				当前位置：系统角色
			</div>
			<div style="padding:15px">
				<span>角色名称:
				<input id="name" name="name" class="easyui-textbox" type="text" ></input></span>
				<span style="margin-left: 10%">角色编码:
				<input id="code" name="code" class="easyui-textbox" type="text"></input></span>
				<span style="margin-left: 10%">是否启用:
				<input id="isused" name="isused" class="easyui-combobox"   panelHeight="auto"
    						data-options="valueField:'otherid',textField:'name',url:'<%= this.getServletContext().getContextPath()%>/enum/findEnumDTOByEnumWord.do?enumWord=isused'">
    						</input></span>
				<div>
					<a href="#" class="easyui-linkbutton" style="float:right " plain="false" iconCls="icon-add" onclick="doAdd()">新 增</a>
					<a href="#"  class="easyui-linkbutton" style="float:right " iconCls="icon-search" onclick="doQuery()">查 询</a>
				</div>
			</div>
	</div>
	<table id="dg">
	</table>
<!-- 	<div id="cc" class="easyui-layout" style="width:100%;height:100%;background-color: white;"> -->
<!-- 			<iframe id="iframe1" style="height: 100%;width: 100%" frameborder="0"  scrolling="no" src="" ></iframe> -->
<!-- 	</div> -->
</body>
</html>