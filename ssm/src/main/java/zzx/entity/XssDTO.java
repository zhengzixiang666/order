package zzx.entity;

import java.io.Serializable;

public class XssDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String data;//主键
	
	private boolean ifXss;//是否阻止xss攻击
	

	public boolean isIfXss() {
		return ifXss;
	}

	public void setIfXss(boolean ifXss) {
		this.ifXss = ifXss;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}
	
	

}
