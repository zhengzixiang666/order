<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ include file="../common/common.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>新增用户</title>

<script type="text/javascript">
	$(function(){
		console.log($('#da').val())
	})
</script>
</head>
<body >


	 <form id="form" method="post">
		    	<table align="center" style="border-collapse:separate; border-spacing:15px;">
		    		<tr>
		    			<td align="right"><label>反射型测试脚本：<input value="<script>alert('ok')</script>" style="width: 260px" class="easyui-textbox" type="text" ></input></label></td>
		    			<td align="right"><label><input value="<script>alert(document.cookie)</script>" style="width: 300px" class="easyui-textbox" type="text" ></input></label></td>
		    		</tr>
		    		<tr>
		    			<td align="right"><label>存储型测试脚本：<input value="<script>alert('ok')</script>" style="width: 260px" class="easyui-textbox" type="text" ></input></label></td>
		    			<td align="right"><label><input id="data1" name="data1" value="<script>alert(document.cookie)</script>" style="width: 300px" class="easyui-textbox" type="text" ></input></label></td>
		    		</tr>
		    		<tr>
		    			<td align="right"><label>DOM型测试脚本：<input value="<svg/onload=alert(1)>" style="width: 260px" class="easyui-textbox" type="text" ></input></label></td>
		    			
		    		</tr>
		    	</table>
	 </form>
	 
	 
</body>
</html>