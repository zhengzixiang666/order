package zzx.enumutil;

public interface BaseStringEnum {
	
	public abstract String getDisplayName();
	
	public abstract String getValue();
	

}
