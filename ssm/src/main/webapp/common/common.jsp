<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>西财商场会员管理系统</title>
<link  rel="stylesheet" type="text/css" href="<%= this.getServletContext().getContextPath()%>/bootstrap/css/bootstrap.min.css"/>
<link  rel="stylesheet" type="text/css" href="<%= this.getServletContext().getContextPath()%>/jqueryeasyui/themes/default/easyui.css"/>
<link  rel="stylesheet" type="text/css" href="<%= this.getServletContext().getContextPath()%>/jqueryeasyui/themes/icon.css"/>
<link  rel="stylesheet" type="text/css" href="<%= this.getServletContext().getContextPath()%>/layui/css/layui.css"/>
<script type="text/javascript" src="<%= this.getServletContext().getContextPath()%>/jqueryeasyui/jquery.min.js"></script>
<script type="text/javascript" src="<%= this.getServletContext().getContextPath()%>/bootstrap/js/bootstrap.js"></script>
<script type="text/javascript" src="<%= this.getServletContext().getContextPath()%>/jqueryeasyui/jquery.easyui.min.js"></script>
<script type="text/javascript" src="<%= this.getServletContext().getContextPath()%>/layui/layui.js"></script>



<script type="text/javascript">
	var layer=null;
	$(function(){
		layui.use('layer', function(){
			layer = layui.layer;
		});
	});
	//格式化时间
	function formatDate(date,str){
		if(str=="yyyy-MM-dd"){
			var y = date.getFullYear();
            var M = date.getMonth() + 1;
            var d = date.getDate();
            return y + '-' +M + '-' + d;
		}
		if(str=="yyyy-MM-dd HH:mm:ss"){
			var y = date.getFullYear();
            var M = date.getMonth() + 1;
            var d = date.getDate();
            var H=date.getHours();
            var m=date.getMinutes();
            var s=date.getSeconds();
            return y + '-' +M + '-' + d +' '+H+':'+m+':'+s;
		}
		return null;
	}
	
</script>

</head>
<body>
</body>
</html>