package zzx.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import zzx.dao.XssScriptDAO;
import zzx.entity.XssScriptDTO;
import zzx.exception.HandException;
import zzx.service.XssScriptService;
@Service(value="xssScriptService")
public class XssScriptServiceImpl implements XssScriptService {
	
	@Resource
	private XssScriptDAO xssScriptDAO;
	
	@Override
	public List<XssScriptDTO> selectListByMap(Map<String, Object> map) {
		return xssScriptDAO.selectListByMap(map);
	}

	@Override
	public XssScriptDTO selectById(Integer id) throws HandException {
		return xssScriptDAO.selectById(id);
	}

	@Override
	public void saveOne(XssScriptDTO t) throws HandException {
		 xssScriptDAO.saveOne(t);
	}

	@Override
	public void updateById(XssScriptDTO t) throws HandException {
		 xssScriptDAO.updateById(t);

	}

	@Override
	public void deleteById(Integer id) throws HandException {
		xssScriptDAO.deleteById(id);

	}

	@Override
	public List<XssScriptDTO> selectListByDTO(XssScriptDTO t) {
		return xssScriptDAO.selectListByDTO(t);
	}

}
