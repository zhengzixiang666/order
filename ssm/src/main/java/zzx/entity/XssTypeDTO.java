package zzx.entity;

import java.io.Serializable;

public class XssTypeDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	
	private Integer xssType;
	
	private String xssRemark;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getXssType() {
		return xssType;
	}

	public void setXssType(Integer xssType) {
		this.xssType = xssType;
	}

	public String getXssRemark() {
		return xssRemark;
	}

	public void setXssRemark(String xssRemark) {
		this.xssRemark = xssRemark;
	}

	
	

}
