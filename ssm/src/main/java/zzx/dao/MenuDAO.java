package zzx.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import zzx.entity.MenuDTO;
/**
 * 菜单DAO接口
 * @author 郑仔祥
 *
 */
@Repository("menuDAO")
public interface MenuDAO {
	/**
	 * 通过父类id获取所有启用子菜单
	 * @param parentId
	 * @return
	 */
	public List<MenuDTO> getChildrenByParentId(Integer parentid);
	
	public List<MenuDTO> getAllMenuDTO();

	public List<MenuDTO> getParentMenuDTO();
	
	public MenuDTO getParentMenuDTOById(Integer id);
	//通过菜单id，获取启用的菜单
	public MenuDTO getMenuDTOByMenuId(Integer id);
 	
	public List<MenuDTO> getAllListMenuDTO();
	
	public int createMenuDTO(MenuDTO menuDTO);
	
	public int updateMenuDTO(MenuDTO menuDTO);
	
	public int deleteMenuDTOByMenuId(Integer menuId);
	
}
