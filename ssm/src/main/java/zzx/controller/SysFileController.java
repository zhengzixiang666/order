package zzx.controller;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import zzx.entity.SysFileDTO;
import zzx.exception.HandException;
import zzx.service.SysFileService;
import zzx.util.JsonUtil;

@Controller
@RequestMapping("file")
public class SysFileController {
	
	
	
	@Resource
	private SysFileService sysFileService;
	
	@RequestMapping("createFile")
	public String getCreateFile(){
		return "file/createFile";
	}
	
	/**
	 * 文件上传主页面
	 * @return
	 */
	@RequestMapping("upload")
	public String upload(){
		return "file/upload";
	}
	
	/**
	 * 新增文件
	 * @return
	 */
	@RequestMapping("doAddUpload")
	public String doAddUpload(){
		return "file/doAddUpload";
	}
	
	/**
	 * 文件上传
	 * @param request
	 * @param file
	 * @return
	 * @throws FileNotFoundException
	 */
	@RequestMapping("doUpload")
	@ResponseBody
	public JsonUtil doUpload(HttpServletRequest request, @RequestParam(value="file")MultipartFile file) throws FileNotFoundException{
		String fileName = request.getParameter("fileName");//文件名
		String message = request.getParameter("message");//文件描述
		String loginCode=(String)request.getSession().getAttribute("username");//上传名
		String fileSuffixName=file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));//获取文件后缀名
		String fileType=file.getContentType();//文件类型
		try {
			byte[] document=file.getBytes();
			//上传文件到数据库
			SysFileDTO sysFileDTO=new SysFileDTO();
			sysFileDTO.setFileName(fileName);
			sysFileDTO.setDocument(document);
			sysFileDTO.setFileType(fileType);
			sysFileDTO.setLoginCode(loginCode);
			sysFileDTO.setMessage(message);
			sysFileDTO.setSuffixName(fileSuffixName);
			sysFileService.createFile(sysFileDTO);
		} catch (Exception e) {
			e.printStackTrace();
			return new JsonUtil(false,e.getMessage(),null);
		}		
        return new JsonUtil(true,"上传成功！","");
	}
	
	/**
	 * 执行查询所有角色方法
	 * @return
	 */
	@RequestMapping("/findAllSysFileDTO")
	@ResponseBody
	public JsonUtil findAllSysFileDTO(SysFileDTO sysFileDTO){
		List<SysFileDTO>  roleDTOList=sysFileService.findAllSysFileDTO(sysFileDTO);
		JsonUtil json=new JsonUtil(true,"发送成功",roleDTOList);
		return json;
	}
	
	@RequestMapping("/deleteFileById")
	@ResponseBody
	public JsonUtil deleteFileById(SysFileDTO sysFileDTO){
		try {
			sysFileService.deleteFileById(sysFileDTO.getId());
			return new JsonUtil(true,"删除成功","");
		} catch (Exception e) {
			return new JsonUtil(false,e.getMessage(),"");
		}
	}
	
	@RequestMapping("/findFileById")
	public ModelAndView findFileById(@RequestParam String id,@RequestParam String loginCode,ModelAndView mv){
		mv.setViewName("file/findFileById");
		try {
			SysFileDTO	sysFileDTO = sysFileService.findFileById(id);
			mv.addObject("sysFileDTO",sysFileDTO);
			mv.addObject("loginCode",loginCode);
		} catch (HandException e) {
			e.printStackTrace();
			return mv;
		}
		return mv;
	}
	
	@RequestMapping("/updateFileById")
	@ResponseBody
	public JsonUtil updateFileById(SysFileDTO sysFileDTO){
		try {
			sysFileService.updateFileById(sysFileDTO);
			return new JsonUtil(true,"更新成功","");
		} catch (Exception e) {
			return new JsonUtil(false,e.getMessage(),"");
		}
	}
	
	/**
	 * 文件下载
	 */
	@RequestMapping(value="/downloadFile")
	public void downloadFile(HttpServletResponse res,HttpServletRequest req,SysFileDTO sysFileDTO){
		ServletOutputStream out=null;
		res.setContentType("multipart/form-data");
		res.setCharacterEncoding("UTF-8");
		res.setContentType("text/html");
		try {
			SysFileDTO file=sysFileService.downloadFileById(sysFileDTO.getId());
			String userAgent=req.getHeader("User-Agent");
			String fileName = "";
			if (userAgent.contains("MSIE") || userAgent.contains("Trident")) {
				//IE
				fileName = java.net.URLEncoder.encode(file.getFileName()+file.getSuffixName(), "UTF-8");
			} else {
				// 非IE浏览器的处理：
				fileName = new String((file.getFileName()+file.getSuffixName()).getBytes("UTF-8"), "ISO-8859-1");
			}
			res.setHeader("Content-Disposition", "attachment;fileName=" + fileName);
			byte[] fileDocument=file.getDocument();
			InputStream inputStream=new ByteArrayInputStream(fileDocument);
			out=res.getOutputStream();
			int b=0;
			byte[] buffer=new byte[1024];
			while((b=inputStream.read(buffer))!=-1) {
				out.write(buffer, 0, b);
			}
			inputStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			if(out!=null) {
				try {
					out.flush();
					out.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	

}
