package zzx.exception;

/**
 * 抛出异常类
 * @author 郑仔祥
 *
 */
public class HandException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public HandException(String message){
		super(message); 
	}

}
