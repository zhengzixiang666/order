<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ include file="../common/common.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>red</title>
<script type="text/javascript">

</script>
</head>
<body>

<table align="center" height="600px" title="安全性测试之XSS攻击" style="border-collapse:separate; border-spacing:15px;">
  		<tr>
  			<th align="center" width="15%" height="5%"><label>名称</label></th>
  			<th align="center"><label>攻击方式</label></th>
  			<th align="center"><label>危害</label></th>
  			<th align="center"><label>对应策略</label></th>
  		</tr>
  		<tr>
  			<td align="left">反射型 XSS（非持久型）</td>
  			<td align="left">将XSS代码放在URL中，将参数提交到服务器。服务器解析后响应，在响应结果中存在XSS代码，最终通过浏览器解析执行</td>
  			<td align="left" rowspan="3">1、盗取用户cookie值</br></br></br>
  			2、使用js或css破坏页面正常的结构与样式</br></br></br>
  			3、侵占服务器资源</br></br></br>
  			4、流量劫持</br></br></br>
  			5、利用可被攻击的域受到其他域信任的特点，
			做不合法的操作</td>
			<td align="left" rowspan="3">1、数据不直接存入服务器，进行数据处理（加密、解密）</br></br></br>
  			2、对重要的cookie设置httpOnly, 防止客户端通过document.cookie读取</br></br></br>
  			3、DOM XSS的发生主要是在JS中使用eval造成的，所以应当避免使用eval语句</td>
  		</tr>
  		<tr>
  			<td align="left">存储型 XSS（持久型）</td>
  			<td align="left">将XSS代码存储到服务端（数据库、内存、文件系统等），接口请求时直接从服务器读取</td>
  			
  		</tr>
  		<tr>
  			<td align="left">DOM XSS</td>
  			<td align="left">不需要服务器端的解析响应的直接参与，直接通过浏览器端的DOM解析</td>
  			
  		</tr>
  	</table>
</body>
</html>