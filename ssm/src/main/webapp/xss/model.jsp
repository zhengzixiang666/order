<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ include file="../common/common.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>新增用户</title>
<script type="text/javascript">
function doSave(){
	layer.open({
		type:2,
		title:'当前位置 : 模拟background',
		content:'xss.do?data='+$('#data').val(),
		skin:'layui-layer-lan',
		area:['600px','350px'],
		maxmin:false,
		btn: ['关闭'],
		yes: function(index, layero){
			
			layer.close(index);
		  },
		cancel: function(index, layero){ 
				layer.close(index)
			}   
		}); 
	
}
function doSave1(){
	layer.open({
		type:2,
		title:'当前位置 : 正常',
		content:'xss.do?data='+$('#data1').val(),
		skin:'layui-layer-lan',
		area:['600px','350px'],
		maxmin:false,
		btn: ['关闭'],
		yes: function(index, layero){
			
			layer.close(index);
		  },
		cancel: function(index, layero){ 
				layer.close(index)
			}   
		}); 
	
}
</script>
</head>
<body >
<form id="form1" method="post" >
		    	<table align="center" style="border-collapse:separate; border-spacing:15px;">
		    		<tr>
		    			<td align="right"><label>正常测试：<input id="data1" name="data1" value="13123" style="width: 260px" class="easyui-textbox" type="text" ></input></label></td>
		    		</tr>
		    		<tr>
		    			<td align="right">
		    				<div align="right"><a href="#" class="easyui-linkbutton" plain="false" iconCls="icon-add" onclick="doSave1()">提 交</a> </div>
		    			</td>
		    		</tr>
		    	</table>
	 </form>
	 <form id="form" method="post" >
		    	<table align="center" style="border-collapse:separate; border-spacing:15px;">
		    		<tr>
		    			<td align="right"><label>测试攻击：<input id="data" name="data" value="background:red;" style="width: 260px" class="easyui-textbox" type="text" ></input></label></td>
		    		</tr>
		    		<tr>
		    			<td align="right">
		    				<div align="right"><a href="#" class="easyui-linkbutton" plain="false" iconCls="icon-add" onclick="doSave()">提 交</a> </div>
		    			</td>
		    		</tr>
		    	</table>
	 </form>
	 
	 
</body>
</html>