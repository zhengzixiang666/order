package zzx.dao;

import java.util.List;

import zzx.entity.SysFileDTO;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zhengzixiang
 * @since 2019-02-28
 */
public interface SysFileDAO {
    
	/**
	 * 文件上传
	 * @param sysFileDTO
	 * @return
	 */
	public int uploadFile(SysFileDTO sysFileDTO);
	
	public List<SysFileDTO> getFileListNoDocument(SysFileDTO sysFileDTO);
	
	public List<SysFileDTO> getFileListHaveDocument(SysFileDTO sysFileDTO);
	
	public SysFileDTO getFileById(String id);
	
	public List<SysFileDTO> getFileByFileName(String fileName);
	
	public void deleteFileById(String id);
	
	public void updateFileById(SysFileDTO sysFileDTO);
	
}
