package zzx.controller;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import zzx.entity.MenuDTO;
import zzx.exception.HandException;
import zzx.service.RoleMenuService;
import zzx.util.JsonUtil;

@Controller
@RequestMapping("roleMenu")
public class RoleMenuController {
	
	@Resource
	public RoleMenuService roleMenuService;
	
	@RequestMapping("/findAllMenuDTOByRoleId")
	@ResponseBody
	public JsonUtil findAllMenuDTOByRoleId(@RequestParam Integer roleid){
		JsonUtil json=new JsonUtil();
		try {
			List<MenuDTO> menuDTO=roleMenuService.findAllMenuDTOByRoleId(roleid);
			json.setData(menuDTO);
			json.setMessage("查询成功");
			json.setSuccess(true);
		} catch (HandException e) {
			e.printStackTrace();
			json.setMessage(e.getMessage());
			json.setSuccess(false);
			return json;
		}
		return json;
		
	}
	
	@RequestMapping("/saveOrDeleteRoleMenuDTO")
	@ResponseBody
	public JsonUtil  saveOrDeleteRoleMenuDTO(Integer roleid,String menuids){
		JsonUtil json=new JsonUtil();
		try {
			roleMenuService.saveOrDeleteRoleMenuDTO(roleid, menuids);
			json.setData(null);
			json.setMessage("保存成功");
			json.setSuccess(true);
		} catch (HandException e) {
			e.printStackTrace();
			json.setMessage(e.getMessage());
			return json;
		}
		return json;
	}

}
