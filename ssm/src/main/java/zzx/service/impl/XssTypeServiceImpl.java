package zzx.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import zzx.dao.XssTypeDAO;
import zzx.entity.XssTypeDTO;
import zzx.exception.HandException;
import zzx.service.XssTypeService;

@Service(value="xssTypeService")
public class XssTypeServiceImpl implements XssTypeService {

	@Resource
	private XssTypeDAO xssTypeDAO;
	
	
	@Override
	public List<XssTypeDTO> selectListByMap(Map<String, Object> map) {
		
		return xssTypeDAO.selectListByMap(map);
	}

	@Override
	public XssTypeDTO selectById(Integer id) throws HandException {
		
		return xssTypeDAO.selectById(id);
	}

	@Override
	public void saveOne(XssTypeDTO t) throws HandException {
		
		xssTypeDAO.saveOne(t);
	}

	@Override
	public void updateById(XssTypeDTO t) throws HandException {
		
		xssTypeDAO.updateById(t);
	}

	@Override
	public void deleteById(Integer id) throws HandException {
		xssTypeDAO.deleteById(id);

	}

	@Override
	public List<XssTypeDTO> selectListByDTO(XssTypeDTO t) {
		return xssTypeDAO.selectListByDTO(t);
	}

}
