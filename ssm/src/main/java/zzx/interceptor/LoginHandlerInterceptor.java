package zzx.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import zzx.util.StringUtil;
/**
 * 登陆拦截器
 * @author 郑仔祥
 *
 */
public class LoginHandlerInterceptor implements HandlerInterceptor {

	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
			//获取URL
			String uri=request.getRequestURI();
			if("/ssm/user/register.do".equals(uri)) {
				return true;
			}
			//得到session中的用户名和密码
			HttpSession session=request.getSession();
			String username=(String)session.getAttribute("username");
			if(StringUtil.isBlank(username)){
				 response.sendRedirect(request.getContextPath()+"/login.jsp");
				 return false;
			}
		return true;
	}

	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {

	}

	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {

	}

}
