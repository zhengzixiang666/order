package zzx.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import zzx.entity.XssDTO;
import zzx.exception.HandException;
import zzx.util.JsonUtil;
import zzx.util.XssUtil;

@Controller //这个注解正式时使用
@RequestMapping("xss")
public class XssController {
	
	@RequestMapping("/index")
	public String index(){
		return "xss/index";
	}

	@RequestMapping("/test")
	@ResponseBody
	public String test(XssDTO data){
		String value=data.getData();
		if(!data.isIfXss()) {
			//如果不进行攻击则执行替换方法
			value=XssUtil.xssKhMethod(data.getData());
		}
		System.out.println(value);
		return value;
	}
	
	@RequestMapping("/model")
	public String model(){
		return "xss/model";
	}
	
	@RequestMapping("/script")
	public String test(){
		return "xss/script";
	}
	
	
	@RequestMapping("/xss")
	@ResponseBody
	public JsonUtil editMain(XssDTO xssDTO){
		JsonUtil json=new JsonUtil();
		try {
			json.setData(xssDTO);
		} catch (HandException e) {
			e.printStackTrace();
			return json;
		}
		return json;
	}
	
	@RequestMapping("/toTest")
	public String toTest(ModelAndView mv){
		return "xss/test";
	}
	
	@RequestMapping("/dom")
	public String dom(){
		return "xss/dom";
	}
	
	@RequestMapping("/desc")
	public String desc(){
		return "xss/desc";
	}
}
