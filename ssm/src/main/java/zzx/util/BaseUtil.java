package zzx.util;




import java.io.IOException;
import java.io.InputStream;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

public class BaseUtil {
	
	private static final String resource="mybatis-config.xml";
	
	private static SqlSession openSession;
	
	/**
	 * 
	 * @return
	 * @throws IOException
	 */
	public static SqlSessionFactory getSqlSessionFactory() throws IOException{
		InputStream inputStream=Resources.getResourceAsStream(resource);
		return new SqlSessionFactoryBuilder().build(inputStream);
	}
	/**
	 * 
	 * @return
	 * @throws IOException
	 */
	public static SqlSession getSqlSession() throws IOException{
		SqlSessionFactory sqlSessionFactory=BaseUtil.getSqlSessionFactory();
		openSession=sqlSessionFactory.openSession();
		return openSession;
	}
	/**
	 * 
	 * 
	 */
	public static void closeOpenSession(SqlSession openSession) {
		openSession.close();
	}
	
}
