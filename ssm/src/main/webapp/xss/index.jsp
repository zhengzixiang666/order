<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ include file="../common/common.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>新增用户</title>
<script type="text/javascript">
function doSave(){
	var data=$('#data').val();
	$('#form').form('submit',{
		url:'test.do',
		data:{"data":data},
		success:function(data){
			
		}
	});
}
function doSave1(){
	var data1=$('#data1').val();
	var ifXss1=$('#ifXss1').val();
	$('#form1').form('submit',{
		url:'test.do?data='+data1+"&ifXss="+ifXss1,
		success:function(data){
			layer.alert('测试成功',{
				skin: 'layui-layer-molv'
			});
		}
	});
}

function doSave3(){
	var data3=$('#data3').val();
	var ifXss3=$('#ifXss3').val();
	$('#form1').form('submit',{
		url:'test.do?data='+data3+"&ifXss="+ifXss3,
		success:function(data){
			layer.alert('测试成功',{
				skin: 'layui-layer-molv'
			});
		}
	});
}
</script>
</head>
<body>
<form id="form1" method="post" >
		    	<table align="center" style="border-collapse:separate; border-spacing:15px;">
		    		<tr>
		    			<td align="right"><label>正常测试：<input id="data1" name="data1" value="13123" style="width: 260px" class="easyui-textbox" type="text" ></input></label></td>
		    		</tr>
		    		<tr>
		    			<td align="right"><input type="hidden" id="ifXss1" name="ifXss1" value="true"/></td>
		    		</tr>
		    		<tr>
		    			<td align="right">
		    				<div align="right"><a href="#" class="easyui-linkbutton" plain="false" iconCls="icon-add" onclick="doSave1()">提 交</a> </div>
		    			</td>
		    		</tr>
		    	</table>
	 </form>
	  <form id="form" method="post" >
		    	<table align="center" style="border-collapse:separate; border-spacing:15px;">
		    		<tr>
		    			<td align="right"><label>测试攻击：<input id="data" name="data" value="<script>alert('ok')</script>" style="width: 260px" class="easyui-textbox" type="text" ></input></label></td>
		    		</tr>
		    		<tr>
		    			<td align="right"><input type="hidden" id="ifXss" name="ifXss" value="true"/></td>
		    		</tr>
		    		<tr>
		    			<td align="right">
		    				<div align="right"><a href="#" class="easyui-linkbutton" plain="false" iconCls="icon-add" onclick="doSave()">提 交</a> </div>
		    			</td>
		    		</tr>
		    	</table>
	 </form>
	 
	 <form id="form3" method="post" >
		    	<table align="center" style="border-collapse:separate; border-spacing:15px;">
		    		<tr>
		    			<td align="right"><label>预防攻击：<input id="data3" name="data3" value="<script>alert('ok')</script>" style="width: 260px" class="easyui-textbox" type="text" ></input></label></td>
		    		</tr>
		    		<tr>
		    			<td align="right"><input type="hidden" id="ifXss3" name="ifXss3" value="false"/></td>
		    		</tr>
		    		<tr>
		    			<td align="right">
		    				<div align="right"><a href="#" class="easyui-linkbutton" plain="false" iconCls="icon-add" onclick="doSave3()">提 交</a> </div>
		    			</td>
		    		</tr>
		    	</table>
	 </form>
</body>
</html>