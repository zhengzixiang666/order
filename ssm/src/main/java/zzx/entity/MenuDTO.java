
package zzx.entity;

import java.util.ArrayList;
import java.util.List;

/**
 * 菜单实体类：tur_menu
 * @author 郑仔祥
 *
 */
public class MenuDTO {
	
	private Integer id;//主键
	
	private Integer parentid;//外键，父菜单id
	
	private String code;//菜单编码
	
	private String text;//菜单名称
	
	private String iconCls;//图标
	
	private Integer isleaf;//是末级，1是，2否
	
	private String url;//请求url
	
	private Integer menuorder;//顺序
	
	private Integer isused;//是否启用，1，是，2否
	
	private Integer menutype;//菜单类型/1，系统管理。2，业务管理。3，基础信息。4，其它
	
	private String remark;//备注，一般备注名称
	
	private String checked;//是否选中
	
	private Integer brotherOrSon;
	
	private List<MenuDTO> children= new ArrayList<MenuDTO>();//孩子列表
	
	public MenuDTO(){}
	


	public MenuDTO(Integer id, Integer parentid, String code, String text, String iconCls, Integer isleaf, String url,
			Integer menuorder, Integer isused, Integer menutype, String remark, String checked,
			List<MenuDTO> children) {
		this.id = id;
		this.parentid = parentid;
		this.code = code;
		this.text = text;
		this.iconCls = iconCls;
		this.isleaf = isleaf;
		this.url = url;
		this.menuorder = menuorder;
		this.isused = isused;
		this.menutype = menutype;
		this.remark = remark;
		this.checked = checked;
		this.children = children;
	}









	public Integer getBrotherOrSon() {
		return brotherOrSon;
	}



	public void setBrotherOrSon(Integer brotherOrSon) {
		this.brotherOrSon = brotherOrSon;
	}



	public String getChecked() {
		return checked;
	}



	public void setChecked(String checked) {
		this.checked = checked;
	}



	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getParentid() {
		return parentid;
	}

	public void setParentid(Integer parentid) {
		this.parentid = parentid;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getIconCls() {
		return iconCls;
	}

	public void setIconCls(String iconCls) {
		this.iconCls = iconCls;
	}

	public Integer getIsleaf() {
		return isleaf;
	}

	public void setIsleaf(Integer isleaf) {
		this.isleaf = isleaf;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Integer getMenuorder() {
		return menuorder;
	}

	public void setMenuorder(Integer menuorder) {
		this.menuorder = menuorder;
	}

	public Integer getIsused() {
		return isused;
	}

	public void setIsused(Integer isused) {
		this.isused = isused;
	}

	public Integer getMenutype() {
		return menutype;
	}

	public void setMenutype(Integer menutype) {
		this.menutype = menutype;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public List<MenuDTO> getChildren() {
		return children;
	}

	public void setChildren(List<MenuDTO> children) {
		this.children = children;
	}



	@Override
	public String toString() {
		return "MenuDTO [id=" + id + ", parentid=" + parentid + ", code=" + code + ", text=" + text + ", iconCls="
				+ iconCls + ", isleaf=" + isleaf + ", url=" + url + ", menuorder=" + menuorder + ", isused=" + isused
				+ ", menutype=" + menutype + ", remark=" + remark + ", checked=" + checked + ", children=" + children
				+ "]";
	}


	
	

}
