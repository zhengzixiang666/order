<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ include file="../common/common.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title></title>
<script type="text/javascript">

if ($.fn.pagination){
    $.fn.pagination.defaults.beforePageText = '第';
    $.fn.pagination.defaults.afterPageText = '共{pages}页';
    $.fn.pagination.defaults.displayMsg = '显示{from}到{to},共{total}记录';
}

//分页功能    
function pagerFilter(data) {
    if (typeof data.length == 'number' && typeof data.splice == 'function') {
        data = {
            total: data.length,
            rows: data
        }
    }
    var dg = $(this);
    var opts = dg.datagrid('options');
    var pager = dg.datagrid('getPager');
    pager.pagination({
        onSelectPage: function (pageNum, pageSize) {
            opts.pageNumber = pageNum;
            opts.pageSize = pageSize;
            pager.pagination('refresh', {
                pageNumber: pageNum,
                pageSize: pageSize
            });
            dg.datagrid('loadData', data);
        }
    });
    if (!data.originalRows) {
        if(data.rows)
            data.originalRows = (data.rows);
        else if(data.data && data.data.rows)
            data.originalRows = (data.data.rows);
        else
            data.originalRows = [];
    }
    var start = (opts.pageNumber - 1) * parseInt(opts.pageSize);
    var end = start + parseInt(opts.pageSize);
    data.rows = (data.originalRows.slice(start, end));
    return data;
}

	$(function() {
		$('#dg').datagrid({
			iconCls : 'icon-a_detail',
			fit : true,
			fitColumns : true,
			rownumbers : true,
			pagination : true,
			loadFilter: pagerFilter,
			checkOnSelect:true,
			border : false,
			striped : true,
			toolbar : '#tb',
			columns : [ [ {
				field : 'id',
				title : '类别id',
				checkbox:  true
			},{
				field : 'orderCode',
				title : '订单编号',
				width : '15%',
				align : 'center'
			},{
				field : 'orderSumNumber',
				title : '订单数量',
				width : '15%',
				align : 'center'
			},{
				field : 'orderSumMoney',
				title : '总金额',
				width : '15%',
				align : 'center'
			},{
				field : 'orderStatus',
				title : '订单状态',
				width : '15%',
				align : 'center',
				formatter : function(value){
                   if(value==1){
                	   return "未支付"
                   }
                   if(value==2){
                	   return "已支付->待审核"
                   }
                   if(value==3){
                	   return "已完成"
                   }
                }
			},{
				field : 'orderDate',
				title : '订单日期',
				width : '15%',
				align : 'center',
				formatter : function(value){
                    var date = new Date(value);
                   return formatDate(date,"yyyy-MM-dd");
                }
			},{
				field : 'field',
				title : '操作',
				width : '25%',
				align : 'center',
				formatter:function(value,row,index){
					var data="<a href='#' name='searchRow' class='easyui-linkbutton' onclick='searchRow(" + row.id + ")' title='详情'></a>"
					;
					return data;
				}
				
			}]],
			onLoadSuccess:function(data){    
				$("a[name='searchRow']").linkbutton({text:'详情',plain:false,iconCls:'icon-search'}); 
		        $("a[name='pay']").linkbutton({text:'支付',plain:false,iconCls:'icon-edit'}); 
		        $("a[name='deleteRow']").linkbutton({text:'删除',plain:false,iconCls:'icon-cancel'});    
			}
		});
		reloadData();
	});
	//加载数据表格
	function reloadData(){
		$.ajax({
			url:'findDTO.do',
			type:'post',
			dataType:'json',
			data:{"orderStatus":3},
			success:function(data){
				var json=data.data;
				success(json);
			},
			error:function(){
				alert('error');
			}
		});
	}
	//处理枚举类的显示，并加载表格
	function success(json){
		
		$('#dg').datagrid('loadData',json);
	}
	
	//查询
	function doQuery(){
		var orderCode=jQuery('#orderCode').val();
		//var name=jQuery('#name').val();
		$.ajax({
			url:'findDTO.do',
			type:'post',
			data:{"orderCode":orderCode,"orderStatus":3},
			dataType:'json',
			success:function(data){
				var json=data.data;
				success(json);
			},
			error:function(){
				alert('请求失败');
			}
		});
	}
	
	function searchRow(id){
		layer.open({
			type:2,
			title:'当前位置 : 订单详情',
			content:'list.do?id='+id,
			skin:'layui-layer-lan',
			area:['900px','600px'],
			maxmin:false,
			btn: ['关闭'],
			yes: function(index, layero){
				reloadData();
				layer.close(index);
			  },
			cancel: function(index, layero){ 
				 reloadData();
				  layer.close(index)
				}   
			});
	}
	
</script>
</head>
<body>
	<div id="tb" style="padding:15px;background-color: #ADD8E6;">
			<div>
				当前位置：商品列表
			</div>
			<div style="padding:15px">
				<span style="margin-left: 10%">商品编号:</span>
				<input id="orderCode" name="orderCode" class="easyui-textbox" type="text">
				<a href="#" class="easyui-linkbutton" style="margin-left:20%;" iconCls="icon-search" onclick="doQuery()">查 询</a>
			</div>
	</div>
	<table id="dg">
	</table>
<!-- 	<div id="dd"></div> -->
</body>
</html>