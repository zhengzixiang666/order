package zzx.entity;

/**
 * 枚举实体类
 * @author 郑仔祥
 *
 */
public class EnumDTO {
	
	public Integer id;//枚举主键
	
	public Integer otherid;//对应表中枚举id
	
	public String enumword;//枚举关键字
	
	public String code;//枚举编号
	
	public String name;//枚举名称
	
	public Integer isused;//是否启用/1，启用，2，不启用
	
	public String remark;//备注
	
	public String ext;//备用字段
	
	public EnumDTO(){}

	public EnumDTO(Integer id, Integer otherid, String enumword, String code, String name, Integer isused,
			String remark, String ext) {
		this.id = id;
		this.otherid = otherid;
		this.enumword = enumword;
		this.code = code;
		this.name = name;
		this.isused = isused;
		this.remark = remark;
		this.ext = ext;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getOtherid() {
		return otherid;
	}

	public void setOtherid(Integer otherid) {
		this.otherid = otherid;
	}

	public String getEnumword() {
		return enumword;
	}

	public void setEnumword(String enumword) {
		this.enumword = enumword;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getIsused() {
		return isused;
	}

	public void setIsused(Integer isused) {
		this.isused = isused;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getExt() {
		return ext;
	}

	public void setExt(String ext) {
		this.ext = ext;
	}

	@Override
	public String toString() {
		return "EnumDTO [id=" + id + ", otherid=" + otherid + ", enumword=" + enumword + ", code=" + code + ", name="
				+ name + ", isused=" + isused + ", remark=" + remark + ", ext=" + ext + "]";
	}
	
	
	
	
	

}
