/*
 Navicat Premium Data Transfer

 Source Server         : 127.0.0.1
 Source Server Type    : MySQL
 Source Server Version : 80027
 Source Host           : localhost:3306
 Source Schema         : zzx

 Target Server Type    : MySQL
 Target Server Version : 80027
 File Encoding         : 65001

 Date: 13/12/2021 14:56:11
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for apply_member
-- ----------------------------
DROP TABLE IF EXISTS `apply_member`;
CREATE TABLE `apply_member`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NULL DEFAULT NULL COMMENT '用户id',
  `user_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '用户名',
  `level` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '给予的会员等级',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '申请备注',
  `status` int NULL DEFAULT NULL COMMENT '申请状态:0：已申请,1:申请通过,2:申请失败',
  `audit_remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '审核理由',
  `apply_date` datetime NULL DEFAULT NULL COMMENT '申请时间',
  `audit_date` datetime NULL DEFAULT NULL COMMENT '审核时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of apply_member
-- ----------------------------

-- ----------------------------
-- Table structure for bus_food
-- ----------------------------
DROP TABLE IF EXISTS `bus_food`;
CREATE TABLE `bus_food`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `food_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '商品编号',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '商品名称',
  `money` decimal(12, 2) NULL DEFAULT NULL COMMENT '价格',
  `factory` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '厂家',
  `factory_date` datetime NULL DEFAULT NULL COMMENT '生产日期',
  `create_date` datetime NULL DEFAULT NULL COMMENT '创建日期',
  `user_id` int NULL DEFAULT NULL COMMENT '创建人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of bus_food
-- ----------------------------

-- ----------------------------
-- Table structure for bus_order
-- ----------------------------
DROP TABLE IF EXISTS `bus_order`;
CREATE TABLE `bus_order`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `order_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '订单号',
  `order_sum_number` int NULL DEFAULT NULL COMMENT '订单总数',
  `order_date` datetime NULL DEFAULT NULL COMMENT '订单日期',
  `order_desc` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '订单说明',
  `order_status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '订单状态，1，未提交，2，待确认，3，已完成',
  `user_id` int NULL DEFAULT NULL COMMENT '用户id',
  `order_sum_money` decimal(12, 2) NULL DEFAULT NULL COMMENT '订单总价',
  `pay_money` decimal(12, 2) NULL DEFAULT NULL COMMENT '支付价格',
  `food_user_id` int NULL DEFAULT NULL COMMENT '商品用户id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 18 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of bus_order
-- ----------------------------

-- ----------------------------
-- Table structure for bus_order_detail
-- ----------------------------
DROP TABLE IF EXISTS `bus_order_detail`;
CREATE TABLE `bus_order_detail`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `food_id` int NULL DEFAULT NULL COMMENT '商品id',
  `order_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '订单号',
  `order_number` int NULL DEFAULT NULL COMMENT '数量',
  `order_date` datetime NULL DEFAULT NULL COMMENT '订单日期',
  `order_status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '订单状态，1，未提交，2，待确认，3，已完成',
  `user_id` int NULL DEFAULT NULL COMMENT '用户id',
  `order_money` decimal(12, 2) NULL DEFAULT NULL COMMENT '订单价格',
  `food_user_id` int NULL DEFAULT NULL COMMENT '商品用户id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 37 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of bus_order_detail
-- ----------------------------

-- ----------------------------
-- Table structure for sys_file
-- ----------------------------
DROP TABLE IF EXISTS `sys_file`;
CREATE TABLE `sys_file`  (
  `id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `login_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户名',
  `suffix_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文件后缀名',
  `file_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文件类型',
  `file_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文件名称',
  `message` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文件描述',
  `create_time` datetime(6) NULL DEFAULT NULL COMMENT '上传时间',
  `document` longblob NULL COMMENT '文件字节',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_file
-- ----------------------------
INSERT INTO `sys_file` VALUES ('EE3A7C282B0B4DE78D474788163D725C', 'zzx', '.txt', 'text/plain', 'DOM脚本', 'DOM脚本', '2021-06-05 13:15:56.030000', 0x3C7363726970743E0D0A77696E646F772E6F6E6C6F61643D66756E6374696F6E28297B0D0A202020766172206C696E6B3D646F63756D656E742E676574456C656D656E747342795461674E616D6528226122293B0D0A202020666F7228766172206A3D303B6A3C6C696E6B2E6C656E6774683B6A2B2B297B0D0A2020202020206C696E6B5B6A5D2E687265663D227777772E62616964752E636F6D223B0D0A2020207D0D0A7D0D0A3C2F7363726970743E);

-- ----------------------------
-- Table structure for tco_enum
-- ----------------------------
DROP TABLE IF EXISTS `tco_enum`;
CREATE TABLE `tco_enum`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '枚举主键',
  `otherid` int NOT NULL COMMENT '对应表中枚举id',
  `enumword` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '枚举关键字',
  `code` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '枚举编号',
  `name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '枚举名称',
  `isused` int NULL DEFAULT NULL COMMENT '是否启用/1，启用，2，不启用',
  `remark` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `ext` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备用字段',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 24 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tco_enum
-- ----------------------------
INSERT INTO `tco_enum` VALUES (1, 1, 'isused', '01', '启用', 1, 'zzx', NULL);
INSERT INTO `tco_enum` VALUES (2, 2, 'isused', '02', '不启用', 2, 'zzx', NULL);
INSERT INTO `tco_enum` VALUES (3, 1, 'attrbute', '01', '超级管理员', 1, 'zzx', NULL);
INSERT INTO `tco_enum` VALUES (4, 2, 'attrbute', '02', '管理员', 1, 'zzx', NULL);
INSERT INTO `tco_enum` VALUES (5, 3, 'attrbute', '03', '用户', 1, 'zzx', NULL);
INSERT INTO `tco_enum` VALUES (6, 2, 'isleaf', '2', '菜单', 1, 'zzx', NULL);
INSERT INTO `tco_enum` VALUES (7, 1, 'isleaf', '1', '功能', 1, 'zzx', NULL);
INSERT INTO `tco_enum` VALUES (8, 1, 'yesno', '01', '是', 1, 'zzx', NULL);
INSERT INTO `tco_enum` VALUES (9, 2, 'yesno', '02', '否', 1, 'zzx', NULL);
INSERT INTO `tco_enum` VALUES (10, 1, 'menutype', '01', '系统管理', 1, 'zzx', NULL);
INSERT INTO `tco_enum` VALUES (11, 2, 'menutype', '02', '业务管理', 1, 'zzx', NULL);
INSERT INTO `tco_enum` VALUES (12, 3, 'menutype', '03', '基础信息', 1, 'zzx', NULL);
INSERT INTO `tco_enum` VALUES (13, 4, 'menutype', '04', '其它', 1, 'zzx', NULL);
INSERT INTO `tco_enum` VALUES (14, 1, 'xss_type', '01', '反射型XSS', 1, 'zzx', NULL);
INSERT INTO `tco_enum` VALUES (15, 2, 'xss_type', '02', '存储型XSS', 1, 'zzx', NULL);
INSERT INTO `tco_enum` VALUES (16, 3, 'xss_type', '03', 'DOMXSS', 1, 'zzx', NULL);
INSERT INTO `tco_enum` VALUES (17, 0, 'memberStatus', '01', '已申请', 1, NULL, NULL);
INSERT INTO `tco_enum` VALUES (18, 1, 'memberStatus', '02', '申请通过', 1, NULL, NULL);
INSERT INTO `tco_enum` VALUES (19, 2, 'memberStatus', '03', '申请驳回', 1, NULL, NULL);
INSERT INTO `tco_enum` VALUES (20, 1, 'memberLevel', '01', '普通用户', 1, NULL, '1');
INSERT INTO `tco_enum` VALUES (21, 2, 'memberLevel', '02', '一级会员', 1, NULL, '0.7');
INSERT INTO `tco_enum` VALUES (22, 3, 'memberLevel', '03', '二级会员', 1, NULL, '0.8');
INSERT INTO `tco_enum` VALUES (23, 4, 'memberLevel', '04', '三级会员', 1, NULL, '0.9');

-- ----------------------------
-- Table structure for tur_menu
-- ----------------------------
DROP TABLE IF EXISTS `tur_menu`;
CREATE TABLE `tur_menu`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '菜单主键',
  `parentid` int NULL DEFAULT NULL COMMENT '父菜单id',
  `code` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单编码',
  `text` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单名称',
  `isleaf` int NULL DEFAULT NULL COMMENT '1，功能，2，菜单',
  `url` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '默认请求路径',
  `menuorder` int NULL DEFAULT NULL COMMENT '顺序',
  `isused` int NULL DEFAULT NULL COMMENT '是否启用/1,是，2否',
  `menutype` int NULL DEFAULT NULL COMMENT '菜单类型/1，系统管理。2，业务管理。3，基础信息。4，其它',
  `remark` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `iconCls` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图标',
  `checked` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否选中/true,false',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_tur_menu_parentid`(`parentid`) USING BTREE,
  CONSTRAINT `fk_tur_menu_parentid` FOREIGN KEY (`parentid`) REFERENCES `tur_menu` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1010469 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tur_menu
-- ----------------------------
INSERT INTO `tur_menu` VALUES (1, NULL, '01', 'xx系统', 2, '#', 0, 2, 3, 'admin', 'icon-menu-folder', NULL);
INSERT INTO `tur_menu` VALUES (100, 1, '0100', '业务中心', 2, '#', 1, 1, 3, 'zzx', 'icon-menu-folder', NULL);
INSERT INTO `tur_menu` VALUES (101, 1, '0101', '应用中心', 2, '#', 4, 1, 1, 'zzx', 'icon-menu-folder', NULL);
INSERT INTO `tur_menu` VALUES (10101, 101, '010101', '用户管理', 2, '#', 98, 1, 1, 'zzx', 'icon-menu-folder', NULL);
INSERT INTO `tur_menu` VALUES (10102, 101, '010102', '权限管理', 2, '#', 99, 1, 1, 'zzx', 'icon-menu-folder', NULL);
INSERT INTO `tur_menu` VALUES (10104, 101, '010104', '文件管理', 2, '#', 4, 1, 1, 'zzx', 'icon-menu-folder', NULL);
INSERT INTO `tur_menu` VALUES (1010102, 10101, '01010102', '系统角色', 1, 'role/findAllRoleDTOMain.do', 2, 1, 1, 'zzx', 'icon-menu-file', NULL);
INSERT INTO `tur_menu` VALUES (1010103, 10101, '01010103', '用户设置', 1, 'user/findAllUserDTOMain.do', 3, 1, 1, 'zzx', 'icon-menu-file', NULL);
INSERT INTO `tur_menu` VALUES (1010201, 10102, '01010201', '菜单设置', 1, 'menu/findAllListMenuDTOMain.do', 1, 1, 1, 'zzx', 'icon-menu-file', NULL);
INSERT INTO `tur_menu` VALUES (1010409, 1010466, '2e1084d0-d726-45fb-b31f-6ba5f6c32a24', '商品列表', 1, 'busFood/list.do', 1, 1, 1, '', 'icon-menu-file', NULL);
INSERT INTO `tur_menu` VALUES (1010414, 10104, '8d0850a4-ccf5-46ad-a73f-c94493083fa4', '文件上传', 1, 'file/upload.do', 2, 1, 1, '文件上传', 'icon-menu-file', NULL);
INSERT INTO `tur_menu` VALUES (1010454, 101, '131755AD9D3E45BDA3DC52661186BC40', '商品管理', 2, '#', 1, 1, 1, '', 'icon-menu-folder', NULL);
INSERT INTO `tur_menu` VALUES (1010455, 1010454, '861861B72A2545E489C26B2C66A6C015', '商品信息', 1, 'busFood/save.do', 1, 1, 1, '', 'icon-menu-file', NULL);
INSERT INTO `tur_menu` VALUES (1010456, 101, '0FD9080CA6374DC5BDCD68CC1E415304', '订单审核', 2, '#', 2, 1, 1, '', 'icon-menu-folder', NULL);
INSERT INTO `tur_menu` VALUES (1010457, 1010456, '7432E1CC9F9549F6A15C89C27FF68CE6', '待审核', 1, 'busOrder/wait.do', 1, 1, 1, '', 'icon-menu-file', NULL);
INSERT INTO `tur_menu` VALUES (1010458, 1010456, '121C6EBB7EA2421BA1F2A800B6F7DA39', '已审核', 1, 'busOrder/compet.do', 2, 1, 1, '', 'icon-menu-file', NULL);
INSERT INTO `tur_menu` VALUES (1010459, 1010467, 'F3EF68A2BBFD4B31B9BAB1BC0182E21B', '购物车', 1, 'busOrder/save.do', 2, 1, 1, '', 'icon-menu-file', NULL);
INSERT INTO `tur_menu` VALUES (1010460, 1010467, '7DF56636A811446CAA49BC008EAB4AC9', '我的订单', 1, 'busOrder/mylist.do', 3, 1, 1, '', 'icon-menu-file', NULL);
INSERT INTO `tur_menu` VALUES (1010461, 1010467, '4413493C2CE249C8994E31AF850A1506', '订单明细', 1, 'busOrderDetail/save.do', 4, 1, 1, '', 'icon-menu-file', NULL);
INSERT INTO `tur_menu` VALUES (1010462, 1010468, '7AFD68766E56411EBE331D1BE137D33A', '会员申请', 1, 'apply/list.do', 3, 1, 1, '', 'icon-menu-file', NULL);
INSERT INTO `tur_menu` VALUES (1010463, 1010468, '7B9254851E874729A645603A6E30E01C', '会员审核', 1, 'apply/auditlist.do', 5, 1, 1, '', 'icon-menu-file', NULL);
INSERT INTO `tur_menu` VALUES (1010464, 1010467, 'A15EB6974AF0474E84DE3E58F20B33A7', '个人信息', 1, 'user/info.do', 4, 1, 1, '', 'icon-menu-file', NULL);
INSERT INTO `tur_menu` VALUES (1010465, 1010467, 'F43CAB8E13A54028BE4ACC95483DD1AB', '修改密码', 1, 'user/pass.do', 6, 1, 1, '', 'icon-menu-file', NULL);
INSERT INTO `tur_menu` VALUES (1010466, 100, '1AD0F5B57CC546ECAB9486C43C351D4A', '商品', 2, '', 2, 1, 1, '', 'icon-menu-folder', NULL);
INSERT INTO `tur_menu` VALUES (1010467, 100, 'C9418CBA6930477AA390215F9F0C813E', '个人中心', 2, '', 3, 1, 1, '', 'icon-menu-folder', NULL);
INSERT INTO `tur_menu` VALUES (1010468, 100, '491CB51FF0834347A8724CAAC787B5DF', '会员管理', 2, '', 4, 1, 1, '', 'icon-menu-folder', NULL);

-- ----------------------------
-- Table structure for tur_role
-- ----------------------------
DROP TABLE IF EXISTS `tur_role`;
CREATE TABLE `tur_role`  (
  `roleid` int NOT NULL AUTO_INCREMENT COMMENT '角色id',
  `code` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色编码',
  `name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色名',
  `attrbute` int NULL DEFAULT NULL COMMENT '角色属性/1，超级管理员。2，管理员。3，操作员',
  `isused` int NULL DEFAULT NULL COMMENT '是否启用/1，启用，2，不启用',
  `remark` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`roleid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 30 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tur_role
-- ----------------------------
INSERT INTO `tur_role` VALUES (22, '01', '用户', 3, 1, '操作员');
INSERT INTO `tur_role` VALUES (27, '04', '超级管理员', 3, 1, '系统管理员');
INSERT INTO `tur_role` VALUES (28, '02', '管理员', 3, 1, '管理员');

-- ----------------------------
-- Table structure for tur_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `tur_role_menu`;
CREATE TABLE `tur_role_menu`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '角色菜单关联表主键',
  `roleid` int NOT NULL COMMENT '角色id',
  `menuid` int NOT NULL COMMENT '菜单id',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_tur_role_menu_roleid`(`roleid`) USING BTREE,
  INDEX `fk_tur_role_menu_meunid`(`menuid`) USING BTREE,
  CONSTRAINT `fk_tur_role_menu_meunid` FOREIGN KEY (`menuid`) REFERENCES `tur_menu` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `fk_tur_role_menu_roleid` FOREIGN KEY (`roleid`) REFERENCES `tur_role` (`roleid`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 215 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tur_role_menu
-- ----------------------------
INSERT INTO `tur_role_menu` VALUES (122, 27, 100);
INSERT INTO `tur_role_menu` VALUES (123, 27, 1010409);
INSERT INTO `tur_role_menu` VALUES (125, 27, 101);
INSERT INTO `tur_role_menu` VALUES (126, 27, 10101);
INSERT INTO `tur_role_menu` VALUES (127, 27, 1010102);
INSERT INTO `tur_role_menu` VALUES (128, 27, 1010103);
INSERT INTO `tur_role_menu` VALUES (131, 27, 10102);
INSERT INTO `tur_role_menu` VALUES (132, 27, 1010201);
INSERT INTO `tur_role_menu` VALUES (133, 27, 10104);
INSERT INTO `tur_role_menu` VALUES (134, 27, 1010414);
INSERT INTO `tur_role_menu` VALUES (181, 27, 1010459);
INSERT INTO `tur_role_menu` VALUES (182, 27, 1010460);
INSERT INTO `tur_role_menu` VALUES (183, 27, 1010454);
INSERT INTO `tur_role_menu` VALUES (184, 27, 1010455);
INSERT INTO `tur_role_menu` VALUES (185, 27, 1010456);
INSERT INTO `tur_role_menu` VALUES (186, 27, 1010457);
INSERT INTO `tur_role_menu` VALUES (187, 27, 1010458);
INSERT INTO `tur_role_menu` VALUES (188, 27, 1010461);
INSERT INTO `tur_role_menu` VALUES (190, 22, 1010409);
INSERT INTO `tur_role_menu` VALUES (191, 22, 1010459);
INSERT INTO `tur_role_menu` VALUES (192, 22, 1010460);
INSERT INTO `tur_role_menu` VALUES (193, 22, 1010461);
INSERT INTO `tur_role_menu` VALUES (194, 28, 1010454);
INSERT INTO `tur_role_menu` VALUES (195, 28, 1010455);
INSERT INTO `tur_role_menu` VALUES (196, 28, 1010456);
INSERT INTO `tur_role_menu` VALUES (197, 28, 1010457);
INSERT INTO `tur_role_menu` VALUES (198, 28, 1010458);
INSERT INTO `tur_role_menu` VALUES (199, 27, 1010462);
INSERT INTO `tur_role_menu` VALUES (200, 27, 1010463);
INSERT INTO `tur_role_menu` VALUES (201, 27, 1010464);
INSERT INTO `tur_role_menu` VALUES (202, 27, 1010465);
INSERT INTO `tur_role_menu` VALUES (203, 22, 1010462);
INSERT INTO `tur_role_menu` VALUES (204, 22, 1010464);
INSERT INTO `tur_role_menu` VALUES (205, 22, 1010465);
INSERT INTO `tur_role_menu` VALUES (206, 28, 1010464);
INSERT INTO `tur_role_menu` VALUES (207, 28, 1010465);
INSERT INTO `tur_role_menu` VALUES (208, 28, 1010103);
INSERT INTO `tur_role_menu` VALUES (209, 28, 1010463);
INSERT INTO `tur_role_menu` VALUES (210, 27, 1010466);
INSERT INTO `tur_role_menu` VALUES (211, 27, 1010467);
INSERT INTO `tur_role_menu` VALUES (212, 27, 1010468);
INSERT INTO `tur_role_menu` VALUES (213, 22, 1010466);
INSERT INTO `tur_role_menu` VALUES (214, 22, 1010467);

-- ----------------------------
-- Table structure for tur_user
-- ----------------------------
DROP TABLE IF EXISTS `tur_user`;
CREATE TABLE `tur_user`  (
  `userid` int NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `logincode` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '登陆账号',
  `loginpassword` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '登陆密码',
  `isused` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '是否启用/1，启用，2，不启用',
  `attrbute` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户属性/1|超级管理员/2|管理员/3|操作员',
  `role_str` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色名称列表',
  `ext` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备用字段',
  `createdate` datetime(6) NULL DEFAULT NULL COMMENT '创建时间',
  `bind` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '2' COMMENT '是否已绑定1，已绑定，2，未绑定',
  `user_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1' COMMENT '会员等级:1：普通用户,2:一级会员:3:二级会员:4:三级会员',
  `points` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '积分',
  `discount` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1' COMMENT '折扣',
  PRIMARY KEY (`userid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 44 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tur_user
-- ----------------------------
INSERT INTO `tur_user` VALUES (16, 'admin', '123456', '1', '3', 'admin', '超级管理员', '2021-12-10 17:37:47.779000', '1', '', '0', '1');
INSERT INTO `tur_user` VALUES (27, 'yh', '123456', '1', '3', '用户', '用户', '2021-06-24 14:41:10.775000', '2', '1', '0', '1');
INSERT INTO `tur_user` VALUES (38, 'gly', '123456', '1', '3', '管理员', '管理员', '2021-06-24 14:41:27.831000', '2', '', '0', '1');

-- ----------------------------
-- Table structure for tur_user_role
-- ----------------------------
DROP TABLE IF EXISTS `tur_user_role`;
CREATE TABLE `tur_user_role`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '角色用户关联表主键',
  `roleid` int NOT NULL COMMENT '角色id',
  `userid` int NOT NULL COMMENT '用户id',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `FK_TUR__user_role_ROLEID`(`roleid`) USING BTREE,
  INDEX `FK_tur_user_role_userid`(`userid`) USING BTREE,
  CONSTRAINT `FK_TUR__user_role_ROLEID` FOREIGN KEY (`roleid`) REFERENCES `tur_role` (`roleid`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_tur_user_role_userid` FOREIGN KEY (`userid`) REFERENCES `tur_user` (`userid`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 38 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tur_user_role
-- ----------------------------
INSERT INTO `tur_user_role` VALUES (21, 22, 27);
INSERT INTO `tur_user_role` VALUES (32, 28, 38);
INSERT INTO `tur_user_role` VALUES (34, 27, 16);

-- ----------------------------
-- Table structure for xss_script
-- ----------------------------
DROP TABLE IF EXISTS `xss_script`;
CREATE TABLE `xss_script`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `xss_script` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of xss_script
-- ----------------------------

-- ----------------------------
-- Table structure for xss_type
-- ----------------------------
DROP TABLE IF EXISTS `xss_type`;
CREATE TABLE `xss_type`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `xss_type` int NULL DEFAULT NULL COMMENT 'XSS类型',
  `xss_remark` varchar(3000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'XSS类型说明',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of xss_type
-- ----------------------------
INSERT INTO `xss_type` VALUES (4, 1, '通过将XSS攻击代码放在请求URL上，将其作为输入提交到服务器端。当服务器端解析提交后，XSS代码会随着响应内容一起传回浏览器，最后浏览器解析并执行XSS代码。由于整个过程像一个反射，因此称为反射型XSS');
INSERT INTO `xss_type` VALUES (5, 2, '与反射型XSS“相似”，但不同的是提交的XSS代码会被存储在服务器端，当下一次请求该页面时，不用提交XSS代码，也会触发XSS攻击。例如当进行用户注册时，用户提交一条包含XSS代码的注册信息到服务器端，当用户查看个人信息时候，那些个人信息就会被浏览器当成正常的HTML和JS解析执行，从而触发了XSS攻击.');
INSERT INTO `xss_type` VALUES (6, 3, '与反射型和存储型XSS不同之处在于，DOM XSS不需要服务器的参与，通过浏览器的DOM解析即可触发XSS攻击，避免了服务器端的信息检验和字符过滤。');

SET FOREIGN_KEY_CHECKS = 1;
