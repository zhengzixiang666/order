package zzx.controller;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import zzx.entity.XssScriptDTO;
import zzx.exception.HandException;
import zzx.service.XssScriptService;
import zzx.util.JsonUtil;

@Controller //这个注解正式时使用
@RequestMapping("xssScript")
public class XssScriptController {

	
	@Resource
	public XssScriptService xssScriptService;
	

	@RequestMapping("/save")
	public String type(){
		return "xssScript/save";
	}
	
	@RequestMapping("/findAllDTO")
	@ResponseBody
	public JsonUtil findAllDTO(){
		List<XssScriptDTO> list=xssScriptService.selectListByDTO(null);
		JsonUtil json=new JsonUtil(true,"发送成功",list);
		return json;
	};
	
	@RequestMapping("/findDTO")
	@ResponseBody
	public JsonUtil findDTO(XssScriptDTO xssScriptDTO){
		List<XssScriptDTO> list=xssScriptService.selectListByDTO(xssScriptDTO);
		JsonUtil json=new JsonUtil(true,"查询成功",list);
		return json;
	}
	
	
	@RequestMapping("/addMain")
	public String addMain(){
		
		return "xssScript/addMain";
	}
	
	
	@RequestMapping("/add")
	@ResponseBody
	public JsonUtil add(XssScriptDTO xssScriptDTO){
		JsonUtil json=new JsonUtil();
		try {
			xssScriptService.saveOne(xssScriptDTO);
			json.setMessage("新增成功！");
		} catch (HandException e) {
			e.printStackTrace();
			json.setMessage(e.getMessage());
			return json;
		}
		return json;
	}
	
	/**
	 * 通过用户id，查询用户信息
	 * @param session
	 * @param userId
	 * @return
	 */
	@RequestMapping("/editMain")
	public ModelAndView editMain(@RequestParam Integer id,ModelAndView mv){
		mv.setViewName("xssScript/editMain");
		try {
			XssScriptDTO xssScriptDTO=xssScriptService.selectById(id);
			mv.addObject("editDTO", xssScriptDTO);
		} catch (HandException e) {
			e.printStackTrace();
			return mv;
		}
		return mv;
	}
	
	/**
	 * 更新用户并且分配角色
	 */
	@RequestMapping("/update")
	@ResponseBody
	public JsonUtil update(XssScriptDTO xssScriptDTO){
		JsonUtil json=new JsonUtil();
		try {
			xssScriptService.updateById(xssScriptDTO);
			json.setMessage("更新成功！");
		} catch (HandException e) {
			e.printStackTrace();
			json.setMessage(e.getMessage());
			return json;
		}
		return json;
	}
	
	@RequestMapping("/delete")
	@ResponseBody
	public JsonUtil delete(@RequestParam Integer id){
		
		try {
			xssScriptService.deleteById(id);
			return new JsonUtil(true,"删除成功",null);
		} catch (HandException e) {
			e.printStackTrace();
			return new JsonUtil(false,e.getMessage(),null);
		}
		
	}
}
