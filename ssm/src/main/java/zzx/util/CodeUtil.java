package zzx.util;

import java.util.Date;

public class CodeUtil {
	
	private static final String DEFAULT="BUS";
	
	public static String getCode() {
		Date date =new Date();
		String code=DateTimeUtil.formatDate("yyyyMMddHHmmss", date);
		return DEFAULT+code;
	}

	public static void main(String[] args) {
		System.out.println(CodeUtil.getCode());
	}
}
