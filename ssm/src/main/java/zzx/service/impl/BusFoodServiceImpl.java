package zzx.service.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import zzx.dao.BusFoodDAO;
import zzx.entity.BusFoodDTO;
import zzx.entity.BusOrderDTO;
import zzx.entity.BusOrderDetailDTO;
import zzx.entity.EnumDTO;
import zzx.entity.UserDTO;
import zzx.service.BusFoodService;
import zzx.service.BusOrderDetailService;
import zzx.service.BusOrderService;
import zzx.service.EnumService;
import zzx.service.UserService;
import zzx.util.CodeUtil;
import zzx.vo.BusOrderDetailVo;
@Service("busFoodService")
public class BusFoodServiceImpl extends BaseServiceImpl<BusFoodDTO, BusFoodDAO> implements BusFoodService{

	@Resource
	private BusOrderService busOrderService;
	
	@Resource
	private BusOrderDetailService busOrderDetailService;
	
	@Resource
	private UserService userService;
	
	@Resource
	private EnumService enumService;
	
	@Override
	public void saveOrderAndDetail(BusOrderDetailVo vo) {
		List<BusOrderDetailDTO> list=vo.getOrderList();
		String orderCode=CodeUtil.getCode();
		BigDecimal sumMoney=new BigDecimal(0);
		BigDecimal discount=new BigDecimal(1);//折扣
		BigDecimal payMoney=new BigDecimal(1);//支付金额
		Integer sumOrderNumber=0;
		Integer userId=null;
		Date date=new Date();
		BusOrderDTO order=new BusOrderDTO();
		if(!CollectionUtils.isEmpty(list)) {
			for (BusOrderDetailDTO busOrderDetailDTO : list) {
				busOrderDetailDTO.setOrderCode(orderCode);
				busOrderDetailDTO.setOrderDate(date);
				busOrderDetailDTO.setOrderStatus(1);
				sumMoney=sumMoney.add(busOrderDetailDTO.getOrderMoney().multiply(new BigDecimal(busOrderDetailDTO.getOrderNumber())));
				sumOrderNumber+=busOrderDetailDTO.getOrderNumber();
				userId=busOrderDetailDTO.getUserId();
				busOrderDetailService.saveOne(busOrderDetailDTO);
			}
		}
		order.setOrderStatus(1);
		order.setOrderCode(orderCode);
		order.setOrderDate(date);
		order.setOrderSumNumber(sumOrderNumber);
		order.setOrderSumMoney(sumMoney);
		order.setUserId(userId);
		//增加新的方法。通过用户id判断一下折扣，如果有折扣，算一下支付金额，如果折扣没有，name按照原价算，同时更新积分
		UserDTO user = userService.findUserDTOById(userId);
		//判断会员等级，1
		if(null !=user.getUserType() && !"".equals(user.getUserType())) {
				discount = new BigDecimal(user.getDiscount());
		
		}else {
			discount = new BigDecimal(1);
		}
		payMoney = sumMoney.multiply(discount);
		order.setPayMoney(payMoney);
		busOrderService.saveOne(order);
	}

}
