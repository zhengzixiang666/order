<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ include file="common/common.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
    <head>
    	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!--         <meta http-equiv="X-UA-Compatible" content="IE=edge"> -->
<!--         <meta name="viewport" content="width=device-width, initial-scale=1"> -->
        <title>西财商场会员管理系统</title>
        <!-- CSS -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap-select.min.css">
        <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="assets/css/form-elements.css">
        <link rel="stylesheet" href="assets/css/style.css">
        <!-- Favicon and touch icons -->
        <link rel="shortcut icon" href="assets/ico/favicon.png">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">
  		<!-- Javascript -->
        <script src="assets/js/jquery-1.11.1.min.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/bootstrap/js/bootstrap-select.min.js"></script>
        <script src="assets/js/jquery.backstretch.min.js"></script>
<!--         <script src="assets/js/scripts.js"></script> -->
        <script type="text/javascript">
        	if (window != top) top.location.href = location.href; 
        	function doLogin(){
        		$.ajax({
					type:'post',
					data:{"username":$('#username').val(),"password":$('#password').val(),"roleCode":$('#roleCode').val()},
					url:'user/login.do',
					dataType:'json',
					success:function(json){
						if(json.success){
							//登陆成功跳转页面到index.jsp
							document.cookie ="loginCode="+$('#username').val();
							document.cookie ="password="+$('#password').val();
							document.cookie ="userId="+json.data.userId;
							window.location.href="index.jsp?userId="+json.data.userId+"&loginCode="+json.data.loginCode;
							
						}else{
							$("#span").html(json.message);
						}
					},
					error:function(json){
						alert(json.message);
					}
				});
        	}
        	
        	function doSave(){
        		$.ajax({
					type:'post',
					data:{"loginCode":$('#loginCode').val(),"loginPassWord":$('#loginPassWord').val(),"ext":$('#ext').val()},
					url:'user/register.do',
					dataType:'json',
					success:function(json){
						if(json.success){
							alert("注册成功！");
							$('#register').hide();
						}else{
							$("#span2").html(json.message);
						}
					},
					error:function(json){
						alert(json.message);
					}
				});
        	}
        	
        	function showLogin(){
        		$('#login').show();
        		$('#register').hide();
        	}
        	function cancel(){
        		$('#login').hide();
        		$('#register').hide();
        		location.reload();
        	}
        	function showRegister(){
        		$('#register').show();
        		$('#login').hide();
        	}
        </script>
        <style type="text/css">
        	html{
        		height: 100%;
        	}
        	body{
        		background-image: url(assets/img/backgrounds/4.jpg);
        		background-size: 100% 100%;
        		background-repeat: no repeat;
        	}
        	.tab{
        		float: right;
        		margin-right: 40px;
        		margin-top: 20px;
        		position: relative;
        	}
        	.top-content{
        		display: none;
        	}
        	.container{
        		width: 30%;
        		margin-top: 50px;
        		height:400px;
        	}
        	.titles{  
        		    	
        	}
        </style>
    </head>
    <body>
    <div class="tab">
    	<a href="#" plain="false" style="color: white;font-weight: bold;font-size: 18px;" onclick="showLogin()">登 陆</a>
    	<span style="color: white;font-weight: bold;"> / </span>
    	<a href="#" plain="false" style="color: white;font-weight: bold;font-size: 18px;" onclick="showRegister()">注 册</a>
    </div>
     <div  style="float:left;width:100%;margin-top:80px;color:white;font-size:40px">
           <div>  欢 迎 使 用 </div> 
           <div style="margin-top:40px">   西 财 商 场 会 员 管 理 系 统</div> 
     </div>
     <div class="top-content" id="login">
         <div class="inner-bg">
             <div class="container" >
                   	<div class="form-top" style="height: 60px;background-color: #4c6295;text-align:center;padding-top:15px;">
                   		<a href="#">
                   	  		<span style="color: #DCDCDC;font-weight: bold;margin-top: 5px;font-size:25px">登  陆</span>
                   	  	</a>
                   	  	<a href="#" onclick="cancel()">
			          <span class="glyphicon glyphicon-remove" style="color: #DCDCDC;float: right;"></span>
			        </a>
                       </div>
                       <div class="form-bottom" >
                    <form role="form" action="" method="post" class="login-form">
                    	<div class="form-group">
                        	用户名：<input type="text" name="username"  style="width:100%;height: 40px;" id="username">
                        </div>
                        <div class="form-group">
                        	密码：<input type="password" name="password" title="密码"  style="width:100%;height: 40px;" id="password">
                        </div>
<!--                         <div class="form-group"> -->
<!--                         	验证码：<input type="text" name="yzm" title="验证码"  style="width:100%;height: 40px;" id="yzm"> -->
<!--                         </div> -->
                   		<div align="center"><a href="#" class="easyui-linkbutton" plain="false" style="color: #065081;font-weight: bold;width: 80px;"  onclick="doLogin()">登  陆</a> </div>
                   		<div><span id="span" style="color: red;font-weight: bold;"></span></div>
                    </form>
                 </div>
             </div>
         </div>
     </div>
     
     
     <div class="top-content" id="register">
         <div class="inner-bg">
             <div class="container" >
                   	   <div class="form-top" style="height: 40px;background-color: #4c6295">
                   		<a href="#">
                   	  		<span style="color: #DCDCDC;font-weight: bold;margin-top: 5px;float: left;">注册用户</span>
                   	  	</a>
                   	  	<a href="#" onclick="cancel()">
			          		<span class="glyphicon glyphicon-remove" style="color: #DCDCDC;float: right;margin-top: 10px;"></span>
			       		</a>
                       </div>
                       <div class="form-bottom" >
                    	<form id="form2" method="post" class="login-form">
		                    	<div class="form-group">
		                        	用户名：<input type="text" name="loginCode"  style="width:100%;height: 40px;" id="loginCode">
		                        </div>
		                        <div class="form-group">
		                        	密码：<input type="text" name="loginPassWord" title="密码"  style="width:100%;height: 40px;" id="loginPassWord">
		                        </div>
		                         <div class="form-group">
		                        	备注：<input type="text" name="ext" title="密码"  style="width:100%;height: 40px;" id="ext">
		                        </div>
                    			<div align="center"><a href="#" class="easyui-linkbutton" plain="false" style="color: #065081;font-weight: bold;width: 80px;"  onclick="doSave()">保 存</a> </div>
<!-- 					    	<table align="center"> -->
<!-- 					    		<tr> -->
<!-- 					    			<td align="right"><label>登陆账号：<input id="loginCode" name="loginCode"  class="easyui-textbox" type="text" ></input></label></td> -->
<!-- 					    		</tr> -->
<!-- 					    		<tr> -->
<!-- 					    			<td align="right"><label>密码：<input id="loginPassWord" name="loginPassWord"  class="easyui-textbox" ></input></label></td> -->
<!-- 					    		</tr> -->
<!-- 					    		<tr> -->
<!-- 					    			<td align="right"><label>备注：<input id="ext" name="ext" class="easyui-textbox" type="text"></input></label></td> -->
<!-- 					    		</tr> -->
<!-- 					    		<tr> -->
<!-- 					    			<td colspan="2"><div align="right"><a href="#" class="easyui-linkbutton" plain="false" iconCls="icon-add" onclick="doSave()">保 存</a> </div></td> -->
<!-- 					    		</tr> -->
<!-- 					    	</table> -->
					    	<div><span id="span2" style="color: red;font-weight: bold;"></span></div>
					   </form>
                 </div>
             </div>
         </div>
     </div>
    </body>
</html>