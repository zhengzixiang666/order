<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ include file="../common/common.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>新增用户</title>
<script type="text/javascript">
	function doUpdate(){
		$('#form').form('submit',{
			url:'update.do',
			success:function(data){
				var data = eval('(' + data + ')');//将json对象转为js对象
				layer.alert(data.message,{
					skin: 'layui-layer-molv',
					yes:function(){
						location.reload();
					}
				});
			}
		});
	}
</script>
</head>
<body>
	<div style="margin-top: 20px">
	   	<form id="form" method="post" >
		    	<table align="center">
		    		<tr>
		    			<td><input type="hidden" id="id" name="id" value="${editDTO.id}"/></td>
		    		</tr>
		    		<tr>
		    			<td align="left"><label>XSS类型：<input id="xssType" value="${editDTO.xssType}" style="width:450px;" name="xssType" class="easyui-combobox"  panelHeight="auto"
    						data-options="valueField:'otherid',textField:'name',url:'<%= this.getServletContext().getContextPath()%>/enum/findEnumDTOByEnumWord.do?enumWord=xss_type'"></input></label></td>
		    		</tr>
		    		<tr>
		    			<td align="left"><label>说明：<input id="xssRemark" value="${editDTO.xssRemark}" name="xssRemark" data-options="multiline:true" class="easyui-textbox" type="text" style="width:500px;height:150px"></input></label></td>
		    		</tr>
		    		<tr>
		    			<td colspan="2"><div align="right"><a href="#" class="easyui-linkbutton" plain="false" iconCls="icon-add" onclick="doUpdate()">保 存</a> </div></td>
		    		</tr>
		    	</table>
		   </form>
	   </div>
</body>
</html>