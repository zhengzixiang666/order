package zzx.enumutil;

public enum BindCodeEnum implements BaseStringEnum {
	
	BIND_CODE_1("1","已绑定"),
	BIND_CODE_2("2","未绑定");
	
	public String value;
	
	public String dispalyName;
	
	private BindCodeEnum(String value, String dispalyName) {
		this.value = value;
		this.dispalyName = dispalyName;
	}

	//通过code找名称
	//根据值获取枚举
	public static String getBindCodeEnumByValue(String value){
		for (BindCodeEnum bindCodeEnum : BindCodeEnum.values()) {
			if(bindCodeEnum.value.equals(value)){
				return bindCodeEnum.dispalyName;
			}
		}
		return null;
	}
	
	@Override
	public String getDisplayName() {
		
		return this.dispalyName;
	}

	@Override
	public String getValue() {
		return this.value;
	}

}
