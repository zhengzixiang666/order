<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ include file="common/common.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>西财商场会员管理系统</title>
<style type="text/css">
</style>
<script type="text/javascript">
	$(function() {
		var roleid=$('#roleid').val();
		var nodeid=",";
		$.ajax({
			url:'roleMenu/findAllMenuDTOByRoleId.do',
			type:'post',
			data:{"roleid":roleid},
			dataType:'json',
			success:function(json){
				var dd=json.data;
				for ( var i in dd) {
					nodeid+=dd[i].id+",";
				}
			},
			error:function(){
				alert('error');
			}
		});
		$('#MyTree').tree({
			checkbox : true,
			url : 'menu/findAllMenuDTO.do',
			loadFilter : function(data) {
				return data.data;
			},
			onLoadSuccess:function(node,data){
				//发送ajax，获取角色id,得到的有权限的
				var noids=nodeid.split(",");
				for ( var i in noids) {
					if(noids[i]){
						var note = $(this).tree('find', noids[i]);
		 				$(this).tree('check', note.target);
					}
				}
				
			}
		});
	});
	//执行保存方法
	function doSave(){
		var roleid=$('#roleid').val();
		var nodes = $('#MyTree').tree('getChecked');
		var menuids="";
		if(nodes){
			for ( var i in nodes) {
				menuids+=nodes[i].id+",";
			}
		}
		//发送ajax
		$.ajax({
			url:'roleMenu/saveOrDeleteRoleMenuDTO.do',
			type:'post',
			data:{"roleid":roleid,"menuids":menuids},
			dataType:'json',
			success:function(data){
				layer.alert(data.message,{
					skin: 'layui-layer-molv',
					yes:function(){
						location.reload();
					}
				});
			}
		});
	}
</script>
</head>
<body>
	<input id="roleid" value="${param.roleid}" type="hidden" name="roleid" />
	<a href="#" class="easyui-linkbutton" style="float: right;" iconCls="icon-add" onclick="doSave()">保 存</a>
	<div region="west" title="功能菜单" split="true"
		style="width: 15%; height: 100%;">
		<ul id="MyTree" class="easyui-tree" style="color: black;">
		</ul>
	</div>
	
</body>
</html>