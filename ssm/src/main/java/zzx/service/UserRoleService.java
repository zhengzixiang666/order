package zzx.service;

import java.util.List;


import zzx.entity.RoleDTO;
import zzx.exception.HandException;

/**
 * 用户角色服务层
 * @author 郑仔祥
 *
 */
public interface UserRoleService {
	
	/**
	 * 通过用户id，查询对应的所有角色
	 * @param userId
	 * @return
	 */
	public abstract List<RoleDTO> findAllRoleDTOByUserId(Integer userId)throws HandException;

}
