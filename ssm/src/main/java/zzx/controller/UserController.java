package zzx.controller;


import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import zzx.entity.UserDTO;
import zzx.exception.HandException;
import zzx.service.UserRoleService;
import zzx.service.UserService;
import zzx.util.JsonUtil;

//@RestController //这个注解是测试时使用
@Controller //这个注解正式时使用
@RequestMapping("user")
public class UserController {
	//操作日志暂时先不处理，后期完善
//	private static final Logger logger=LoggerFactory.getLogger(UserController.class);

	@Resource
	public UserService userService;
	
	@Resource
	public UserRoleService userRoleService;
	
	
	@RequestMapping("/login")
	@ResponseBody
	public JsonUtil doLogin(HttpServletRequest request,@RequestParam String username,@RequestParam String password,String roleCode) throws IOException{
		JsonUtil json=new JsonUtil();
		try {
			UserDTO userDTO=userService.findUserDTOLogin(username, password);
			//登陆角色不能为空
//			if(StringUtils.isBlank(roleCode)){
//				json.setMessage("未选择角色，登陆失败！");
//				json.setSuccess(false);
//				return json;
//			}
			//通过用户id查询角色
//			List<RoleDTO> roleDTOList=userRoleService.findAllRoleDTOByUserId(userDTO.getUserId());
//			if(ListUtil.isEmpty(roleDTOList)){
//				json.setMessage("该用户没有分配角色权限，登陆失败，请联系系统管理员！");
//				json.setSuccess(false);
//				return json;
//			}else{
//				boolean isfalse=true;
//				for (RoleDTO roleDTO : roleDTOList) {
//					if(roleCode.equals(roleDTO.getCode())){
//						isfalse=false;
//						break;
//					}
//				}
//				//该用户不存在这个角色
//				if(isfalse){
//					json.setMessage("该用户登陆角色不对，登陆失败！");
//					json.setSuccess(false);
//					return json;
//				}
//			}
			//将用户名和密码存放在session中
			HttpSession session=request.getSession();
			session.setAttribute("username", userDTO.getLoginCode());
			session.setAttribute("userId", userDTO.getUserId());
			session.setAttribute("password", userDTO.getLoginPassWord());
			json.setSuccess(true);
			json.setData(userDTO);
		} catch (Exception e) {
			e.printStackTrace();
			json.setMessage(e.getMessage());
			json.setSuccess(false);
			return json;
		}
		return json;
	}
	
	/**
	 * 通过用户id，查询用户信息
	 * @param session
	 * @param userId
	 * @return
	 */
	@RequestMapping("/findUserDTOByUserId")
	public ModelAndView findUserDTOByUserId(@RequestParam Integer userId,ModelAndView mv){
		mv.setViewName("user/findUserDTOByUserId");
		try {
			Map<String,Object>	map = userService.findUserDTOByUserId(userId);
			mv.addObject("userDTO",(map==null || map.get("userDTO")==null)?null:map.get("userDTO"));
			mv.addObject("roleid",(map==null || map.get("roleid")==null?null:map.get("roleid")));
		} catch (HandException e) {
			e.printStackTrace();
			return mv;
		}
		return mv;
	}
	
	@RequestMapping("/findList")
	@ResponseBody
	public List<UserDTO> findList(){
		return userService.findBindList();
	} 
	
	
	/**
	 * 用户设置的主页面
	 * @return
	 */
	@RequestMapping("/findAllUserDTOMain")
	public String findAllUserDTOMain(){
		
		return "user/findAllUserDTOMain";
	}
	/**
	 * 用户信息
	 * @return
	 */
	@RequestMapping("/info")
	public ModelAndView findUser(HttpServletRequest request,ModelAndView mv){
		mv.setViewName("user/info");
		try {
			HttpSession session=request.getSession();
			Map<String,Object>	map = userService.findUserDTOByUserId(Integer.valueOf(session.getAttribute("userId").toString()));
			mv.addObject("userDTO",(map==null || map.get("userDTO")==null)?null:map.get("userDTO"));
			mv.addObject("roleid",(map==null || map.get("roleid")==null?null:map.get("roleid")));
		} catch (HandException e) {
			e.printStackTrace();
			return mv;
		}
		return mv;
	}
	/**
	 * 用户信息
	 * @return
	 */
	@RequestMapping("/pass")
	public ModelAndView pass(HttpServletRequest request,ModelAndView mv){
		mv.setViewName("user/pass");
		try {
			HttpSession session=request.getSession();
			Map<String,Object>	map = userService.findUserDTOByUserId(Integer.valueOf(session.getAttribute("userId").toString()));
			mv.addObject("userDTO",(map==null || map.get("userDTO")==null)?null:map.get("userDTO"));
			mv.addObject("roleid",(map==null || map.get("roleid")==null?null:map.get("roleid")));
		} catch (HandException e) {
			e.printStackTrace();
			return mv;
		}
		return mv;
	}
	/**
	 * 得到所有用户
	 * @return
	 */
	@RequestMapping("/findAllUserDTO")
	@ResponseBody
	public JsonUtil findAllUserDTO(HttpSession session){
		List<UserDTO> userDTOList=userService.findAllUserDTO();
		JsonUtil json=new JsonUtil(true,"发送成功",userDTOList);
		return json;
	};
	/**
	 * 新增用户的主页面
	 * @return
	 */
	@RequestMapping("/addUserDTOMain")
	public String addUserDTOMain(){
		
		return "user/addUserDTOMain";
	}
	
	/**
	 * 新增用户
	 * @param userDTO
	 * @return
	 */
	@RequestMapping("/addUserDTO")
	@ResponseBody
	public JsonUtil addUserDTO(UserDTO userDTO){
		JsonUtil json=new JsonUtil();
		try {
			userService.createUserDTOByUserDTO(userDTO);
		} catch (HandException e) {
			e.printStackTrace();
			json.setMessage(e.getMessage());
			json.setSuccess(false);
			return json;
		}
		return new JsonUtil(true,"新增用户成功！",null);
	}
	
	
	
	/**
	 * 注册用户
	 * @param userDTO
	 * @return
	 */
	@RequestMapping("/register")
	@ResponseBody
	public JsonUtil register(UserDTO userDTO){
		JsonUtil json=new JsonUtil();
		try {
			userService.registerUserDTOByUserDTO(userDTO);
			json.setMessage("新增用户成功！");
			json.setSuccess(true);
		} catch (HandException e) {
			e.printStackTrace();
			json.setMessage(e.getMessage());
			json.setSuccess(false);
			return json;
		}
		return json;
	}
	
	/**
	 * 条件查询
	 * @param userDTO
	 * @return
	 */
	@RequestMapping("/findUserDTOByUserDTO")
	@ResponseBody
	public JsonUtil findUserDTOByUserDTO(UserDTO userDTO){
		List<UserDTO> userDTOList=userService.findUserDTOByUserDTO(userDTO);
		JsonUtil json=new JsonUtil(true,"查询成功",userDTOList);
		return json;
	}
	
	/**
	 * 进入修改主页面
	 * @param userId
	 * @return
	 */
	@RequestMapping("/findAllRoleDTOByUserIdMain")
	public String findAllRoleDTOByUserIdMain(@RequestParam Integer userId){
		return "redirect:/findAllRoleDTOByUserIdMain.jsp?userId="+userId;
	}
	
	/**
	 * 更新用户并且分配角色
	 */
	@RequestMapping("/updateUserDTOByUserId")
	@ResponseBody
	public JsonUtil updateUserDTOByUserId(UserDTO userDTO,String roleid){
		JsonUtil json=new JsonUtil();
		try {
			userService.updateUserDTOByUserDTO(userDTO, roleid);
			json.setMessage("更新成功！");
		} catch (HandException e) {
			e.printStackTrace();
			json.setMessage(e.getMessage());
			return json;
		}
		return json;
	}
	
	@RequestMapping("/deleteUserDTOByUserId")
	@ResponseBody
	public JsonUtil deleteUserDTOByUserId(@RequestParam Integer userId){
		
		try {
			userService.deleteUserDTOByUserId(userId);
			return new JsonUtil(true,"删除成功",null);
		} catch (HandException e) {
			e.printStackTrace();
			return new JsonUtil(false,e.getMessage(),null);
		}
		
	}
	
}
