<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ include file="../common/common.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>新增用户</title>
<script type="text/javascript">
	function doUpdate(){
		$('#form').form('submit',{
			url:'updateUserDTOByUserId.do',
			success:function(data){
				var data = eval('(' + data + ')');//将json对象转为js对象
				layer.alert(data.message,{
					skin: 'layui-layer-molv',
					yes:function(){
						location.reload();
					}
				});
			}
		});
	}
</script>
</head>
<body>
<div id="tb" style="padding:15px;background-color: #ADD8E6;">
			<div>
				当前位置：个人信息
			</div>
	</div>
	<div style="margin-top: 150px">
	   	<form id="form" method="post" >
		    	<table align="center">
		    		<tr>
		    			<td><input type="hidden" id="userId" name="userId" value="${userDTO.userId}"/></td>
		    		</tr>
		    		<tr>
		    			<td align="right"><label>登陆账号：<input id="loginCode" disabled value="${userDTO.loginCode}" name="loginCode"  class="easyui-textbox" type="text" ></input></label></td>
		    			<td align="right"><label>密码：<input id="loginPassWord" disabled value="${userDTO.loginPassWord}" name="loginPassWord"  class="easyui-textbox" ></input></label></td>
		    		</tr>
		    		<tr>
    					<td align="right"><label>角色名称：<input id="roleStr" disabled value="${userDTO.roleStr}" name="roleStr" class="easyui-textbox" type="text"></input></label></td>
    					<td align="right" ><label>备注：<input id="ext" disabled value="${userDTO.ext}" name="ext" class="easyui-textbox" type="text"></input></label></td>
		    		</tr>
		    		<tr>
		    			<td ><label>是否启用：<input id="isUsed" disabled value="${userDTO.isUsed}" name="isUsed" class="easyui-combobox"  panelHeight="auto"
    						data-options="valueField:'otherid',textField:'name',url:'<%= this.getServletContext().getContextPath()%>/enum/findEnumDTOByEnumWord.do?enumWord=isused'"></input></label></td>
    					<td  ><label>分配角色：<input id="roleid" name="roleid" disabled value="${roleid}" class="easyui-combobox" panelHeight="auto" 
    						data-options="valueField:'roleid',textField:'name',multiple:true,url:'<%= this.getServletContext().getContextPath()%>/role/findRoleDTOByIsUsed.do?isused=1'"></input></label></td>
		    		</tr>
		    		<tr>
		    		<td ><label>会员等级：<input id="userType" name="userType" disabled value="${userDTO.userType}" class="easyui-combobox" panelHeight="auto" 
    						data-options="valueField:'otherid',textField:'name',multiple:true,url:'<%= this.getServletContext().getContextPath()%>/enum/findEnumDTOByEnumWord.do?enumWord=memberLevel'"></input></label></td>
		    		<td align="right" ><label>积分：<input id="ext" disabled value="${userDTO.points}" name="ext" class="easyui-textbox" type="text"></input></label></td>
		    		</tr>
		    		<tr>
		    		<td align="right"><label>折扣：<input id="discount" disabled value="${userDTO.discount}" name="discount" class="easyui-textbox" type="text"></input></label></td>
    						</td>
		    		</tr>
<!-- 		    		<tr> -->
<!-- 		    			<td colspan="2"><div  align="right"><a href="#" class="easyui-linkbutton" plain="false" iconCls="icon-add" onclick="doUpdate()">更新</a> </div></td> -->
<!-- 		    		</tr> -->
		    	</table>
		   </form>
	   </div>
</body>
</html>