<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ include file="../common/common.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title></title>
<script type="text/javascript">

if ($.fn.pagination){
    $.fn.pagination.defaults.beforePageText = '第';
    $.fn.pagination.defaults.afterPageText = '共{pages}页';
    $.fn.pagination.defaults.displayMsg = '显示{from}到{to},共{total}记录';
}

//分页功能    
function pagerFilter(data) {
    if (typeof data.length == 'number' && typeof data.splice == 'function') {
        data = {
            total: data.length,
            rows: data
        }
    }
    var dg = $(this);
    var opts = dg.datagrid('options');
    var pager = dg.datagrid('getPager');
    pager.pagination({
        onSelectPage: function (pageNum, pageSize) {
            opts.pageNumber = pageNum;
            opts.pageSize = pageSize;
            pager.pagination('refresh', {
                pageNumber: pageNum,
                pageSize: pageSize
            });
            dg.datagrid('loadData', data);
        }
    });
    if (!data.originalRows) {
        if(data.rows)
            data.originalRows = (data.rows);
        else if(data.data && data.data.rows)
            data.originalRows = (data.data.rows);
        else
            data.originalRows = [];
    }
    var start = (opts.pageNumber - 1) * parseInt(opts.pageSize);
    var end = start + parseInt(opts.pageSize);
    data.rows = (data.originalRows.slice(start, end));
    return data;
}

	$(function() {
		$('#dg').datagrid({
			iconCls : 'icon-a_detail',
			fit : true,
			fitColumns : true,
			rownumbers : true,
			pagination : true,
			loadFilter: pagerFilter,
			checkOnSelect:true,
			border : false,
			striped : true,
			toolbar : '#tb',
			columns : [ [ {
				field : 'userId',
				title : '用户id',
				checkbox:  true
			},{
				field : 'loginCode',
				title : '登陆账号',
				width : '10%',
				align : 'center'
			}, {
				field : 'attrBute',
				title : '用户属性',
				width : '10%',
				align : 'center'
			},{
				field : 'createdate',
				title : '创建日期',
				width : '10%',
				align : 'center',
				formatter : function(value){
                    var date = new Date(value);
                   return formatDate(date,"yyyy-MM-dd");
                }
			}, {
				field : 'userType',
				title : '会员等级',
				width : '10%',
				align : 'center',
				formatter : function(value){
                   if(value=='1'){
                	   return '非会员'
                   }
                   if(value=='2'){
                	   return '一级会员'
                   }
                   if(value=='3'){
                	   return '二级会员'
                   }
                   if(value=='4'){
                	   return '三级会员'
                   }
                }
			}, {
				field : 'discount',
				title : '折扣',
				width : '15%',
				align : 'center'
			}, {
				field : 'points',
				title : '积分',
				width : '10%',
				align : 'center'
			}, {
				field : 'isUsed',
				title : '是否启用',
				width : '10%',
				align : 'center'
			},{
				field : 'field',
				title : '操作',
				width : '25%',
				align : 'center',
				formatter:function(value,row,index){
					if(row.userId!=16 && row.userId!=38){
						var data=" <a href='#' name='edit' onclick='edit(" + row.userId + ")' title='修改与分配角色'></a> "
						+"\| <a href='#' name='deleteRow' class='easyui-linkbutton' onclick='deleteRow(" + row.userId + ")' title='删除'></a>";
						return data;
					}else{
						return "";
					}
					
				}
				
			}]],
			onLoadSuccess:function(data){    
		        $("a[name='edit']").linkbutton({text:'修改与分配角色',plain:false,iconCls:'icon-edit'}); 
		        $("a[name='deleteRow']").linkbutton({text:'删除',plain:false,iconCls:'icon-cancel'});    
			}
		});
		reloadData();
	});
	//加载数据表格
	function reloadData(){
		$.ajax({
			url:'findAllUserDTO.do',
			type:'post',
			dataType:'json',
			success:function(data){
				var json=data.data;
				success(json);
			},
			error:function(){
				alert('error');
			}
		});
	}
	//处理枚举类的显示，并加载表格
	function success(json){
		for ( var i in json) {
			if(json[i].isUsed==1 || json[i].isUsed=='1'){
				json[i].isUsed='启用';
			}
			if(json[i].isUsed==2 || json[i].isUsed=='2'){
				json[i].isUsed='不启用';
			}
			if(json[i].attrBute==1){
				json[i].attrBute='超级管理员';
			}
			if(json[i].attrBute==2){
				json[i].attrBute='管理员';
			}
			if(json[i].attrBute==3){
				json[i].attrBute='操作员';
			}
		}
		$('#dg').datagrid('loadData',json);
	}
	
	//查询
	function doQuery(){
		var loginCode=jQuery('#loginCode').val();
		var isUsed=jQuery('#isUsed').val();
		$.ajax({
			url:'findUserDTOByUserDTO.do',
			type:'post',
			data:{"loginCode":loginCode,"isUsed":isUsed},
			dataType:'json',
			success:function(data){
				var json=data.data;
				success(json);
			},
			error:function(){
				alert('error');
			}
		});
	}
	//新增
	function doAdd(){
		layer.open({
			type:2,
			title:'当前位置 : 新增用户',
			content:'addUserDTOMain.do',
			skin:'layui-layer-lan',
			area:['600px','300px'],
			maxmin:false,
			btn: ['关闭'],
			yes: function(index, layero){
				reloadData();
				layer.close(index);
			  },
			cancel: function(index, layero){ 
					reloadData();
					layer.close(index)
				}   
			});   
	}
	//修改方法
	function edit(userId){
		layer.open({
			type:2,
			title:'当前位置 : 修改与分配角色',
			content:'findUserDTOByUserId.do?userId='+userId,
			skin:'layui-layer-lan',
			area:['600px','300px'],
			maxmin:false,
			btn: ['关闭'],
			yes: function(index, layero){
				reloadData();
				layer.close(index);
			  },
			cancel: function(index, layero){ 
					reloadData();
				  layer.close(index)
				}   
			}); 
	}

	//删除方法
	function deleteRow(userId){
		$.ajax({
			type:'post',
			data:{"userId":userId},
			url:'deleteUserDTOByUserId.do',
			dataType:'json',
			success:function(json){
				reloadData();
				layer.alert(json.message);
			},
			error:function(){
				layer.alert("删除失败");
			}
		});
	}
	
	
</script>
</head>
<body>
	<div id="tb" style="padding:15px;background-color: #ADD8E6;">
			<div>
				当前位置：用户设置
			</div>
			<div style="padding:15px">
				<span style="margin-left: 10%">登陆账号:</span>
				<input id="loginCode" class="easyui-textbox" type="text">
				<span style="margin-left: 10%">是否启用:</span>
				<input id="isUsed" class="easyui-combobox" panelHeight="auto" name="isUsed" 
    						data-options="valueField:'otherid',textField:'name',url:'<%= this.getServletContext().getContextPath()%>/enum/findEnumDTOByEnumWord.do?enumWord=isused'"></input>
				<a href="#" class="easyui-linkbutton" style="margin-left:20%;" iconCls="icon-search" onclick="doQuery()">查 询</a>
				<a href="#" class="easyui-linkbutton" plain="false" iconCls="icon-add" onclick="doAdd()">新 增</a>
			</div>
	</div>
	<table id="dg">
	</table>
<!-- 	<div id="dd"></div> -->
</body>
</html>